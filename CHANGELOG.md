# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- "Simple" mode for assets: instead of controlling the price and amount of 
  simple assets separately, for simple mode assets you only need to control the
  value.

### Changed
- Sent webhook calls are now displayed in the timezone configured in the account
  settings
- Updated to PHP 8.2

## [0.3.0] - 2022-10-22
### Added
- Scheduled rebalancing checks: check the balance of allocations daily and send
  a webhook when rebalancing is needed.
- Allocation 'simple view' that shows only the important values
- Allocation 'purchase view' that shows only the information required to
  buy/sell assets, including the value in the asset's original currency
- Temporary transactions: mark a transaction as temporary and show a warning
  when it's older than 30 days.
- Site wide administrators that can view various system information
- Admin view for the log of scheduled check executions.
- Admin view for user management.

### Changed
- Allocation tolerance is now a threshold on the value deviation of the entire
  allocation, not on each separate allocation line anymore.
- Updated to PHP 8.1
- Updated to Laravel 9

## [0.2.0] - 2021-05-01
### Added
- Transaction notes: remind yourself of anything particular about the
  transaction
- Import configuration notes: remind yourself how to create an export in the
  source system, for example
- Show estimated invested amounts in value graph, based on the known price at
  the moment of transactions
- Show total value history of an allocation by stacking all included assets in a
  graph
- Timezone support: set a display timezone in the preferences so values are
  correctly inserted and displayed. Import configurations now also have a
  timezone, which is used as a fallback when the input data does not already
  specify it.
- Resolution selection for graphs
- Asset importing
- Ability to permanently delete a soft-deleted asset, so conflicts with asset
  aliases during import can be resolved
- Updating preferences such as name and e-mail address
- Behaviour Driven Development test suite using Behat
- Utility to test email sending: `php artisan email:test address@example.com`

### Changed
- Updated the name of the project from asset-allocation (working title) to
  Minerva, and added the owl icon
- Allow zero weights on allocation lines to specify that the value of that line
  should be fully reinvested into the other lines
- Include transactions and prices on the same day as is selected for showing an
  allocation, even when they happen later during the day
- Switch to the new organization upon creation
- Improve navigation on mobile by showing assets, allocations and import
  configurations in the navigation bar only if the screen can fit it
- Invitations now automatically expire after 7 days
- Changed pseudo-randomly generated graph colors with a handpicked palette of 9
  colors, after which pseudo-random generation still occurs
- Changed Account screen to Preferences
- Upgrade to Laravel 8
- Optimize Docker image size

### Fixed
- Fix crash when HTTP communication fails during import, the failure message
  will now be written to the import execution log
- Fix asset names and aliases being unique across all organizations
- Fix price and transaction import_id being unique across all organizations
- Fix asset and allocation names of other organizations being shown in the
  create allocation form
- Fix invitations not working properly when registration is open
- Fix crash when trying to import non-JSON data using the JSON format
- Fix crash when trying to import an Asset with an alias that already exists
- Fix crash when an external server does not respond

## [0.1.0] - 2020-09-25
Initial development release

### Added
- MIT license
- Changelog
