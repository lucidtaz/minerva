<?php

namespace Tests\Unit\Jobs;

use App\Jobs\SendWebhook;
use App\Models\Organization;
use App\Models\Webhook;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class SendWebhookTest extends TestCase
{
    private MockHandler $handler;
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();

        $this->handler = new MockHandler();
        $this->client = new Client([
            'handler' => $this->handler,
        ]);
    }

    public function testThatItSendsCorrectRequest(): void
    {
        $this->handler->append(new Response());

        $webhook = Webhook::factory()->make([
            'uri' => 'https://www.example.com',
            'shared_secret' => 'the-shared-secret',
        ]);

        $job = new SendWebhook(
            $webhook,
            'The payload',
            'text/plain',
        );
        $job->handle($this->client);

        $request = $this->handler->getLastRequest();
        $this->assertNotNull($request);

        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals('https://www.example.com', (string) $request->getUri());
        $this->assertEquals('/', $request->getRequestTarget());
        $this->assertEquals('The payload', (string) $request->getBody());

        $expectedSignature = hash_hmac('sha256', $request->getBody(), 'the-shared-secret');
        $actualSignature = $request->getHeaderLine('X-Webhook-Signature');
        $this->assertEquals($expectedSignature, $actualSignature);

        $userAgent = $request->getHeaderLine('User-Agent');
        $this->assertEquals('Minerva', $userAgent);
    }

    public function testThatItSavesACallLog(): void
    {
        $this->handler->append(new Response(200, [], 'Mock response'));

        /** @var Organization $organization */
        $organization = Organization::factory()->make();
        $organization->save();

        /** @var Webhook $webhook */
        $webhook = Webhook::factory()->make();
        $organization->webhooks()->save($webhook); // It needs to exist in the database in order to save a call log

        $job = new SendWebhook(
            $webhook,
            'The payload',
            'text/plain',
        );
        $job->handle($this->client);

        $this->assertCount(1, $webhook->calls);

        $call = $webhook->calls[0];

        $this->assertEquals("HTTP/1.1 200 OK\r\n\r\nMock response", $call->response);
        $this->assertEquals(200, $call->status_code);
        $this->assertGreaterThanOrEqual(0, $call->duration_ms);
        $this->assertNull($call->error);
        $this->assertNull($call->short_error);
    }
}
