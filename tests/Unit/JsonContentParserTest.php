<?php

namespace Tests\Unit;

use App\Services\Import\JsonContentParser;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Tests\TestCase;

class JsonContentParserTest extends TestCase
{
    private MockObject $logger;

    private JsonContentParser $contentParser;

    protected function setUp(): void
    {
        parent::setUp();
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->contentParser = new JsonContentParser($this->logger);
    }

    public function testThatItReportsErrorsOnBadDataFormat(): void
    {
        $rawData = 'This is not a JSON value';

        $this->logger->expects($this->once())
            ->method('error')
            ->with('Could not decode data as JSON. Error message: Syntax error');
        $this->logger->expects($this->once())
            ->method('debug')
            ->with('The data starts with: "This is not a JSON value"');

        $content = $this->contentParser->run($rawData);

        $this->assertEquals([], $content);
    }

    public function testThatItEllipsizesErroneousDataInDebugLog(): void
    {
        $rawData = '012345678901234567890123456789012345678901234567890123456789';
        $expected = '01234567890123456789012345678901234567890123456789';

        $this->logger->expects($this->once())
            ->method('error')
            ->with('Could not decode data as JSON. Error message: Syntax error');
        $this->logger->expects($this->once())
            ->method('debug')
            ->with("The data starts with: \"$expected\"");

        $content = $this->contentParser->run($rawData);

        $this->assertEquals([], $content);
    }
}
