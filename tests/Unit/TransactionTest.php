<?php

namespace Tests\Unit;

use App\Models\Transaction;
use Carbon\CarbonImmutable;
use Tests\TestCase;

class TransactionTest extends TestCase
{
    public function testMomentBehaviour(): void
    {
        // Verify some behaviour of the CarbonDateTime Eloquent cast integrated into a model

        $transaction = new Transaction([
            'moment' => new CarbonImmutable('2021-01-01 14:51:00'),
        ]);
        $this->assertEquals('UTC', $transaction->moment->timezone->getName());
        $this->assertEquals('2021-01-01 14:51:00', $transaction->moment->format('Y-m-d H:i:s'));

        // The attribute can be set to a new object including timezone, which is remembered
        // Moreover, now when reading the timezone out of the casted attribute it's reflected correctly
        // This is not something our Cast class does, since that always converts to UTC for internal storage
        // Eloquent models have a "class cast cache" which remembers the original object (pre-casting)
        // assigned to the attribute, and reuses it on subsequent access.
        $transaction->moment = new CarbonImmutable('2021-01-01 20:21:00', 'Asia/Calcutta');
        $this->assertEquals('Asia/Calcutta', $transaction->moment->timezone->getName());
        $this->assertEquals('2021-01-01 20:21:00', $transaction->moment->format('Y-m-d H:i:s'));
        $this->assertEquals('2021-01-01 14:51:00', $transaction->moment->setTimezone('UTC')->format('Y-m-d H:i:s'));

        // Changing the timezone directly does not work since the casted object is a CarbonImmutable, not a Carbon
        // If we would have assigned a Carbon object it would work fine
        $transaction->moment = new CarbonImmutable('2021-01-01 14:51:00');
        $transaction->moment->setTimezone('Europe/Amsterdam');
        $this->assertEquals('UTC', $transaction->moment->timezone->getName());
        $this->assertEquals('2021-01-01 14:51:00', $transaction->moment->format('Y-m-d H:i:s'));

        // Another way of setting a value with a timezone:
        $transaction->moment = new CarbonImmutable('2021-01-01 14:51:00');
        $transaction->moment = $transaction->moment->setTimezone('Asia/Calcutta');
        $this->assertEquals('Asia/Calcutta', $transaction->moment->timezone->getName());
        $this->assertEquals('2021-01-01 20:21:00', $transaction->moment->format('Y-m-d H:i:s'));

        // Internal representation is in UTC:
        $transaction->moment = new CarbonImmutable('2021-01-01 20:21:00', 'Asia/Calcutta');
        $this->assertEquals('2021-01-01T14:51:00.000000Z', $transaction->attributesToArray()['moment'] ?? null);
    }
}
