<?php

namespace Tests\Unit;

use App\Services\Import\DownloadInputMethod;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Tests\TestCase;

class DownloadInputMethodTest extends TestCase
{
    private MockHandler $httpClientHandler;
    private Client $httpClient;
    private MockObject $logger;

    protected function setUp(): void
    {
        parent::setUp();

        $this->httpClientHandler = new MockHandler();
        $this->httpClient = new Client(['handler' => $this->httpClientHandler]);

        $this->logger = $this->createMock(LoggerInterface::class);
    }

    public function testThatItReturnsResponseContents(): void
    {
        $downloadInputMethod = new DownloadInputMethod('https://www.example.com', $this->httpClient, $this->logger);

        $this->httpClientHandler->append(new Response(200, [], 'Mock response contents'));

        $data = $downloadInputMethod->getRawData();

        $this->assertEquals('Mock response contents', $data);
    }

    public function testThatItLogsCommunicationErrors(): void
    {
        $downloadInputMethod = new DownloadInputMethod('https://www.example.com', $this->httpClient, $this->logger);

        $this->httpClientHandler->append(new TransferException('Mock exception message'));

        $this->logger->expects($this->once())
            ->method('error')
            ->with('Error during HTTP request: Mock exception message');

        $data = $downloadInputMethod->getRawData();

        $this->assertEquals('', $data);
    }

    public function testThatItLogsHttpErrors(): void
    {
        $downloadInputMethod = new DownloadInputMethod('https://www.example.com', $this->httpClient, $this->logger);

        $this->httpClientHandler->append(new Response(404, [], 'Mock response'));

        $this->logger->expects($this->once())
            ->method('error')
            ->with('Response status code 404');

        $data = $downloadInputMethod->getRawData();

        $this->assertEquals('', $data);
    }
}
