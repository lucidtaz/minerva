<?php

namespace Tests\Unit\Services;

use App\Services\DateTimeHelper;
use Carbon\CarbonImmutable;
use Tests\TestCase;

class DateTimeHelperTest extends TestCase
{
    /**
     * @dataProvider timeIsInRangeData
     */
    public function testTimeIsInRange(string $time, string $timezone, CarbonImmutable $from, CarbonImmutable $to, bool $expected): void
    {
        $actual = DateTimeHelper::timeIsInRange($time, $timezone, $from, $to);
        $this->assertEquals(
            $expected,
            $actual,
            sprintf(
                'Failed asserting that %s in %s is in-between %s and %s',
                $time,
                $timezone,
                $from->toIso8601String(),
                $to->toIso8601String(),
            ),
        );
    }

    public function timeIsInRangeData(): array
    {
        return [
            'Simple true' => [
                '12:00',
                'UTC',
                new CarbonImmutable('2021-08-03 11:00:00', 'UTC'),
                new CarbonImmutable('2021-08-03 13:00:00', 'UTC'),
                true,
            ],
            'Multiple days true' => [
                '12:00',
                'UTC',
                new CarbonImmutable('2021-08-01 11:00:00', 'UTC'),
                new CarbonImmutable('2021-08-03 13:00:00', 'UTC'),
                true,
            ],
            'Simple false' => [
                '12:00',
                'UTC',
                new CarbonImmutable('2021-08-03 10:00:00', 'UTC'),
                new CarbonImmutable('2021-08-03 11:00:00', 'UTC'),
                false,
            ],
            'Multiple days false' => [
                '12:00',
                'UTC',
                new CarbonImmutable('2021-08-01 13:00:00', 'UTC'),
                new CarbonImmutable('2021-08-02 11:00:00', 'UTC'),
                false,
            ],
            'Timezone true' => [
                '14:00',
                'Europe/Amsterdam',
                new CarbonImmutable('2021-08-03 11:00:00', 'UTC'),
                new CarbonImmutable('2021-08-03 13:00:00', 'UTC'),
                true,
            ],
            'Timezone false' => [
                '12:00',
                'Europe/Amsterdam',
                new CarbonImmutable('2021-08-03 11:00:00', 'UTC'),
                new CarbonImmutable('2021-08-03 13:00:00', 'UTC'),
                false,
            ],
            'Timezone multiple days true 1' => [
                '14:00',
                'Europe/Amsterdam',
                new CarbonImmutable('2021-08-01 11:00:00', 'UTC'),
                new CarbonImmutable('2021-08-03 13:00:00', 'UTC'),
                true,
            ],
            'Timezone multiple days true 2' => [
                '12:00',
                'Europe/Amsterdam',
                new CarbonImmutable('2021-08-01 13:00:00', 'UTC'),
                new CarbonImmutable('2021-08-02 11:00:00', 'UTC'),
                true,
            ],
            'Daylight saving time true' => [ // Clock turns back
                '02:30',
                'Europe/Amsterdam',
                new CarbonImmutable('2021-10-31 01:00:00', 'Europe/Amsterdam'),
                new CarbonImmutable('2021-10-31 04:00:00', 'Europe/Amsterdam'),
                true,
            ],
            'Daylight saving time true that looks like false' => [ // Clock turns forward, but the moment is not skipped! This behaviour was implemented here: https://bugs.php.net/bug.php?id=79396
                '02:30', // This is a time that does not exist on that day
                'Europe/Amsterdam',
                new CarbonImmutable('2021-03-28 01:00:00', 'Europe/Amsterdam'),
                new CarbonImmutable('2021-03-28 04:00:00', 'Europe/Amsterdam'),
                true,
            ],
        ];
    }
}
