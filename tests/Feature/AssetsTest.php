<?php

namespace Tests\Feature;

use App\Enums\OrganizationUserRole;
use App\Models\Asset;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AssetsTest extends TestCase
{
    use DatabaseTransactions;

    public function testAssetListing(): void
    {
        /** @var Organization $organization */
        $organization = Organization::factory()->create();

        /** @var User $user */
        $user = User::factory()->create();
        $user->organizations()->attach($organization, ['role' => OrganizationUserRole::Member]);

        $asset = Asset::factory()->make();
        $organization->assets()->save($asset);

        $user->displayAsset()->associate($asset);

        $response = $this->actingAs($user)->get('/assets');

        $response->assertStatus(200);
    }
}
