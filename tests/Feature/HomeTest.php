<?php

namespace Tests\Feature;

use App\Enums\OrganizationUserRole;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class HomeTest extends TestCase
{
    use DatabaseTransactions;

    public function testGuestRedirect(): void
    {
        $response = $this->get('/');

        $response->assertStatus(302);
        $response->assertLocation('/login');
    }

    public function testHomeMessage(): void
    {
        /** @var Organization $organization */
        $organization = Organization::factory()->create();

        /** @var User $user */
        $user = User::factory()->create();
        $user->organizations()->attach($organization, ['role' => OrganizationUserRole::Member]);

        $response = $this->actingAs($user)->get('/');

        $response->assertStatus(200);
        $response->assertSeeText('You are logged in!');
    }
}
