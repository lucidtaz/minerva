<?php

namespace Tests\Feature;

use App\Enums\OrganizationUserRole;
use App\Models\Allocation;
use App\Models\Asset;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AllocationsTest extends TestCase
{
    use DatabaseTransactions;

    public function testCheckEndpoint(): void
    {
        /** @var Organization $organization */
        $organization = Organization::factory()->create();

        /** @var User $user */
        $user = User::factory()->create();
        $user->organizations()->attach($organization, ['role' => OrganizationUserRole::Member]);

        /** @var Asset $currencyAsset */
        $currencyAsset = Asset::factory()->currency()->make();
        $organization->assets()->save($currencyAsset);
        $user->displayAsset()->associate($currencyAsset);

        $allocation = Allocation::factory()->make();
        $organization->allocations()->save($allocation);

        $response = $this->actingAs($user, 'api')->postJson("/api/allocations/{$allocation->id}/check");

        $response->assertStatus(200);
    }
}
