<?php

namespace Tests\Acceptance\features\bootstrap;

use App\Enums\EntityType;
use App\Enums\InputContentType;
use App\Enums\InputMethod;
use App\Enums\OrganizationUserRole;
use App\Models\Allocation;
use App\Models\Asset;
use App\Models\AssetAlias;
use App\Models\ImportConfiguration;
use App\Models\Invitation;
use App\Models\Organization;
use App\Models\Price;
use App\Models\Transaction;
use App\Models\User;
use Behat\Gherkin\Node\TableNode;
use Carbon\CarbonImmutable;
use Config;
use Illuminate\Http\UploadedFile;
use Illuminate\Testing\TestResponse;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends BaseContext
{
    private ?Organization $givenOrganization;
    private ?Asset $givenCurrency = null;
    private ?User $givenUser;
    private ?Invitation $givenInvitation;
    private ?User $actingUser;
    private ?Asset $givenAsset;
    private ?Allocation $givenAllocation;
    private ?ImportConfiguration $givenImportConfiguration;

    private ?TestResponse $response;

    /**
     * @Given /^I am a guest user$/
     */
    public function iAmAGuestUser()
    {
        $this->actingUser = null;
    }

    /**
     * @Given /^open registration is enabled$/
     */
    public function openRegistrationIsEnabled()
    {
        Config::set('app.open_registration', true);
    }

    /**
     * @Given /^open registration is disabled$/
     */
    public function openRegistrationIsDisabled()
    {
        Config::set('app.open_registration', false);
    }

    /**
     * @Given a stored Organization
     */
    public function givenAStoredOrganization(): void
    {
        $this->givenOrganization = Organization::factory()->create();
    }

    /**
     * @Given a stored User
     */
    public function givenAStoredUser(): void
    {
        $this->givenUser = User::factory()->make();

        if ($this->givenCurrency !== null) {
            $this->givenUser->displayAsset()->associate($this->givenCurrency);
        }

        if ($this->givenOrganization !== null) {
            $this->givenOrganization->users()->save($this->givenUser, [
                'role' => OrganizationUserRole::Admin,
            ]);
            $this->givenOrganization->unsetRelations();
        }
    }

    /**
     * @Given a stored Invitation
     */
    public function givenAStoredInvitation(): void
    {
        $this->givenInvitation = Invitation::factory()->create([
            'organization_id' => $this->givenOrganization->id,
        ]);
    }

    /**
     * @Given I am logged in as that User
     */
    public function givenLoggedInAsGivenUser(): void
    {
        $this->actingUser = $this->givenUser;
    }

    /**
     * @Given a stored currency :symbol - :name
     * @Given a stored currency :symbol - :name with a price of :priceCurrencySymbol :priceValue
     */
    public function givenAStoredCurrency(string $symbol, string $name, ?string $priceCurrencySymbol = null, ?string $priceValue = null): void
    {
        $this->givenCurrency = Asset::factory()->make([
            'name' => $name,
            'currency_symbol' => $symbol,
        ]);
        $this->givenOrganization->assets()->save($this->givenCurrency);
        $this->givenOrganization->unsetRelations();

        if ($priceCurrencySymbol !== null && $priceValue !== null) {
            $currency = $this->givenOrganization
                ->assets
                ->where('currency_symbol', '=', $priceCurrencySymbol)
                ->first();

            $price = Price::factory()->make([
                'value' => $priceValue,
                'base_asset_id' => $currency->id,
                'moment' => CarbonImmutable::minValue(),
            ]);
            $this->givenCurrency->prices()->save($price);
            $this->givenCurrency->unsetRelations();
        }
    }

    /**
     * @Given a stored Asset :name
     */
    public function givenAStoredAsset(string $name): void
    {
        $this->givenAsset = Asset::factory()->make([
            'name' => $name,
        ]);
        $this->givenOrganization->assets()->save($this->givenAsset);
        $this->givenOrganization->unsetRelations();
    }

    /**
     * @Given a stored Asset :name with aliases
     */
    public function givenAStoredAssetWithAliases(string $name, TableNode $tableNode): void
    {
        $this->givenAsset = Asset::factory()->make([
            'name' => $name,
        ]);
        $this->givenOrganization->assets()->save($this->givenAsset);
        $this->givenOrganization->unsetRelations();

        foreach ($tableNode->getRows() as $row) {
            $alias = $row[0];

            $assetAlias = new AssetAlias([
                'alias' => $alias,
            ]);
            $assetAlias->organization()->associate($this->givenOrganization);
            $assetAlias->asset()->associate($this->givenAsset);
            $assetAlias->save();
        }
    }

    /**
     * @Given a stored Asset :name with a price of :currencySymbol :priceValue
     * @Given a stored Asset :name with a price of :currencySymbol :priceValue and amount of :amount
     * @Given a stored Asset :name with a price of :currencySymbol :priceValue and amount of :amount at :moment
     */
    public function givenAssetPrice(string $name, string $currencySymbol, string $priceValue, ?string $amount = null, ?string $moment = null): void
    {
        if ($moment === null) {
            $moment = CarbonImmutable::now()->toIso8601String();
        }

        $this->givenAsset = Asset::factory()->make([
            'name' => $name,
        ]);
        $this->givenOrganization->assets()->save($this->givenAsset);
        $this->givenOrganization->unsetRelations();

        $currency = $this->givenOrganization
            ->assets
            ->where('currency_symbol', '=', $currencySymbol)
            ->first();

        $price = Price::factory()->make([
            'value' => $priceValue,
            'base_asset_id' => $currency->id,
            'moment' => $moment,
        ]);
        $this->givenAsset->prices()->save($price);

        if ($amount !== null) {
            $transaction = Transaction::factory()->make([
                'amount' => $amount,
                'moment' => $moment,
            ]);
            $this->givenAsset->transactions()->save($transaction);
        }
        $this->givenAsset->unsetRelations();
    }

    /**
     * @Given Asset :name has a value of :currencySymbol :value
     */
    public function givenAssetValue(string $name, string $currencySymbol, string $value): void
    {
        /** @var Asset|null $asset */
        $asset = $this->givenOrganization
            ->assets
            ->first(fn (Asset $asset) => $asset->name === $name);
        $this->assertNotNull($asset);

        /** @var Asset|null $currency */
        $currency = $this->givenOrganization
            ->assets
            ->first(fn (Asset $asset) => $asset->currency_symbol === $currencySymbol);
        $this->assertNotNull($currency);

        if ($currency->id !== $asset->id) {
            /** @var Price|null $price */
            $price = $asset->prices
                ->filter(fn (Price $price) => $price->base_asset_id === $currency->id)
                ->sortByDesc(fn (Price $price) => $price->moment->timestamp)
                ->first();
            if ($price === null) {
                $price = Price::factory()->make([
                    'base_asset_id' => $currency->id,
                ]);
                $asset->prices()->save($price);
            }

            $amount = bcdiv($value, $price->value);
        } else {
            $amount = $value;
        }

        $transaction = Transaction::factory()->make([
            'amount' => $amount,
        ]);
        $asset->transactions()->save($transaction);
    }

    /**
     * @Given a stored :asset Transaction amount of :amount
     * @Given a stored :asset Transaction amount of :amount at :moment
     */
    public function givenAStoredTransaction(string $name, string $amount, ?string $moment = null): void
    {
        /** @var Asset|null $asset */
        $asset = $this->givenOrganization
            ->assets
            ->first(fn (Asset $asset) => $asset->name === $name);
        $this->assertNotNull($asset);

        $transaction = Transaction::factory()->make([
            'amount' => $amount,
            'moment' => new CarbonImmutable($moment ?? 'now'),
        ]);
        $asset->transactions()->save($transaction);
    }

    /**
     * @Given a stored Allocation with configuration
     * @Given a stored Allocation :name with configuration
     */
    public function givenAStoredAllocation(TableNode $table, ?string $name = null): void
    {
        $items = $table->getColumnsHash();

        $values = [];
        if ($name !== null) {
            $values['name'] = $name;
        }

        /** @var Allocation $allocation */
        $allocation = Allocation::factory()->make($values);
        $this->givenAllocation = $allocation;

        $this->givenOrganization->allocations()->save($this->givenAllocation);
        $this->givenOrganization->unsetRelations();

        foreach ($items as $item) {
            if ($item['type'] === 'asset') {
                $asset = $this->givenOrganization
                    ->assets
                    ->first(fn (Asset $asset) => $asset->name === $item['name']);
                $this->assertNotNull($asset, "Did not find Asset '{$item['name']}'");
                $this->givenAllocation
                    ->assets()
                    ->attach($asset, ['weight' => $item['weight'] ?? 1]);
            } elseif ($item['type'] === 'allocation') {
                $childAllocation = $this->givenOrganization
                    ->allocations
                    ->first(fn (Allocation $allocation) => $allocation->name === $item['name']);
                $this->assertNotNull($childAllocation, "Did not find Allocation '{$item['name']}'");
                $this->givenAllocation
                    ->childAllocations()
                    ->attach($childAllocation, ['weight' => $item['weight'] ?? 1]);
            }
        }
    }

    /**
     * @Given an Import Configuration (with values)
     */
    public function givenAnImportConfiguration(?TableNode $table = null): void
    {
        $data = $table !== null ? $table->getColumnsHash()[0]: [];

        /** @var Allocation $allocation */
        $this->givenImportConfiguration = ImportConfiguration::factory()->make($data);

        $this->givenOrganization->importConfigurations()->save($this->givenImportConfiguration);
        $this->givenOrganization->unsetRelations();
    }

    /**
     * @When /^I register an account$/
     */
    public function iRegisterAnAccount(?string $invitationToken = null)
    {
        $number = random_int(1000, 9999);
        $data = [
            'name' => "Tester $number",
            'email' => "unittest_$number@example.com",
            'password' => 'passwordExample1!',
            'password_confirmation' => 'passwordExample1!',
            'organization_name' => 'Default',
            'invitation_token' => $invitationToken,
        ];

        if ($this->actingUser !== null) {
            $this->actingAs($this->actingUser);
        }
        $this->response = $this->post('register', $data);
    }

    /**
     * @When /^I register an account using the invitation$/
     */
    public function iRegisterAnAccountUsingTheInvitation()
    {
        $this->iRegisterAnAccount($this->givenInvitation->token);
    }

    /**
     * @When I create a Transaction (with values)
     */
    public function iCreateATransaction(?TableNode $table = null): void
    {
        $data = $table !== null ? $table->getColumnsHash()[0]: [];

        $values = [
            'amount' => $data['amount'] ?? '10.00',
            'note' => $data['note'] ?? null,
            'location' => $data['location'] ?? null,
            'moment' => $data['moment'] ?? CarbonImmutable::now()->toIso8601String(),
        ];

        $this->response = $this
            ->followingRedirects()
            ->actingAs($this->actingUser)
            ->post("/assets/{$this->givenAsset->uuid}/transactions", $values);
    }

    /**
     * @When I create an Import Configuration (with values)
     */
    public function iCreateAnImportConfiguration(?TableNode $table = null): void
    {
        $data = $table !== null ? $table->getColumnsHash()[0]: [];

        $values = [
            'name' => $data['name'] ?? 'Sample name',
            'note' => $data['note'] ?? null,
            'input_method' => $data['input_method'] ?? InputMethod::Upload->value,
            'input_content_type' => $data['input_content_type'] ?? InputContentType::CSV->value,
            'date_format' => $data['date_format'] ?? null,
            'url' => $data['url'] ?? 'https://www.example.com',
            'has_header_row' => $data['has_header_row'] ?? '1',
            'delimiter' => $data['delimiter'] ?? ',',
            'enclosure_character' => $data['enclosure_character'] ?? '"',
            'escape_character' => $data['escape_character'] ?? '\\\\',
            'parse_expression' => $data['parse_expression'] ?? '[.[] | {}]',
            'entity_type' => $data['entity_type'] ?? EntityType::Price->value,
        ];

        $this->response = $this
            ->followingRedirects()
            ->actingAs($this->actingUser)
            ->post("/imports", $values);
    }

    /**
     * @When I view the Asset's Transactions
     */
    public function iViewTheAssetTransactions(): void
    {
        $this->response = $this->actingAs($this->actingUser)->get("/assets/{$this->givenAsset->uuid}");
    }

    /**
     * @When I view the Allocation
     * @When I view the Allocation with filter
     */
    public function iViewTheStoredAllocation(?TableNode $filter = null): void
    {
        if ($filter !== null) {
            $queryString = http_build_query($filter->getColumnsHash()[0]);
        } else {
            $queryString = '';
        }
        $this->response = $this->actingAs($this->actingUser)->get("/allocations/{$this->givenAllocation->id}/detailed?$queryString");
    }

    /**
     * @When I import a(n) :extension file with contents:
     */
    public function iImportAFile(string $extension, string $contents): void
    {
        $filename = 'test.' . strtolower($extension);
        $this->response = $this->actingAs($this->actingUser)
            ->followingRedirects()
            ->post("/imports/{$this->givenImportConfiguration->id}/run", [
                'import_file' => UploadedFile::fake()->createWithContent($filename, $contents)
            ]);
    }

    /**
     * @Then /^I am logged in$/
     */
    public function iAmLoggedIn()
    {
        $this->assertAuthenticated();
    }

    /**
     * @Then /^I can use the application$/
     */
    public function iCanUseTheApplication()
    {
        $this->response = $this->get('/assets');
        $this->response->assertSuccessful();
    }

    /**
     * @Then the operation is successful
     */
    public function theOperationIsSuccessful()
    {
        $this->response->assertOk();
    }

    /**
     * @Then I see :text
     */
    public function iSee(string $text)
    {
        $this->response->assertSee($text);
    }

    /**
     * @Then /^I see that I need an Invitation$/
     */
    public function iSeeThatINeedAnInvitation()
    {
        $this->response->assertSessionHasErrors(['invitation_token' => 'The invitation token field is required.']);
    }

    /**
     * @Then I see Allocation Lines
     */
    public function iSeeAllocationLines(TableNode $table): void
    {
        $expectedLines = $table->getColumnsHash();

        $crawler = new Crawler();
        $crawler->addHtmlContent($this->response->getContent());

        $foundLines = $crawler->filter('.allocation-lines');

        foreach ($expectedLines as $expectedLine) {
            $matchedLine = null;

            foreach ($foundLines as $foundLine) {
                $lineCrawler = new Crawler();
                $lineCrawler->addNode($foundLine);

                $linkNodes = $lineCrawler->filter('td > a');
                foreach ($linkNodes as $linkNode) {
                    if (trim($linkNode->nodeValue) === $expectedLine['asset class']) {
                        // Found the correct line!
                        $matchedLine = $foundLine;
                        continue 2;
                    }
                }
            }

            $this->assertNotNull($matchedLine, "Failed to find an Allocation Line for '{$expectedLine['asset class']}'");

            $assetClass = trim($matchedLine->childNodes[0*2+1]->nodeValue);
            $share = trim($matchedLine->childNodes[1*2+1]->nodeValue);
            $amount = trim($matchedLine->childNodes[2*2+1]->nodeValue);
            $price = trim($matchedLine->childNodes[3*2+1]->nodeValue);
            $value = trim($matchedLine->childNodes[4*2+1]->nodeValue);
            $targetValue = trim($matchedLine->childNodes[5*2+1]->nodeValue);
            $deltaValue = trim($matchedLine->childNodes[6*2+1]->nodeValue);
            $delta = trim($matchedLine->childNodes[7*2+1]->nodeValue);

            foreach ($expectedLine as $attribute => $expectedValue) {
                if ($attribute === 'asset class') {
                    $this->assertStringContainsString($expectedValue, $assetClass, "Failed asserting that asset class '$assetClass' contains '$expectedValue'");
                } elseif ($attribute === 'share') {
                    $this->assertStringContainsString($expectedValue, $share, "Failed asserting that share '$share' contains to '$expectedValue'");
                } elseif ($attribute === 'amount') {
                    $this->assertStringContainsString($expectedValue, $amount, "Failed asserting that amount '$amount' contains '$expectedValue'");
                } elseif ($attribute === 'price') {
                    $this->assertStringContainsString($expectedValue, $price, "Failed asserting that price '$price' contains '$expectedValue'");
                } elseif ($attribute === 'value') {
                    $this->assertStringContainsString($expectedValue, $value, "Failed asserting that value '$value' contains '$expectedValue'");
                } elseif ($attribute === 'target value') {
                    $this->assertStringContainsString($expectedValue, $targetValue, "Failed asserting that target value '$targetValue' contains '$expectedValue'");
                } elseif ($attribute === 'delta value') {
                    $this->assertStringContainsString($expectedValue, $deltaValue, "Failed asserting that delta value '$deltaValue' contains '$expectedValue'");
                } elseif ($attribute === 'delta') {
                    $this->assertStringContainsString($expectedValue, $delta, "Failed asserting that delta '$delta' contains '$expectedValue'");
                } else {
                    $this->fail("Unexpected attribute to compare: $attribute");
                }
            }
        }
    }

    /**
     * @Then /^I am a member of the organization$/
     */
    public function iAmAMemberOfTheOrganization()
    {
        $this->response = $this->get('/organizations');
        $this->response->assertSee($this->givenOrganization->name);
    }

    /**
     * @Then an Asset is saved with values
     * @Then Assets are saved with values
     */
    public function anAssetIsSaved(TableNode $table): void
    {
        $expectedAssets = $table->getColumnsHash();

        foreach ($expectedAssets as $expectedAsset) {
            $expectedAsset['organization_id'] = $expectedAsset['organization_id'] ?? $this->givenOrganization->id;

            $this->assertDatabaseHas('assets', $expectedAsset);
        }
    }
}
