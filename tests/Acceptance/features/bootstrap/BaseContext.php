<?php

namespace Tests\Acceptance\features\bootstrap;

use Behat\Behat\Context\Context;
use Tests\TestCase;

/**
 * Defines application features from the specific context.
 */
class BaseContext extends TestCase implements Context
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $_ENV['APP_ENV'] = 'testing';
        $_SERVER['APP_ENV'] = 'testing';
        putenv('APP_ENV=testing');

        parent::__construct();
        $this->setUp();
    }

    /**
     * @BeforeScenario
     */
    public function before(): void
    {
        $this->artisan('migrate:fresh');
    }
}
