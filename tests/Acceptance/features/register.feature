Feature: Account registration

  As a guest user
  I want to register an account
  So that I can start using the application

  As a guest user
  I want to register an account using an invitation
  So that I become a member of an existing organization

  Scenario: A guest registers
    Given I am a guest user
    And open registration is enabled
    When I register an account
    Then I am logged in
    And I can use the application

  Scenario: Registration is not open
    Given I am a guest user
    And open registration is disabled
    When I register an account
    Then I see that I need an Invitation

  Scenario: Registration with an invitation while registration is not open
    Given a stored Organization
    And a stored Invitation
    And I am a guest user
    And open registration is disabled
    When I register an account using the invitation
    Then I am logged in
    And I am a member of the organization

  Scenario: Registration with an invitation while registration is open
    Given a stored Organization
    And a stored Invitation
    And I am a guest user
    And open registration is enabled
    When I register an account using the invitation
    Then I am logged in
    And I am a member of the organization
