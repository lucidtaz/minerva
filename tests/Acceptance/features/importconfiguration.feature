Feature: Import configuration

  As a user
  I want to add a note to an import configuration
  So that I can remind myself how to export data from the source system

  Background:
    Given a stored Organization
    And a stored User
    And I am logged in as that User

  Scenario: Import configuration with note
    When I create an Import Configuration with values
      | note        |
      | Sample note |
    Then I see "Sample note"
