Feature: Transaction management

  As a user
  I want to add notes to transactions
  So that I can be reminded to remove a manually added transaction after its automatic counterpart is imported

  Background:
    Given a stored Organization
    And a stored currency E - EUR
    And a stored User
    And I am logged in as that User

  Scenario: Create a new Transaction with a note
    Given a stored Asset "Beyond Meat"
    When I create a Transaction with values
      |note                 |
      |This is a sample note|
    And I view the Asset's Transactions
    Then I see "This is a sample note"
