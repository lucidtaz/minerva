Feature: Import configuration

  As a user
  I want to import assets from a file
  So that I can restore my data from a back-up

  Background:
    Given a stored Organization
    And a stored User
    And I am logged in as that User

  Scenario: Import a new asset
    Given an Import Configuration with values
      | input_method | input_content_type | entity_type | has_header_row | delimiter | enclosure_character | escape_character | parse_expression |
      | upload       | csv                | asset       | 1              | ,         | "                   | \\\\             | [.[] \| {name: .name, import_id: .import_id, symbol: .currency_symbol, aliases: (.aliases \| split(","))}] |
    When I import a CSV file with contents:
      """
      "name","import_id","currency_symbol","aliases"
      "Test Asset","test_asset","T","test asset,test_asset"
      """
    Then the operation is successful
    And I see "INFO: Running import configuration"
    And I see "INFO: Import complete"
    And an Asset is saved with values
      | name       |
      | Test Asset |

  Scenario: Import asset with conflicting alias
    Given a stored Asset "Conflicting Test" with aliases
      | test asset |
    Given an Import Configuration with values
      | input_method | input_content_type | entity_type | has_header_row | delimiter | enclosure_character | escape_character | parse_expression |
      | upload       | csv                | asset       | 1              | ,         | "                   | \\\\             | [.[] \| {name: .name, import_id: .import_id, symbol: .currency_symbol, aliases: (.aliases \| split(","))}] |
    When I import a CSV file with contents:
      """
      "name","import_id","currency_symbol","aliases"
      "Test Asset","test_asset","T","test asset,test_asset"
      """
    Then the operation is successful
    And I see "INFO: Running import configuration"
    And I see 'ERROR: Asset alias already exists. Alias = "test asset". The offending Asset must be deleted, even if it is already soft-deleted.'
    And I see "INFO: Import complete"

  Scenario: Import assets with different modes
    Given an Import Configuration with values
      | input_method | input_content_type | entity_type | has_header_row | delimiter | enclosure_character | escape_character | parse_expression |
      | upload       | csv                | asset       | 1              | ,         | "                   | \\\\             | [.[] \| {name: .name, import_id: .import_id, symbol: .currency_symbol, mode: .mode, aliases: (.aliases \| split(","))}] |
    When I import a CSV file with contents:
      """
      "name","import_id","currency_symbol","mode","aliases"
      "Test Asset 1","test_asset_1","T1","normal","test asset 1"
      "Test Asset 2","test_asset_2","T2","simple","test asset 2"
      """
    Then the operation is successful
    And I see "INFO: Running import configuration"
    And I see "INFO: Import complete"
    And Assets are saved with values
      | name         | mode   |
      | Test Asset 1 | normal |
      | Test Asset 2 | simple |
