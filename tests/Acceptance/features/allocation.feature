Feature: Allocation display

  As a user
  I want to view an allocation
  So that I can balance my investments

  As a user
  I want to include allocation lines with weight zero
  So that I can represent a currency that I want to fully invest

  Background:
    Given a stored Organization
    And a stored currency E - EUR
    And a stored User
    And I am logged in as that User

  Scenario: No Assets
    Given a stored Allocation with configuration
      | type | name |
    When I view the Allocation
    Then I see Allocation Lines
      | asset class | value    | target value |
    And I see "Total value deviation: E 0.00 (0%)"

  Scenario: Single Asset
    Given a stored Asset "Beyond Meat" with a price of E 230 and amount of 1
    And a stored Allocation with configuration
      | type  | name        |
      | asset | Beyond Meat |
    When I view the Allocation
    Then I see Allocation Lines
      | asset class | value    | target value |
      | Beyond Meat | E 230.00 |     E 230.00 |
    And I see "Total value deviation: E 0.00 (0%)"

  Scenario: Single Asset with currency conversion
    Given a stored currency S - USD with a price of E 1.18
    Given a stored Asset "Beyond Meat" with a price of S 195 and amount of 1
    And a stored Allocation with configuration
      | type  | name        |
      | asset | Beyond Meat |
    When I view the Allocation
    Then I see Allocation Lines
      | asset class | value    | target value |
      | Beyond Meat | E 230.10 |     E 230.10 |

  Scenario: Sub Allocation
    Given a stored Asset "Beyond Meat" with a price of E 230 and amount of 10
    Given a stored Asset "First Solar" with a price of E 70 and amount of 20
    And a stored Asset "Bitcoin" with a price of E 5000 and amount of 1
    And a stored Allocation "Stocks" with configuration
      | type  | name        | weight |
      | asset | Beyond Meat |      6 |
      | asset | First Solar |      4 |
    And a stored Allocation "Crypto" with configuration
      | type  | name    | weight |
      | asset | Bitcoin |      1 |
    And a stored Allocation with configuration
      | type       | name   | weight |
      | allocation | Stocks |      7 |
      | allocation | Crypto |      3 |
    When I view the Allocation
    Then I see Allocation Lines
      | asset class | share     | amount | price      | value      | target value | delta value | delta |
      | Beyond Meat | 26% (42%) |  10.00 |   E 230.00 | E 2,300.00 |   E 3,654.00 |  E 1,354.00 |  5.89 |
      | First Solar | 16% (28%) |  20.00 |    E 70.00 | E 1,400.00 |   E 2,436.00 |  E 1,036.00 | 14.80 |
      | Stocks      | 43% (70%) |        |            | E 3,700.00 |   E 6,090.00 |  E 2,390.00 |       |
      | Bitcoin     | 57% (30%) |   1.00 | E 5,000.00 | E 5,000.00 |   E 2,610.00 | E -2,390.00 | -0.48 |
      | Crypto      | 57% (30%) |        |            | E 5,000.00 |   E 2,610.00 | E -2,390.00 |       |
    And I see "Total value deviation: E 2,390.00 (27%)"

  Scenario: Total value drain (investment buffer)
    Given a stored Asset "Beyond Meat" with a price of E 250
    And Asset "EUR" has a value of E 500
    And a stored Allocation with configuration
      | type  | name        | weight |
      | asset | Beyond Meat |    100 |
      | asset | EUR         |      0 |
    When I view the Allocation
    Then I see Allocation Lines
      | asset class | amount |    value | target value | delta value |      delta |
      | Beyond Meat |   0.00 |   E 0.00 |     E 500.00 |    E 500.00 |       2.00 |
      | EUR         | 500.00 | E 500.00 |       E 0.00 |   E -500.00 |    -500.00 |
    And I see "Total value deviation: E 500.00 (100%)"

  Scenario: Date filter without time
    Given a stored Asset "Beyond Meat" with a price of E 200 and amount of 1 at "2020-11-22 12:34:56"
    And a stored "Beyond Meat" Transaction amount of 2 at "2020-11-22 12:34:56"
    And a stored Allocation with configuration
      | type  | name        |
      | asset | Beyond Meat |
    When I view the Allocation with filter
      | date       |
      | 2020-11-22 |
    Then I see Allocation Lines
      | asset class | price    | value    |
      | Beyond Meat | E 200.00 | E 600.00 |
