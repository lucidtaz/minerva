<?php

namespace Database\Factories;

use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class TransactionFactory extends Factory
{
    protected $model = Transaction::class;

    public function definition()
    {
        return [
            'moment' => Carbon::make($this->faker->dateTime)->toIso8601String(),
            'amount' => $this->faker->randomElement(['', '', '', '', '-']) . $this->faker->randomNumber($this->faker->numberBetween(1, 8)) . '.' . $this->faker->randomNumber($this->faker->numberBetween(1, 8)),
        ];
    }
}
