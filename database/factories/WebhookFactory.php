<?php

namespace Database\Factories;

use App\Models\Webhook;
use Illuminate\Database\Eloquent\Factories\Factory;

class WebhookFactory extends Factory
{
    protected $model = Webhook::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->words(3, true),
            'uri' => 'https://www.example.com/' . $this->faker->slug,
            'shared_secret' => base64_encode(random_bytes(24)),
        ];
    }
}
