<?php

namespace Database\Factories;

use App\Models\Allocation;
use Illuminate\Database\Eloquent\Factories\Factory;

class AllocationFactory extends Factory
{
    protected $model = Allocation::class;

    public function definition()
    {
        return [
            'name' => ucwords($this->faker->words(3, true)),
            'tolerance' => $this->faker->numberBetween('0', '99'),
        ];
    }
}
