<?php

namespace Database\Factories;

use App\Enums\OrganizationUserRole;
use App\Models\Invitation;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvitationFactory extends Factory
{
    protected $model = Invitation::class;

    public function definition()
    {
        return [
            'token' => $this->faker->uuid,
            'note' => $this->faker->paragraph,
            'role' => $this->faker->randomElement(OrganizationUserRole::cases()),
            'expires_at' => CarbonImmutable::now()->addDays(config('app.invitation_expire_days')),
        ];
    }

    public function expired()
    {
        return $this->state(function (array $attributes) {
            return [
                'expires_at' => CarbonImmutable::now()->subDay(),
            ];
        });
    }
}
