<?php

namespace Database\Factories;

use App\Models\Price;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class PriceFactory extends Factory
{
    protected $model = Price::class;

    public function definition()
    {
        return [
            'moment' => Carbon::make($this->faker->dateTime)->toIso8601String(),
            'value' => $this->faker->randomNumber($this->faker->numberBetween(1, 8)) . '.' . $this->faker->randomNumber($this->faker->numberBetween(1, 8)),
        ];
    }
}
