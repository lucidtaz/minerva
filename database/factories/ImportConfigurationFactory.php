<?php

namespace Database\Factories;

use App\Enums\EntityType;
use App\Enums\InputContentType;
use App\Enums\InputMethod;
use App\Models\ImportConfiguration;
use Illuminate\Database\Eloquent\Factories\Factory;

class ImportConfigurationFactory extends Factory
{
    protected $model = ImportConfiguration::class;

    public function definition()
    {
        $inputMethod = $this->faker->randomElement(InputMethod::cases());
        $inputContentType = $this->faker->randomElement(InputContentType::cases());
        $entityType = $this->faker->randomElement(EntityType::cases());

        return [
            'name' => $this->faker->words(3, true),
            'note' => $this->faker->randomElement([$this->faker->paragraphs(3, true), null]),
            'input_method' => $inputMethod,
            'input_content_type' => $inputContentType,
            'date_format' => $this->faker->randomElement([null, 'U', 'Y-m-d H:i:s', 'd-m-Y']),
            'timezone' => $this->faker->timezone,
            'url' => $inputMethod === InputMethod::Download ? $this->faker->url : null,
            'has_header_row' => $inputContentType === InputContentType::CSV ? $this->faker->boolean : null,
            'delimiter' => $inputContentType === InputContentType::CSV ? ',' : null,
            'enclosure_character' => $inputContentType === InputContentType::CSV ? '"' : null,
            'escape_character' => $inputContentType === InputContentType::CSV ? '\\' : null,
            'entity_type' => $entityType,
            'parse_expression' => '[*]',
        ];
    }
}
