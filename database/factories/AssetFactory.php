<?php

namespace Database\Factories;

use App\Enums\AssetMode;
use App\Models\Asset;
use App\Models\AssetAlias;
use Illuminate\Database\Eloquent\Factories\Factory;

class AssetFactory extends Factory
{
    protected $model = Asset::class;

    public function definition()
    {
        $namePossibilities = [
            $this->faker->currencyCode,
            ucwords($this->faker->words(3, true)),
            $this->faker->company,
        ];

        return [
            'uuid' => $this->faker->uuid,
            'name' => $this->faker->randomElement($namePossibilities),
            'mode' => AssetMode::Normal,
            'import_id' => $this->faker->uuid,
        ];
    }

    public function currency()
    {
        return $this->state(function (array $attributes) {
            return [
                'currency_symbol' => strtoupper($this->faker->randomLetter),
            ];
        });
    }

    public function configure()
    {
        return $this->afterCreating(function (Asset $asset) {
            $nameAlias = new AssetAlias([
                'alias' => $asset->name,
            ]);
            $uuidAlias = new AssetAlias([
                'alias' => $asset->uuid,
            ]);
            $nameAlias->organization()->associate($asset->organization);
            $uuidAlias->organization()->associate($asset->organization);
            $asset->aliases()->saveMany([$nameAlias, $uuidAlias]);
        });
    }
}
