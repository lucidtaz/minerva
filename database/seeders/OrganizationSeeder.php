<?php

namespace Database\Seeders;

use App\Models\Organization;
use Illuminate\Database\Seeder;

class OrganizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $acme = Organization::query()
            ->where('name', '=', 'acme')
            ->first();
        if ($acme === null) {
            Organization::factory()->create([
                'name' => 'Acme',
            ]);
        }

        Organization::factory()->create();
    }
}
