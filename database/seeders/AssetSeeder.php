<?php

namespace Database\Seeders;

use App\Models\Asset;
use App\Models\AssetAlias;
use App\Models\Organization;
use Illuminate\Database\Seeder;

class AssetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        /** @var Organization $organization */
        $organization = Organization::where('name', '=', 'Acme')
            ->firstOrFail();

        $euro = $organization->assets()
            ->where('name', '=', 'EUR')
            ->first();
        if ($euro === null) {
            $euro = Asset::factory()->make([
                'name' => 'EUR',
                'currency_symbol' => '€',
            ]);
            $organization->assets()->save($euro);

            $this->addAlias($euro, $euro->name);
        }

        $bitcoinName = 'XBT';
        $bitcoin = $organization->assets()
            ->where('name', '=', $bitcoinName)
            ->first();
        if ($bitcoin === null) {
            /** @var Asset $bitcoin */
            $bitcoin = Asset::factory()->make([
                'name' => $bitcoinName,
                'currency_symbol' => '₿',
            ]);
            $organization->assets()->save($bitcoin);

            $this->addAlias($bitcoin, $bitcoin->name);
            $this->addAlias($bitcoin, 'XXBT');
        }

        $randomAssets = [
            ...Asset::factory()->currency()->times(2)->make(),
            ...Asset::factory()->times(2)->make(),
        ];
        $organization->assets()->saveMany($randomAssets);
    }

    private function addAlias(Asset $asset, string $aliasValue): void
    {
        $alias = new AssetAlias([
            'alias' => $aliasValue,
        ]);
        $alias->organization()->associate($asset->organization);
        $asset->aliases()->save($alias);
    }
}
