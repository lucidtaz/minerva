<?php

namespace Database\Seeders;

use App\Models\Asset;
use App\Models\Transaction;
use Illuminate\Database\Seeder;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Asset::each(function (Asset $asset) {
            Transaction::factory()->create([
                'asset_id' => $asset->id,
            ]);
        });
    }
}
