<?php

namespace Database\Seeders;

use App\Models\Organization;
use App\Models\Webhook;
use Illuminate\Database\Seeder;

class WebhookSeeder extends Seeder
{
    public function run(): void
    {
        $acme = Organization::query()
            ->where('name', '=', 'Acme')
            ->firstOrFail();

        $webhook = Webhook::factory()->make();
        $acme->webhooks()->save($webhook);
    }
}
