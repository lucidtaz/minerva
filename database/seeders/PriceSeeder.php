<?php

namespace Database\Seeders;

use App\Models\Asset;
use App\Models\Price;
use Illuminate\Database\Seeder;

class PriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $euro = Asset::query()
            ->where('name', '=', 'EUR')
            ->firstOrFail();

        Asset::each(function (Asset $asset) use ($euro) {
            if ($asset->id !== $euro->id) {
                Price::factory()->create([
                    'asset_id' => $asset->id,
                    'base_asset_id' => $euro->id,
                ]);
            }
        });
    }
}
