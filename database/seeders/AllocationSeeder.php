<?php

namespace Database\Seeders;

use App\Models\Allocation;
use App\Models\Organization;
use Illuminate\Database\Seeder;

class AllocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var Organization $organization */
        $organization = Organization::where('name', '=', 'Acme')
            ->firstOrFail();

        /** @var Allocation $allocation */
        $allocation = Allocation::factory()->make();
        $organization->allocations()->save($allocation);

        // Select assets at random
        $assets = $organization->assets->filter(function () {
            return random_int(0, 1) === 0;
        });
        $allocation->assets()->attach($assets, ['weight' => 1]);

        // Select allocations at random
        $childAllocations = $organization->allocations->filter(function (Allocation $other) use ($allocation) {
            return $other->id !== $allocation->id &&
                random_int(0, 1) === 0;
        });
        $allocation->childAllocations()->attach($childAllocations, ['weight' => 1]);
    }
}
