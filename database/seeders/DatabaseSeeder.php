<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        $this->call(OrganizationSeeder::class);
        $this->call(AssetSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(PriceSeeder::class);
        $this->call(TransactionSeeder::class);
        $this->call(AllocationSeeder::class);
        $this->call(ImportConfigurationSeeder::class);
        $this->call(WebhookSeeder::class);
    }
}
