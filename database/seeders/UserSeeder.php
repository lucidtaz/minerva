<?php

namespace Database\Seeders;

use App\Enums\OrganizationUserRole;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $acme = Organization::query()
            ->where('name', '=', 'Acme')
            ->firstOrFail();

        $adminUser = User::query()
            ->where('name', '=', 'admin')
            ->first();
        if ($adminUser === null) {
            $euro = $acme->assets()
                ->where('name', '=', 'EUR')
                ->firstOrFail();

            /** @var User $adminUser */
            $adminUser = User::factory()->create([
                'name' => 'admin',
                'email' => 'admin@example.com',
                'password' => password_hash('admin', PASSWORD_BCRYPT),
                'is_admin' => true,
            ]);

            $adminUser->organizations()->attach($acme, ['role' => OrganizationUserRole::Admin]);
            $adminUser->displayAsset()->associate($euro)->save();
        }

        /** @var User $newUser */
        $newUser = User::factory()->create();
        $randomOrganization = Organization::inRandomOrder()->first();
        $newUser->organizations()->attach($randomOrganization, ['role' => OrganizationUserRole::Member]);
    }
}
