<?php

namespace Database\Seeders;

use App\Enums\EntityType;
use App\Enums\InputContentType;
use App\Enums\InputMethod;
use App\Models\ImportConfiguration;
use App\Models\Organization;
use Illuminate\Database\Seeder;

class ImportConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var Organization $organization */
        $organization = Organization::where('name', '=', 'Acme')
            ->firstOrFail();

        $this->addExportedAssetsCsv($organization);
        $this->addExportedPricesCsv($organization);
        $this->addExportedTransactionsCsv($organization);

        $this->addUsdEurPrices($organization);
        $this->addUsdEurPriceHistory($organization);
        $this->addKrakenBitcoinPrices($organization);
        $this->addKrakenBitcoinTradesCsv($organization);
        $this->addIngTransactionsCsv($organization);
        $this->addIngTransactionPricesCsv($organization);
        $this->addIngPriceJson($organization);
        $this->addBitcoinBlockchainTransactions($organization);
    }

    private function addExportedAssetsCsv(Organization $organization): void
    {
        $importConfigurationName = 'Exported Assets CSV';
        $importConfiguration = $organization->importConfigurations()
            ->where('name', '=', $importConfigurationName)
            ->first();

        if ($importConfiguration !== null) {
            // Nothing to do, it already exists!
            return;
        }

        $importConfiguration = ImportConfiguration::factory()->make([
            'name' => $importConfigurationName,
            'note' => 'Use this import configuration to import asset CSV files exported from this application.',
            'input_method' => InputMethod::Upload,
            'input_content_type' => InputContentType::CSV,
            'date_format' => null,
            'timezone' => 'UTC',
            'url' => null,
            'has_header_row' => true,
            'delimiter' => ',',
            'enclosure_character' => '"',
            'escape_character' => '\\\\',
            'entity_type' => EntityType::Asset,
            'parse_expression' => '[.[] | select(.deleted_at == null or .deleted_at == "") | {uuid: .uuid, name: .name, import_id: .import_id, symbol: .currency_symbol, aliases: (.aliases | split(","))}]',
        ]);
        $organization->importConfigurations()->save($importConfiguration);
    }

    private function addExportedPricesCsv(Organization $organization): void
    {
        $importConfigurationName = 'Exported Prices CSV';
        $importConfiguration = $organization->importConfigurations()
            ->where('name', '=', $importConfigurationName)
            ->first();

        if ($importConfiguration !== null) {
            // Nothing to do, it already exists!
            return;
        }

        $importConfiguration = ImportConfiguration::factory()->make([
            'name' => $importConfigurationName,
            'note' => 'Use this import configuration to import price CSV files exported from this application.',
            'input_method' => InputMethod::Upload,
            'input_content_type' => InputContentType::CSV,
            'date_format' => null,
            'timezone' => 'UTC',
            'url' => null,
            'has_header_row' => true,
            'delimiter' => ',',
            'enclosure_character' => '"',
            'escape_character' => '\\\\',
            'entity_type' => EntityType::Price,
            'parse_expression' => '[.[] | {asset: .asset_uuid, import_id: .import_id, currency: .base_asset_uuid, moment: .moment, value: .value}]',
        ]);
        $organization->importConfigurations()->save($importConfiguration);
    }

    private function addExportedTransactionsCsv(Organization $organization): void
    {
        $importConfigurationName = 'Exported Transactions CSV';
        $importConfiguration = $organization->importConfigurations()
            ->where('name', '=', $importConfigurationName)
            ->first();

        if ($importConfiguration !== null) {
            // Nothing to do, it already exists!
            return;
        }

        $importConfiguration = ImportConfiguration::factory()->make([
            'name' => $importConfigurationName,
            'note' => 'Use this import configuration to import transaction CSV files exported from this application.',
            'input_method' => InputMethod::Upload,
            'input_content_type' => InputContentType::CSV,
            'date_format' => null,
            'timezone' => 'UTC',
            'url' => null,
            'has_header_row' => true,
            'delimiter' => ',',
            'enclosure_character' => '"',
            'escape_character' => '\\\\',
            'entity_type' => EntityType::Transaction,
            'parse_expression' => '[.[] | {asset: .asset_uuid, import_id: .import_id, amount: .amount, location: .location, note: .note, moment: .moment}]',
        ]);
        $organization->importConfigurations()->save($importConfiguration);
    }

    private function addUsdEurPrices(Organization $organization): void
    {
        $importConfigurationName = 'USD EUR Prices';
        $importConfiguration = $organization->importConfigurations()
            ->where('name', '=', $importConfigurationName)
            ->first();

        if ($importConfiguration !== null) {
            // Nothing to do, it already exists!
            return;
        }

        $importConfiguration = ImportConfiguration::factory()->make([
            'name' => $importConfigurationName,
            'input_method' => InputMethod::Download,
            'input_content_type' => InputContentType::JSON,
            'date_format' => null,
            'timezone' => 'UTC',
            'url' => 'https://api.exchangeratesapi.io/latest?symbols=USD&base=EUR',
            'delimiter' => null,
            'enclosure_character' => null,
            'escape_character' => null,
            'entity_type' => EntityType::Price,
            'parse_expression' => '[{asset: "USD", moment: .date, value: .rates.USD, currency: .base}]',
        ]);
        $organization->importConfigurations()->save($importConfiguration);
    }

    private function addUsdEurPriceHistory(Organization $organization): void
    {
        $importConfigurationName = 'USD EUR Price history';
        $importConfiguration = $organization->importConfigurations()
            ->where('name', '=', $importConfigurationName)
            ->first();

        if ($importConfiguration !== null) {
            // Nothing to do, it already exists!
            return;
        }

        $importConfiguration = ImportConfiguration::factory()->make([
            'name' => $importConfigurationName,
            'input_method' => InputMethod::Download,
            'input_content_type' => InputContentType::JSON,
            'date_format' => null,
            'timezone' => 'UTC',
            'url' => 'https://api.exchangeratesapi.io/history?symbols=USD&base=EUR&start_at=2018-01-01&end_at=2020-09-01',
            'delimiter' => null,
            'enclosure_character' => null,
            'escape_character' => null,
            'entity_type' => EntityType::Price,
            'parse_expression' => '[.rates | keys[] as $date | {asset: "USD", moment: $date, value: .[$date].USD, currency: "EUR"}]',
        ]);
        $organization->importConfigurations()->save($importConfiguration);
    }

    private function addKrakenBitcoinPrices(Organization $organization): void
    {
        $importConfigurationName = 'Kraken XBT EUR';
        $importConfiguration = $organization->importConfigurations()
            ->where('name', '=', $importConfigurationName)
            ->first();

        if ($importConfiguration !== null) {
            // Nothing to do, it already exists!
            return;
        }

        $importConfiguration = ImportConfiguration::factory()->make([
            'name' => $importConfigurationName,
            'input_method' => InputMethod::Download,
            'input_content_type' => InputContentType::JSON,
            'date_format' => 'U',
            'timezone' => 'UTC',
            'url' => 'https://api.kraken.com/0/public/OHLC?pair=XXBTZEUR&interval=1440',
            'delimiter' => null,
            'enclosure_character' => null,
            'escape_character' => null,
            'entity_type' => EntityType::Price,
            'parse_expression' => '[.result.XXBTZEUR[] | {asset: "XBT", moment: .[0], value: .[4], currency: "EUR"}]',
        ]);
        $organization->importConfigurations()->save($importConfiguration);
    }

    private function addKrakenBitcoinTradesCsv(Organization $organization): void
    {
        $importConfigurationName = 'Kraken XBT Trades CSV';
        $importConfiguration = $organization->importConfigurations()
            ->where('name', '=', $importConfigurationName)
            ->first();

        if ($importConfiguration !== null) {
            // Nothing to do, it already exists!
            return;
        }

        $importConfiguration = ImportConfiguration::factory()->make([
            'name' => $importConfigurationName,
            'input_method' => InputMethod::Upload,
            'input_content_type' => InputContentType::CSV,
            'date_format' => null,
            'timezone' => 'UTC',
            'url' => null,
            'has_header_row' => true,
            'delimiter' => ',',
            'enclosure_character' => '"',
            'escape_character' => '\\\\',
            'entity_type' => EntityType::Transaction,
            'parse_expression' => '[.[] | select(.txid != "" and .txid != null and .pair == "XXBTZEUR") | {asset: "XBT", moment: .time, amount: ((.vol | tonumber) * (if .type == "sell" then -1 else 1 end)), import_id: .txid, location: "Kraken"}]',
        ]);
        $organization->importConfigurations()->save($importConfiguration);
    }

    private function addIngTransactionsCsv(Organization $organization): void
    {
        $importConfigurationName = 'ING Transactions CSV';
        $importConfiguration = $organization->importConfigurations()
            ->where('name', '=', $importConfigurationName)
            ->first();

        if ($importConfiguration !== null) {
            // Nothing to do, it already exists!
            return;
        }

        $importConfiguration = ImportConfiguration::factory()->make([
            'name' => $importConfigurationName,
            'input_method' => InputMethod::Upload,
            'input_content_type' => InputContentType::CSV,
            'date_format' => null,
            'timezone' => 'Europe/Amsterdam',
            'url' => null,
            'has_header_row' => true,
            'delimiter' => '\t',
            'enclosure_character' => '"',
            'escape_character' => '\\\\',
            'entity_type' => EntityType::Transaction,
            'parse_expression' => '[.[] | select(.Type == "K" or .Type == "V") | {asset: .Naam, moment: .Boekdatum, amount: ((.Aantal | split(",") | join(".") | tonumber) * (if .Type == "K" then 1 else -1 end)), import_id: .Nummer, location: "ING"}]',
        ]);
        $organization->importConfigurations()->save($importConfiguration);
    }

    private function addIngTransactionPricesCsv(Organization $organization): void
    {
        $importConfigurationName = 'ING Transaction Prices CSV';
        $importConfiguration = $organization->importConfigurations()
            ->where('name', '=', $importConfigurationName)
            ->first();

        if ($importConfiguration !== null) {
            // Nothing to do, it already exists!
            return;
        }

        $importConfiguration = ImportConfiguration::factory()->make([
            'name' => $importConfigurationName,
            'input_method' => InputMethod::Upload,
            'input_content_type' => InputContentType::CSV,
            'date_format' => null,
            'timezone' => 'Europe/Amsterdam',
            'url' => null,
            'has_header_row' => true,
            'delimiter' => '\t',
            'enclosure_character' => '"',
            'escape_character' => '\\\\',
            'entity_type' => EntityType::Price,
            'parse_expression' => '[.[] | select(.Type == "K" or .Type == "V") | {asset: .Naam, moment: .Boekdatum, value: ((.Koers | split(",") | join(".") | tonumber) / (if .Wisselkoers == "" then 1 else (.Wisselkoers | split(",") | join(".") | tonumber) end)), currency: ."Koers Valuta", import_id: .Nummer}]',
        ]);
        $organization->importConfigurations()->save($importConfiguration);
    }

    private function addIngPriceJson(Organization $organization): void
    {
        $importConfigurationName = 'ING Prices - S&P 500';
        $importConfiguration = $organization->importConfigurations()
            ->where('name', '=', $importConfigurationName)
            ->first();

        if ($importConfiguration !== null) {
            // Nothing to do, it already exists!
            return;
        }

        $importConfiguration = ImportConfiguration::factory()->make([
            'name' => $importConfigurationName,
            'input_method' => InputMethod::Download,
            'input_content_type' => InputContentType::JSON,
            'date_format' => null,
            'timezone' => 'UTC',
            'url' => 'https://www.ing.nl/open/particulier/beleggen/v1/api/instruments?query=IE00B3WJKG14',
            'has_header_row' => false,
            'delimiter' => null,
            'enclosure_character' => null,
            'escape_character' => null,
            'entity_type' => EntityType::Price,
            'parse_expression' => '[.results[] | {asset: "iShares S&P 500 Information Tech ETF", moment: .price.time, value: .price.price, currency: .price.currency}]',
        ]);
        $organization->importConfigurations()->save($importConfiguration);
    }

    private function addBitcoinBlockchainTransactions(Organization $organization): void
    {
        $importConfigurationName = 'XBT blockchain transactions';
        $importConfiguration = $organization->importConfigurations()
            ->where('name', '=', $importConfigurationName)
            ->first();

        if ($importConfiguration !== null) {
            // Nothing to do, it already exists!
            return;
        }

        $importConfiguration = ImportConfiguration::factory()->make([
            'name' => $importConfigurationName,
            'input_method' => InputMethod::Download,
            'input_content_type' => InputContentType::JSON,
            'date_format' => 'U',
            'timezone' => 'UTC',
            'url' => 'https://blockchain.info/multiaddr?active=$addr1%7C$addr2%7C...',
            'delimiter' => null,
            'enclosure_character' => null,
            'escape_character' => null,
            'entity_type' => EntityType::Transaction,
            'parse_expression' => '[.txs[] | {asset: "XBT", moment: .time, amount: (.result / 100000000), import_id: .hash, location: "Local wallet"}]',
        ]);
        $organization->importConfigurations()->save($importConfiguration);
    }
}
