<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeAssetForeignKeysToCascadingDelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_aliases', function (Blueprint $table) {
            $table->dropForeign('asset_aliases_asset_id_foreign');
            $table->foreign('asset_id')->references('id')->on('assets')->cascadeOnDelete();
        });
        Schema::table('prices', function (Blueprint $table) {
            $table->dropForeign('prices_base_asset_id_foreign');
            $table->foreign('base_asset_id')->references('id')->on('assets')->cascadeOnDelete();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_display_asset_id_foreign');
            $table->foreign('display_asset_id')->references('id')->on('assets')->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_aliases', function (Blueprint $table) {
            $table->dropForeign('asset_aliases_asset_id_foreign');
            $table->foreign('asset_id')->references('id')->on('assets');
        });
        Schema::table('prices', function (Blueprint $table) {
            $table->dropForeign('prices_base_asset_id_foreign');
            $table->foreign('base_asset_id')->references('id')->on('assets');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_display_asset_id_foreign');
            $table->foreign('display_asset_id')->references('id')->on('assets');
        });
    }
}
