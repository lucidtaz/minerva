<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImportConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_configurations', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('input_method');
            $table->string('input_content_type');
            $table->string('date_format')->nullable();
            $table->string('url')->nullable();
            $table->boolean('has_header_row')->nullable();
            $table->string('delimiter')->nullable();
            $table->string('enclosure_character')->nullable();
            $table->string('escape_character')->nullable();
            $table->string('entity_type');
            $table->string('parse_expression');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_configurations');
    }
}
