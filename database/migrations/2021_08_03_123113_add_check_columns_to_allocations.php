<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCheckColumnsToAllocations extends Migration
{
    public function up(): void
    {
        Schema::table('allocations', function (Blueprint $table) {
            $table->time('check_at')->nullable();
            $table->string('check_timezone')->nullable();
            $table->foreignId('check_currency_id')->nullable()->references('id')->on('assets');
            $table->foreignId('check_webhook_id')->nullable()->references('id')->on('webhooks');
        });
    }

    public function down(): void
    {
        Schema::table('allocations', function (Blueprint $table) {
            $table->dropColumn('check_at');
            $table->dropColumn('check_timezone');
            $table->dropColumn('check_currency_id');
            $table->dropColumn('check_webhook_id');
        });
    }
}
