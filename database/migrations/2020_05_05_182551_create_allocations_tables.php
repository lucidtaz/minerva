<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAllocationsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('allocations', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->decimal('tolerance', 2, 0); // Percentage without decimals
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('allocatables', function (Blueprint $table) {
            $table->id();
            $table->foreignId('allocation_id')->references('id')->on('allocations')->cascadeOnDelete();
            $table->foreignId('allocatable_id');
            $table->string('allocatable_type');
            $table->integer('weight')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('allocatables');
        Schema::dropIfExists('allocations');
    }
}
