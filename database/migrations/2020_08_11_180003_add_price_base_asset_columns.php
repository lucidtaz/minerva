<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPriceBaseAssetColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prices', function (Blueprint $table) {
            $table->foreignId('base_asset_id')
                ->nullable()
                ->default(null)
                ->references('id')
                ->on('assets');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreignId('display_asset_id')
                ->nullable()
                ->default(null)
                ->references('id')
                ->on('assets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('display_asset_id');
        });

        Schema::table('prices', function (Blueprint $table) {
            $table->dropColumn('base_asset_id');
        });
    }
}
