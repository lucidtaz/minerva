<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAssetCurrencySymbol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->text('currency_symbol')->nullable()->index();
        });

        DB::table('assets')
            ->where('name', '=', 'EUR')
            ->update([
                'currency_symbol' => '€',
                'updated_at' => Carbon::now()->toIso8601String(),
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->dropColumn('currency_symbol');
        });
    }
}
