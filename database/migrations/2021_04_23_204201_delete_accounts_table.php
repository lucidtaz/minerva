<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeleteAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('accounts');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->foreignId('organization_id')->references('id')->on('organizations')->cascadeOnDelete();
            $table->foreignId('asset_id')->references('id')->on('assets')->cascadeOnDelete();
            $table->string('name');
            $table->foreignId('import_execution_id')->references('id')->on('import_executions')->cascadeOnDelete();
            $table->string('import_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
