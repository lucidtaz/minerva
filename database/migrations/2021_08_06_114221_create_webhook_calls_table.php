<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebhookCallsTable extends Migration
{
    public function up(): void
    {
        Schema::create('webhook_calls', function (Blueprint $table) {
            $table->id();
            $table->foreignId('webhook_id')->references('id')->on('webhooks')->cascadeOnDelete();
            $table->text('request');
            $table->text('response')->nullable();
            $table->integer('status_code')->nullable();
            $table->text('error')->nullable();
            $table->integer('duration_ms');
            $table->timestamps();

            $table->index(['webhook_id', 'created_at']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('webhook_calls');
    }
}
