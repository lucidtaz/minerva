<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveImportIdUniqueness extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prices', function (Blueprint $table) {
            $table->dropUnique('prices_import_id_unique');
        });
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropUnique('transactions_import_id_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prices', function (Blueprint $table) {
            $table->unique('import_id');
        });
        Schema::table('transactions', function (Blueprint $table) {
            $table->unique('import_id');
        });
    }
}
