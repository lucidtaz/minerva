<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImportExecutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_executions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('import_configuration_id')->references('id')->on('import_configurations');
            $table->text('log');
            $table->integer('entities_created');
            $table->integer('warnings')->default(0);
            $table->integer('errors')->default(0);
            $table->timestamps();
        });

        Schema::table('assets', function (Blueprint $table) {
            $table->foreign('import_execution_id', 'fk_assets_import_execution_id')
                ->references('id')
                ->on('import_executions')
                ->cascadeOnDelete();
        });

        Schema::table('prices', function (Blueprint $table) {
            $table->foreign('import_execution_id', 'fk_prices_import_execution_id')
                ->references('id')
                ->on('import_executions')
                ->cascadeOnDelete();
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->foreign('import_execution_id', 'fk_transactions_import_execution_id')
                ->references('id')
                ->on('import_executions')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropForeign('fk_transactions_import_execution_id');
        });

        Schema::table('prices', function (Blueprint $table) {
            $table->dropForeign('fk_prices_import_execution_id');
        });

        Schema::table('assets', function (Blueprint $table) {
            $table->dropForeign('fk_assets_import_execution_id');
        });

        Schema::dropIfExists('import_executions');
    }
}
