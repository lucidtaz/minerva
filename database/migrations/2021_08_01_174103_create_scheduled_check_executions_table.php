<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduledCheckExecutionsTable extends Migration
{
    public function up(): void
    {
        Schema::create('scheduled_check_executions', function (Blueprint $table) {
            $table->id();
            $table->string('command');
            $table->dateTime('time_from');
            $table->dateTime('time_to');
            $table->boolean('finished');
            $table->string('outcome');
            $table->timestamps();

            $table->index(['command', 'time_to', 'finished']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('scheduled_check_executions');
    }
}
