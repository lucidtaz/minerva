<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDebugToWebhookCallsTable extends Migration
{
    public function up(): void
    {
        Schema::table('webhook_calls', function (Blueprint $table) {
            $table->text('debug')->nullable();
        });
    }

    public function down(): void
    {
        Schema::table('webhook_calls', function (Blueprint $table) {
            $table->dropColumn('debug');
        });
    }
}
