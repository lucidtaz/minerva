<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAssetAliasOrganizationOwnership extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $organization = DB::table('organizations')
            ->select(['id'])
            ->first();

        Schema::table('asset_aliases', function (Blueprint $table) use ($organization) {
            $table->foreignId('organization_id')
                ->default($organization !== null ? $organization->id : null)
                ->references('id')
                ->on('organizations')
                ->cascadeOnDelete();

        });

        Schema::table('asset_aliases', function (Blueprint $table) {
            $table->dropUnique(['alias']);

            $table->unique(['organization_id', 'alias']);

            $table->foreignId('organization_id')
                ->default(null)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_aliases', function (Blueprint $table) {
            $table->dropUnique(['organization_id', 'alias']);

            $table->dropColumn('organization_id');

            $table->unique(['alias']);
        });
    }
}
