<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEntityOrganizationOwnership extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $organization = DB::table('organizations')
            ->select(['id'])
            ->first();

        Schema::table('allocations', function (Blueprint $table) use ($organization) {
            $table->foreignId('organization_id')
                ->default($organization !== null ? $organization->id : null)
                ->references('id')
                ->on('organizations')
                ->cascadeOnDelete();
        });
        Schema::table('assets', function (Blueprint $table) use ($organization) {
            $table->foreignId('organization_id')
                ->default($organization !== null ? $organization->id : null)
                ->references('id')
                ->on('organizations')
                ->cascadeOnDelete();
        });
        Schema::table('import_configurations', function (Blueprint $table) use ($organization) {
            $table->foreignId('organization_id')
                ->default($organization !== null ? $organization->id : null)
                ->references('id')
                ->on('organizations')
                ->cascadeOnDelete();
        });
        Schema::table('invitations', function (Blueprint $table) use ($organization) {
            $table->foreignId('organization_id')
                ->default($organization !== null ? $organization->id : null)
                ->nullable()
                ->references('id')
                ->on('organizations')
                ->cascadeOnDelete();
        });

        Schema::table('allocations', function (Blueprint $table) {
            $table->foreignId('organization_id')
                ->default(null)
                ->change();
        });
        Schema::table('assets', function (Blueprint $table) {
            $table->foreignId('organization_id')
                ->default(null)
                ->change();
        });
        Schema::table('import_configurations', function (Blueprint $table) {
            $table->foreignId('organization_id')
                ->default(null)
                ->change();
        });
        Schema::table('invitations', function (Blueprint $table) {
            $table->foreignId('organization_id')
                ->default(null)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invitations', function (Blueprint $table) {
            $table->dropColumn('organization_id');
        });
        Schema::table('import_configurations', function (Blueprint $table) {
            $table->dropColumn('organization_id');
        });
        Schema::table('assets', function (Blueprint $table) {
            $table->dropColumn('organization_id');
        });
        Schema::table('allocations', function (Blueprint $table) {
            $table->dropColumn('organization_id');
        });
    }
}
