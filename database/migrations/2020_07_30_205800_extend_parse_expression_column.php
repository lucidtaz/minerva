<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ExtendParseExpressionColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('import_configurations', function (Blueprint $table) {
            $table->string('parse_expression', 4095)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('UPDATE import_configurations SET parse_expression = SUBSTRING(parse_expression for 255);');
        Schema::table('import_configurations', function (Blueprint $table) {
            $table->string('parse_expression', 255)->change();
        });
    }
}
