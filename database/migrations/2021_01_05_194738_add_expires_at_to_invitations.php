<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExpiresAtToInvitations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invitations', function (Blueprint $table) {
            $table->dateTime('expires_at')->nullable();
        });
        DB::statement(
            "UPDATE invitations SET expires_at = created_at + :expire * INTERVAL '1 day';",
            [':expire' => config('app.invitation_expire_days')]
        );
        Schema::table('invitations', function (Blueprint $table) {
            $table->dateTime('expires_at')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invitations', function (Blueprint $table) {
            $table->dropColumn('expires_at');
        });
    }
}
