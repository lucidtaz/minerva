<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('/allocations/{allocation}/check', 'AllocationCheckController@check')
        ->name('api.allocations.check');

    Route::post('/imports/{importConfiguration}/run', 'ImportController@run')
        ->name('api.imports.run');

    Route::post('/exports/{organization}/run', 'ExportController@runApi')
        ->name('api.exports.run');

    Route::get('/me', 'Auth\UserController@me')
        ->name('api.users.me');
});
