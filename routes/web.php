<?php

use App\Enums\AllocationViewMode;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'view.variables'], function () {
    Auth::routes();
});

Route::group(['middleware' => ['auth', 'organization', 'view.variables']], function () {
    Route::get('/', 'HomeController@index')
        ->name('home');

    Route::get('/invitations', 'InvitationController@index')
        ->name('invitations.index');
    Route::get('/invitations/create', 'InvitationController@create')
        ->name('invitations.create');
    Route::post('/invitations', 'InvitationController@store')
        ->name('invitations.store');
    Route::delete('/invitations/{maybeExpiredInvitation}', 'InvitationController@delete')
        ->name('invitations.delete');

    Route::get('/preferences', 'PreferencesController@edit')
        ->name('preferences.edit');
    Route::put('/preferences', 'PreferencesController@update')
        ->name('preferences.update');

    Route::get('/organizations', 'OrganizationController@index')
        ->name('organizations.index');
    Route::get('/organizations/create', 'OrganizationController@create')
        ->name('organizations.create');
    Route::get('/organizations/deleted', 'OrganizationController@deleted')
        ->name('organizations.deleted');
    Route::get('/organizations/{organization}', 'OrganizationController@show')
        ->name('organizations.show');
    Route::get('/organizations/{organization}/edit', 'OrganizationController@edit')
        ->name('organizations.edit');
    Route::post('/organizations', 'OrganizationController@store')
        ->name('organizations.store');
    Route::post('/organizations/{trashedOrganization}/restore', 'OrganizationController@restore')
        ->name('organizations.restore');
    Route::post('/organizations/{organization}/switch', 'OrganizationController@switch')
        ->name('organizations.switch');
    Route::put('/organizations/{organization}', 'OrganizationController@update')
        ->name('organizations.update');
    Route::delete('/organizations/{organization}', 'OrganizationController@delete')
        ->name('organizations.delete');

    Route::get('/webhooks', 'WebhookController@index')
        ->name('webhooks.index');
    Route::get('/webhooks/create', 'WebhookController@create')
        ->name('webhooks.create');
    Route::get('/webhooks/{webhook}/edit', 'WebhookController@edit')
        ->name('webhooks.edit');
    Route::post('/webhooks', 'WebhookController@store')
        ->name('webhooks.store');
    Route::put('/webhooks/{webhook}', 'WebhookController@update')
        ->name('webhooks.update');
    Route::delete('/webhooks/{webhook}', 'WebhookController@delete')
        ->name('webhooks.delete');
    Route::post('/webhooks/{webhook}/test', 'WebhookController@test')
        ->name('webhooks.test');

    Route::get('/webhooks/{webhook}', 'WebhookCallController@index')
        ->name('webhookcalls.index');
    Route::get('/webhooks/{webhook}/calls/{call}', 'WebhookCallController@show')
        ->name('webhookcalls.show');

    Route::put('/token', 'Auth\ApiTokenController@update')->name('token.update');

    Route::get('/assets', 'AssetController@index')
        ->name('assets.index');
    Route::get('/assets/create', 'AssetController@create')
        ->name('assets.create');
    Route::get('/assets/deleted', 'AssetController@deleted')
        ->name('assets.deleted');
    Route::get('/assets/{asset:uuid}', 'AssetController@show')
        ->name('assets.show');
    Route::post('/assets', 'AssetController@store')
        ->name('assets.store');
    Route::get('/assets/{asset:uuid}/edit', 'AssetController@edit')
        ->name('assets.edit');
    Route::put('/assets/{asset:uuid}', 'AssetController@update')
        ->name('assets.update');
    Route::delete('/assets/{asset:uuid}', 'AssetController@delete')
        ->name('assets.delete');
    Route::post('/assets/{trashedAsset:uuid}/restore', 'AssetController@restore')
        ->name('assets.restore');
    Route::post('/assets/{trashedAsset:uuid}/forceDelete', 'AssetController@forceDelete')
        ->name('assets.forceDelete');

    Route::get('/assets/{asset:uuid}/prices/create', 'PriceController@create')
        ->name('prices.create');
    Route::post('/assets/{asset:uuid}/prices', 'PriceController@store')
        ->name('prices.store');
    Route::delete('/assets/{asset:uuid}/prices/{price}', 'PriceController@delete')
        ->name('prices.delete');

    Route::get('/assets/{asset:uuid}/transactions/create', 'TransactionController@create')
        ->name('transactions.create');
    Route::post('/assets/{asset:uuid}/transactions', 'TransactionController@store')
        ->name('transactions.store');
    Route::get('/assets/{asset:uuid}/transactions/{transaction}/edit', 'TransactionController@edit')
        ->name('transactions.edit');
    Route::put('/assets/{asset:uuid}/transactions/{transaction}', 'TransactionController@update')
        ->name('transactions.update');
    Route::delete('/assets/{asset:uuid}/transactions/{transaction}', 'TransactionController@delete')
        ->name('transactions.delete');

    Route::get('/assets/{asset:uuid}/values/create', 'ValueController@create')
        ->name('values.create');
    Route::post('/assets/{asset:uuid}/values', 'ValueController@store')
        ->name('values.store');

    Route::get('/allocations', 'AllocationController@index')
        ->name('allocations.index');
    Route::get('/allocations/create', 'AllocationController@create')
        ->name('allocations.create');
    Route::get('/allocations/deleted', 'AllocationController@deleted')
        ->name('allocations.deleted');
    Route::get('/allocations/{allocation}/{view}', 'AllocationController@show')
        ->where('view', implode('|', array_map(fn(AllocationViewMode $x) => $x->value, AllocationViewMode::cases())))
        ->name('allocations.show');
    Route::get('/allocations/{allocation}/value', 'AllocationController@value')
        ->name('allocations.value');
    Route::get('/allocations/{allocation}/edit', 'AllocationController@edit')
        ->name('allocations.edit');
    Route::post('/allocations', 'AllocationController@store')
        ->name('allocations.store');
    Route::post('/allocations/{trashedAllocation}/restore', 'AllocationController@restore')
        ->name('allocations.restore');
    Route::put('/allocations/{allocation}', 'AllocationController@update')
        ->name('allocations.update');
    Route::delete('/allocations/{allocation}', 'AllocationController@delete')
        ->name('allocations.delete');

    Route::get('/allocations/{allocation}/check/edit', 'AllocationCheckController@edit')
        ->name('allocations.check.edit');
    Route::put('/allocations/{allocation}/check', 'AllocationCheckController@update')
        ->name('allocations.check.update');
    Route::delete('/allocations/{allocation}/check', 'AllocationCheckController@delete')
        ->name('allocations.check.delete');

    Route::get('/imports', 'ImportController@index')
        ->name('imports.index');
    Route::get('/imports/create', 'ImportController@create')
        ->name('imports.create');
    Route::get('/imports/deleted', 'ImportController@deleted')
        ->name('imports.deleted');
    Route::get('/imports/{importConfiguration}', 'ImportController@show')
        ->name('imports.show');
    Route::get('/imports/{importConfiguration}/edit', 'ImportController@edit')
        ->name('imports.edit');
    Route::post('/imports', 'ImportController@store')
        ->name('imports.store');
    Route::post('/imports/{trashedImportConfiguration}/restore', 'ImportController@restore')
        ->name('imports.restore');
    Route::post('/imports/{importConfiguration}/run', 'ImportController@run')
        ->name('imports.run');
    Route::put('/imports/{importConfiguration}', 'ImportController@update')
        ->name('imports.update');
    Route::delete('/imports/{importConfiguration}', 'ImportController@delete')
        ->name('imports.delete');

    Route::get('/executions', 'ExecutionController@index')
        ->name('executions.index');
    Route::get('/executions/{execution}', 'ExecutionController@show')
        ->name('executions.show');
    Route::delete('/executions/{execution}', 'ExecutionController@delete')
        ->name('executions.delete');

    Route::get('/exports', 'ExportController@index')
        ->name('exports.index');
    Route::get('/exports/create', 'ExportController@create')
        ->name('exports.create');
    Route::post('/exports/run', 'ExportController@run')
        ->name('exports.run');

    Route::post('/display/graph', 'Display\DisplayController@setGraphControls')
        ->name('display.graph');

    Route::get('/notifications', 'NotificationController@index')
        ->name('notifications.index');
    Route::get('/notifications/{notification}', 'NotificationController@show')
        ->name('notifications.show');

    Route::group(['prefix' => '/admin', 'middleware' => ['can:admin-actions']], function () {
        Route::get('/', 'Admin\HomeController@index')
            ->name('admin.home');

        Route::get('/users', 'Admin\UserController@index')
            ->name('admin.users.index');
        Route::get('/users/{user}', 'Admin\UserController@show')
            ->name('admin.users.show');
        Route::post('/users/{user}/promote', 'Admin\UserController@promote')
            ->name('admin.users.promote');
        Route::post('/users/{user}/demote', 'Admin\UserController@demote')
            ->name('admin.users.demote');

        Route::get('/scheduled-checks/executions', 'Admin\ScheduledChecksController@index')
            ->name('admin.scheduled-checks.index');
    });
});
