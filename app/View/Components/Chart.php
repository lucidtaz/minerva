<?php

namespace App\View\Components;

use App\Dtos\DataSet;
use App\Http\Controllers\Display\RemembersDisplaySettings;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Illuminate\Support\Enumerable;
use Illuminate\View\Component;
use Request;

class Chart extends Component
{
    use RemembersDisplaySettings;

    private string $id;

    /**
     * @var Enumerable&DataSet[]
     */
    private Enumerable $dataSets;

    private bool $stacked;

    /**
     * @param Enumerable&DataSet[]|DataSet $dataSets
     */
    public function __construct($dataSets, bool $stacked = false)
    {
        if ($dataSets instanceof DataSet) {
            $dataSets = new Collection([$dataSets]);
        }

        $this->id = uniqid('chart', false);
        $this->dataSets = $dataSets;
        $this->stacked = $stacked;
    }

    public function render(): View
    {
        return view('components.chart', [
            'id' => $this->id,
            'dataSets' => $this->dataSets,
            'stacked' => $this->stacked,
            'controls' => $this->getGraphControls(Request::session(), Request::user()->timezone)
        ]);
    }
}
