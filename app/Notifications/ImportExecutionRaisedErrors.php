<?php

namespace App\Notifications;

use App\Models\ImportExecution;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class ImportExecutionRaisedErrors extends Notification implements FormattableNotification
{
    use Queueable;

    public function __construct(private ImportExecution $execution)
    {
        //
    }

    public function via($notifiable)
    {
        return ['database'];
    }

    public function toArray($notifiable)
    {
        return [
            'import_configuration_name' => $this->execution->importConfiguration->name,
        ];
    }

    public static function formatMessage(array $data): string
    {
        return 'Execution raised errors for import configuration "' . ($data['import_configuration_name'] ?? 'unknown') . '"';
    }
}
