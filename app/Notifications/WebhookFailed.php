<?php

namespace App\Notifications;

use App\Models\WebhookCall;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class WebhookFailed extends Notification implements FormattableNotification
{
    use Queueable;

    public function __construct(private WebhookCall $call)
    {
        //
    }

    public function via($notifiable)
    {
        return ['database'];
    }

    public function toArray($notifiable)
    {
        return [
            'webhook_id' => $this->call->webhook_id,
            'webhookcall_id' => $this->call->id,
            'webhook_name' => $this->call->webhook->name,
        ];
    }

    public static function formatMessage(array $data): string
    {
        return 'Delivery failed for webhook "' . ($data['webhook_name'] ?? 'unknown') . '"';
    }
}
