<?php

namespace App\Notifications;

interface FormattableNotification
{
    public static function formatMessage(array $data): string;
}
