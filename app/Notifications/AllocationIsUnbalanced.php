<?php

namespace App\Notifications;

use App\Models\Allocation;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class AllocationIsUnbalanced extends Notification implements FormattableNotification
{
    use Queueable;

    public function __construct(private Allocation $allocation)
    {
        //
    }

    public function via($notifiable)
    {
        return ['database'];
    }

    public function toArray($notifiable)
    {
        return [
            'allocation_name' => $this->allocation->name,
        ];
    }

    public static function formatMessage(array $data): string
    {
        return 'Allocation "' . ($data['allocation_name'] ?? 'unknown') . '" needs rebalancing.';
    }
}
