<?php

namespace App\Presentation;

use App\Notifications\FormattableNotification;
use Illuminate\Notifications\DatabaseNotification;

class NotificationPresenter
{
    public function formatMessage(DatabaseNotification $notification): string
    {
        $class = $notification->getAttribute('type');
        if (!class_exists($class)) {
            return 'Unable to format notification message: unknown type';
        }

        if (!is_subclass_of($class, FormattableNotification::class)) {
            return 'Unable to format notification message: not formattable';
        }

        return $class::formatMessage($notification->getAttribute('data'));
    }
}
