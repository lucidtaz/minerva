<?php

namespace App\Presentation;

class NumberDisplayOptions
{
    public function __construct(
        public readonly int $amountDecimalPlaces,
        public readonly int $priceDecimalPlaces,
        public readonly int $valueDecimalPlaces,
        public readonly int $fractionDecimalPlaces
    ) {
        //
    }
}
