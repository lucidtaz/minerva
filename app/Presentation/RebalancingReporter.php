<?php

namespace App\Presentation;

use App\Dtos\AllocationLine;
use App\Dtos\RenderedAllocation;
use Carbon\Carbon;
use Carbon\CarbonInterface;
use Carbon\CarbonInterval;
use Config;
use Illuminate\Support\Collection;

class RebalancingReporter
{
    private CarbonInterval $priceAgeWarningInterval;

    private NumberDisplayOptions $numberDisplayOptions;

    public function __construct()
    {
        $this->priceAgeWarningInterval = CarbonInterval::months(Config::get('app.price_age_warning_interval_months'));
        $this->numberDisplayOptions = new NumberDisplayOptions(2, 2, 2, 0);
    }

    /**
     * @return string[]
     */
    public function formatSummaryTextLines(RenderedAllocation $renderedAllocation): array
    {
        $lines = $this->flattenAllocationLines($renderedAllocation)
            ->map(
                function (AllocationLine $line): string {
                    $currentSharePercent = number_format(bcmul($line->currentShare, 100), $this->numberDisplayOptions->fractionDecimalPlaces) . '%';
                    $target = number_format(bcmul($line->targetShare, 100), $this->numberDisplayOptions->fractionDecimalPlaces) . '%';

                    if ($line->deltaAmount === null) {
                        // For lines that may have no amounts. (i.e. sub-Allocations)
                        return "$line->name $currentSharePercent: target is $target";
                    }

                    $buySell = '';
                    if (bccomp($line->deltaAmount, '0') === -1) {
                        $buySell = 'sell ' . number_format(bcmul($line->deltaAmount, '-1'), $this->numberDisplayOptions->amountDecimalPlaces);
                    } elseif (bccomp($line->deltaAmount, '0') === 1) {
                        $buySell = 'buy ' . number_format($line->deltaAmount, $this->numberDisplayOptions->amountDecimalPlaces);
                    }

                    return "$line->name $currentSharePercent: $buySell to get to $target";
                }
            )
            ->values()
            ->all();

        $lines[] = sprintf(
            'Total value deviation: %s%s (%s%%)',
            $renderedAllocation->totalDeviation->baseAsset->currency_symbol,
            number_format($renderedAllocation->totalDeviation->value, $this->numberDisplayOptions->valueDecimalPlaces),
            number_format($renderedAllocation->totalDeviationRatio * 100, $this->numberDisplayOptions->fractionDecimalPlaces),
        );

        $priceAge = $renderedAllocation->sum->priceAge;
        if ($priceAge->isBefore(Carbon::now()->sub($this->priceAgeWarningInterval))) {
            $lines[] = 'Warning: prices are up to ' . $priceAge->diffForHumans(null, CarbonInterface::DIFF_ABSOLUTE) . ' old!';
        }

        return $lines;
    }

    /**
     * @return Collection&AllocationLine[]
     */
    private function flattenAllocationLines(RenderedAllocation $renderedAllocation): Collection {
        return collect($renderedAllocation->lines)
            ->flatMap(fn (AllocationLine $allocationLine) => [$allocationLine, ...$allocationLine->subLines]);
    }
}
