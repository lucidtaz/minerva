<?php

namespace App\Jobs;

use App\Models\Webhook;
use App\Models\WebhookCall;
use App\Notifications\WebhookFailed;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Message;
use GuzzleHttp\Psr7\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;

class SendWebhook implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public int $tries = 5;

    private string $httpMessage;

    public function __construct(private Webhook $webhook, string $payload, string $contentType)
    {
        $signature = hash_hmac('sha256', $payload, $webhook->shared_secret);
        $request = (new Request(
            'POST',
            $webhook->uri,
            ['Content-Type' => $contentType, 'X-Webhook-Signature' => $signature, 'User-Agent' => config('app.name')],
            $payload
        ))->withRequestTarget($webhook->uri); // https://github.com/guzzle/psr7/issues/106
        $this->httpMessage = Message::toString($request);
    }

    /**
     * @throws GuzzleException
     */
    public function handle(Client $client): void
    {
        $request = Message::parseRequest($this->httpMessage);

        $call = new WebhookCall([
            'request' => $this->httpMessage,
        ]);

        $debug = fopen('php://memory','wb+');

        $response = null;

        $start = microtime(true);
        try {
            $response = $client->send($request, ['debug' => $debug]);
            $error = null;
            $shortError = null;
        } catch (BadResponseException $e) {
            $response = $e->getResponse();
            $error = $e->getMessage();
            $shortError = 'HTTP status code ' . $response->getStatusCode();
            throw $e;
        } catch (ConnectException $e) {
            $error = $e->getMessage();
            $shortError = 'Could not connect';
            throw $e;
        } catch (GuzzleException $e) {
            $error = $e->getMessage();
            $shortError = 'Communication problem';
            throw $e;
        } finally {
            $call->debug = mb_convert_encoding(stream_get_contents($debug, offset: 0), 'UTF-8', 'ISO-8859-1');
            $call->response = $response !== null ? Message::toString($response) : null;
            $call->status_code = $response !== null ? $response->getStatusCode() : null;
            $call->duration_ms = round(1000 * (microtime(true) - $start));
            $call->error = $error;
            $call->short_error = $shortError;

            if ($this->webhook->exists) {
                $this->webhook->calls()->save($call);
            }

            $this->notifyOnFailedWebhookCall($call);

            fclose($debug);
        }
    }

    private function notifyOnFailedWebhookCall(WebhookCall $call): void
    {
        if ($call->error === null) {
            return;
        }

        if ($this->attempts() < $this->tries) {
            // There are still retries left
            return;
        }

        $users = $call->webhook->organization->users;
        Notification::send($users, new WebhookFailed($call));
    }

    public function backoff(): array
    {
        $result = [];
        $base = 60;
        for ($i = 0; $i < $this->tries; $i++) {
            $exponentialBackoffDelay = $base * (2 ** $i);
            try {
                $result[] = random_int(0, $exponentialBackoffDelay);
            } catch (Exception) {
                // Not enough entropy, no problem for us
                $result[] = $exponentialBackoffDelay;
            }
        }
        return $result;
    }
}
