<?php

namespace App\Policies;

use App\Models\Allocation;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class AllocationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models. (list)
     */
    public function viewAny(User $user, Organization $organization): Response
    {
        return $user->hasAnyRoleInside($organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Allocation $allocation): Response
    {
        return $user->hasAnyRoleInside($allocation->organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user, Organization $organization): Response
    {
        return $user->hasAnyRoleInside($organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Allocation $allocation): Response
    {
        return $user->hasAnyRoleInside($allocation->organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Allocation $allocation): Response
    {
        return $user->hasAnyRoleInside($allocation->organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, Allocation $allocation): Response
    {
        return $user->hasAnyRoleInside($allocation->organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can restore check (for unbalance) all allocation models of the organization.
     */
    public function check(User $user, Allocation $allocation): Response
    {
        return $user->hasAnyRoleInside($allocation->organization)
            ? $this->allow()
            : $this->deny();
    }
}
