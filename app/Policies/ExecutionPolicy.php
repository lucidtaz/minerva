<?php

namespace App\Policies;

use App\Models\ImportExecution;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ExecutionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models. (list)
     */
    public function viewAny(User $user, Organization $organization): Response
    {
        return $user->hasAnyRoleInside($organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, ImportExecution $execution): Response
    {
        return $user->hasAnyRoleInside($execution->importConfiguration->organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, ImportExecution $execution): Response
    {
        return $user->hasAnyRoleInside($execution->importConfiguration->organization)
            ? $this->allow()
            : $this->deny();
    }
}
