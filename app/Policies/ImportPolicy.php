<?php

namespace App\Policies;

use App\Models\ImportConfiguration;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ImportPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models. (list)
     */
    public function viewAny(User $user, Organization $organization): Response
    {
        return $user->hasAnyRoleInside($organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, ImportConfiguration $importConfiguration): Response
    {
        return $user->hasAnyRoleInside($importConfiguration->organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user, Organization $organization): Response
    {
        return $user->hasAnyRoleInside($organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, ImportConfiguration $importConfiguration): Response
    {
        return $user->hasAnyRoleInside($importConfiguration->organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, ImportConfiguration $importConfiguration): Response
    {
        return $user->hasAnyRoleInside($importConfiguration->organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, ImportConfiguration $importConfiguration): Response
    {
        return $user->hasAnyRoleInside($importConfiguration->organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can run the import configuration.
     */
    public function run(User $user, ImportConfiguration $importConfiguration): Response
    {
        return $user->hasAnyRoleInside($importConfiguration->organization)
            ? $this->allow()
            : $this->deny();
    }
}
