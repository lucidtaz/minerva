<?php

namespace App\Policies;

use App\Models\Asset;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class AssetPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models. (list)
     */
    public function viewAny(User $user, Organization $organization): Response
    {
        return $user->hasAnyRoleInside($organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Asset $asset): Response
    {
        return $user->hasAnyRoleInside($asset->organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user, Organization $organization): Response
    {
        return $user->hasAnyRoleInside($organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Asset $asset): Response
    {
        return $user->hasAnyRoleInside($asset->organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Asset $asset): Response
    {
        return $user->hasAnyRoleInside($asset->organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, Asset $asset): Response
    {
        return $user->hasAnyRoleInside($asset->organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(User $user, Asset $asset): Response
    {
        return $user->hasAnyRoleInside($asset->organization)
            ? $this->allow()
            : $this->deny();
    }
}
