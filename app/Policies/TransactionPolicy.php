<?php

namespace App\Policies;

use App\Models\Asset;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class TransactionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Transaction $transaction): Response
    {
        return $user->hasAnyRoleInside($transaction->asset->organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user, Asset $asset): Response
    {
        return $user->hasAnyRoleInside($asset->organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Transaction $transaction): Response
    {
        return $user->hasAnyRoleInside($transaction->asset->organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Transaction $transaction): Response
    {
        return $user->hasAnyRoleInside($transaction->asset->organization)
            ? $this->allow()
            : $this->deny();
    }
}
