<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Webhook;
use App\Models\WebhookCall;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class WebhookCallPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models. (list)
     */
    public function viewAny(User $user, Webhook $webhook): Response
    {
        return $user->hasAnyRoleInside($webhook->organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, WebhookCall $call): Response
    {
        return $user->hasAnyRoleInside($call->webhook->organization)
            ? $this->allow()
            : $this->deny();
    }
}
