<?php

namespace App\Policies;

use App\Enums\OrganizationUserRole;
use App\Models\Organization;
use App\Models\User;
use App\Models\Webhook;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class WebhookPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models. (list)
     */
    public function viewAny(User $user, Organization $organization): Response
    {
        return $user->hasAnyRoleInside($organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Webhook $webhook): Response
    {
        return $user->hasAnyRoleInside($webhook->organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user, Organization $organization): Response
    {
        return $user->hasRoleInside($organization, OrganizationUserRole::Admin)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Webhook $webhook): Response
    {
        return $user->hasRoleInside($webhook->organization, OrganizationUserRole::Admin)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Webhook $webhook): Response
    {
        return $user->hasRoleInside($webhook->organization, OrganizationUserRole::Admin)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can send test requests to the webhook.
     */
    public function test(User $user, Webhook $webhook): Response
    {
        return $user->hasRoleInside($webhook->organization, OrganizationUserRole::Admin)
            ? $this->allow()
            : $this->deny();
    }
}
