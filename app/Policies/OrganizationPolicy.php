<?php

namespace App\Policies;

use App\Enums\OrganizationUserRole;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class OrganizationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models. (list)
     */
    public function viewAny(): Response
    {
        return $this->allow();
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Organization $organization): Response
    {
        return $user->hasAnyRoleInside($organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can export (all) data of this organization.
     */
    public function export(User $user, Organization $organization): Response
    {
        return $user->hasAnyRoleInside($organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(): Response
    {
        return $this->allow();
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Organization $organization): Response
    {
        return $user->hasRoleInside($organization, OrganizationUserRole::Admin)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Organization $organization): Response
    {
        return $user->hasRoleInside($organization, OrganizationUserRole::Admin)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, Organization $organization): Response
    {
        return $user->hasRoleInside($organization, OrganizationUserRole::Admin)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can switch their UI to the model.
     */
    public function switch(User $user, Organization $organization): Response
    {
        return $user->hasAnyRoleInside($organization)
            ? $this->allow()
            : $this->deny();
    }
}
