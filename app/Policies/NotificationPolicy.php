<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;
use Illuminate\Notifications\DatabaseNotification;

class NotificationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, DatabaseNotification $notification): Response
    {
        info($notification);
        return $notification->notifiable_type === User::class && $notification->notifiable_id === $user->id
            ? $this->allow()
            : $this->deny();
    }
}
