<?php

namespace App\Policies;

use App\Enums\OrganizationUserRole;
use App\Models\Invitation;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class InvitationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models. (list)
     */
    public function viewAny(User $user, Organization $organization): Response
    {
        return $user->hasRoleInside($organization, OrganizationUserRole::Admin)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user, Organization $organization): Response
    {
        return $user->hasRoleInside($organization, OrganizationUserRole::Admin)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Invitation $invitation): Response
    {
        return $user->hasRoleInside($invitation->organization, OrganizationUserRole::Admin)
            ? $this->allow()
            : $this->deny();
    }
}
