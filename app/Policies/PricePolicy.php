<?php

namespace App\Policies;

use App\Models\Asset;
use App\Models\Price;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class PricePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user, Asset $asset): Response
    {
        return $user->hasAnyRoleInside($asset->organization)
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Price $price): Response
    {
        return $user->hasAnyRoleInside($price->asset->organization)
            ? $this->allow()
            : $this->deny();
    }
}
