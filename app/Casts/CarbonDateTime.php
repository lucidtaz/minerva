<?php

namespace App\Casts;

use Carbon\CarbonImmutable;
use DateTimeInterface;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use InvalidArgumentException;

/**
 * A better datetime cast that properly converts timezones
 *
 * Assumes that the data is stored in the database using the application
 * timezone (default: UTC). If data is assigned with another timezone, it will
 * be converted first.
 */
class CarbonDateTime implements CastsAttributes
{
    public function get($model, string $key, $value, array $attributes): CarbonImmutable
    {
        return new CarbonImmutable($value, config('app.timezone'));
    }

    public function set($model, string $key, $value, array $attributes)
    {
        if (is_string($value)) {
            return $value;
        }
        if ($value instanceof DateTimeInterface) {
            return CarbonImmutable::createFromInterface($value)->setTimezone(config('app.timezone'))->format('Y-m-d H:i:s');
        }
        throw new InvalidArgumentException('Value must be of type string or DateTimeInterface, ' . get_debug_type($value) . ' given.');
    }
}
