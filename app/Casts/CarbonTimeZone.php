<?php

namespace App\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use InvalidArgumentException;

class CarbonTimeZone implements CastsAttributes
{
    public function get($model, string $key, $value, array $attributes): \Carbon\CarbonTimeZone
    {
        return new \Carbon\CarbonTimeZone($value);
    }

    public function set($model, string $key, $value, array $attributes): string
    {
        if (is_string($value)) {
            return $value;
        }
        if ($value instanceof \Carbon\CarbonTimeZone) {
            return $value->getName();
        }
        throw new InvalidArgumentException('Value must be of type string or CarbonTimeZone, ' . get_debug_type($value) . ' given.');
    }
}
