<?php

namespace App\Enums;

enum AllocationViewMode: string
{
    case Detailed = 'detailed';
    case Simple = 'simple';
    case Purchase = 'purchase';
}
