<?php

namespace App\Enums;

enum InputMethod: string
{
    case Download = 'download';
    case Upload = 'upload';
}
