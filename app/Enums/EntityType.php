<?php

namespace App\Enums;

enum EntityType: string
{
    case Asset = 'asset';
    case Price = 'price';
    case Transaction = 'transaction';
}
