<?php

namespace App\Enums;

enum InputContentType: string
{
    case JSON = 'json';
    case CSV = 'csv';
}
