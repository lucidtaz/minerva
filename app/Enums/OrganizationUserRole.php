<?php

namespace App\Enums;

enum OrganizationUserRole: string
{
    case Admin = 'admin';
    case Member = 'member';
}
