<?php

namespace App\Enums;

enum AssetMode: string
{
    case Normal = 'normal'; // Supports prices and transactions separately
    case Simple = 'simple'; // Supports only "values", so they are simpler to manage
}
