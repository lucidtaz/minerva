<?php

namespace App\Services;

use App\Dtos\AllocationLine;
use App\Dtos\RenderedAllocation;
use App\Dtos\Value;
use App\Enums\AllocationViewMode;
use App\Enums\AssetMode;
use App\Models\Allocation;
use App\Models\Asset;
use App\Models\Price;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class AllocationService
{
    private AssetPriceService $assetPriceService;
    private AssetValueService $assetValueService;

    public function __construct(AssetPriceService $assetPriceService, AssetValueService $assetValueService)
    {
        $this->assetPriceService = $assetPriceService;
        $this->assetValueService = $assetValueService;
    }

    public function render(Allocation $allocation, Asset $base, Carbon $moment): RenderedAllocation
    {
        $result = $this->prepare($allocation, $base, $moment);
        $this->balance($result->lines, $result->sum->value, $result->sum->value);
        $result->calculateDeviation();

        return $result;
    }

    /**
     * Render the first part of the RenderedAllocation
     *
     * This creates the Rendered Allocation, meaning all AllocationLines and
     * their sub-AllocationLines. There is no balancing yet since this is a
     * recursive approach which takes the data from child calls, while the
     * balancing need to be done on the totals and fed to the children again.
     */
    private function prepare(Allocation $allocation, Asset $base, Carbon $moment): RenderedAllocation
    {
        $allocation->load([
            'childAllocations',
            'assets.prices.baseAsset',
            'assets.transactions',
        ]);

        $globalPriceAge = Carbon::now();
        $allocationLines = [];
        $totalValue = new Value(null, $base, '0', $moment);
        $totalWeight = 0;

        foreach ($allocation->childAllocations as $childAllocation) {
            $renderedChild = $this->prepare($childAllocation, $base, $moment);
            $weight = $childAllocation->pivot->weight;
            $globalPriceAge = $globalPriceAge->min($renderedChild->sum->priceAge);

            $allocationLines[] = new AllocationLine(
                $allocation,
                $childAllocation,
                route('allocations.show', [$childAllocation, AllocationViewMode::Detailed->value]),
                $childAllocation->name,
                $weight,
                $renderedChild->sum->price,
                $renderedChild->sum->amount,
                $renderedChild->sum->value,
                $renderedChild->sum->priceAge,
                $renderedChild->lines
            );

            $totalWeight += $weight;
            $totalValue->value = bcadd($totalValue->value, $renderedChild->sum->value->value);
        }

        foreach ($allocation->assets as $asset) {
            $weight = $asset->pivot->weight;
            $value = $this->assetValueService->calculateValue($asset, $base, $moment);
            if ($asset->mode === AssetMode::Normal) {
                $priceAge = $asset->prices->max(fn (Price $price) => $price->moment) ?? Carbon::now();
            } else {
                $priceAge = Carbon::now();
            }

            $allocationLines[] = new AllocationLine(
                $allocation,
                null,
                route('assets.show', $asset),
                $asset->name,
                $weight,
                $this->assetPriceService->calculatePrice($asset, $base, $moment),
                $this->assetValueService->calculateAmount($asset, $moment),
                $value,
                $priceAge
            );

            $totalWeight += $weight;
            $totalValue->value = bcadd($totalValue->value, $value->value);
            $globalPriceAge = $globalPriceAge->min($priceAge);
        }
        $sumAllocationLine = new AllocationLine(
            $allocation,
            $allocation,
            null,
            '∑',
            $totalWeight,
            null,
            null,
            $totalValue,
            $globalPriceAge
        );

        return new RenderedAllocation(
            $allocation,
            $allocationLines,
            $sumAllocationLine
        );
    }

    /**
     * @param AllocationLine[] $allocationLines
     * @param Value $allocatedValue The value to allocate to the given set of allocation lines
     * @param Value $globalValue The total value of the entire portfolio, counted from the root
     */
    private function balance(array $allocationLines, Value $allocatedValue, Value $globalValue): void
    {
        if (count($allocationLines) === 0) {
            return;
        }

        if (bccomp('0', $globalValue->value) === 0) {
            // Prevent division by zero
            $allocatedValueGlobalShare = 1;
        } else {
            $allocatedValueGlobalShare = bcdiv($allocatedValue->value, $globalValue->value);
        }

        $totalWeight = '0';
        foreach ($allocationLines as $line) {
            $totalWeight = bcadd($totalWeight, $line->weight);
        }

        foreach ($allocationLines as $line) {
            if (bccomp('0', $globalValue->value) === 0) {
                // Prevent division by zero
                $currentGlobalShare = 1;
            } else {
                $currentGlobalShare = bcdiv($line->value->value, $globalValue->value);
            }
            $lineAllocatedLocalShare = bcdiv($line->weight, $totalWeight);
            $lineAllocatedGlobalShare = bcmul($lineAllocatedLocalShare, $allocatedValueGlobalShare);

            $targetValue = new Value(
                $line->value->asset,
                $line->value->baseAsset,
                bcmul($allocatedValue->value, $lineAllocatedLocalShare),
                $line->value->moment
            );
            $deltaValue = new Value(
                $line->value->asset,
                $line->value->baseAsset,
                bcsub($targetValue->value, $line->value->value),
                $targetValue->moment
            );
            if ($line->price === null) {
                $deltaValueInOriginalBase = null;
            } else {
                $deltaValueInOriginalBase = new Value(
                    $line->value->asset,
                    $line->price->baseAsset,
                    bccomp($line->price->rebaseRate, '0') === 0 ? '0' : bcdiv($deltaValue->value, $line->price->rebaseRate),
                    $line->value->moment,
                );
            }
            if ($line->amount === null || $line->price === null) {
                $targetAmount = null;
                $deltaAmount = null;
            } else {
                if (bccomp($line->price->rebasedValue, 0) !== 0) {
                    $targetAmount = bcdiv($targetValue->value, $line->price->rebasedValue);
                } else {
                    $targetAmount = $line->amount;
                }
                $deltaAmount = bcsub($targetAmount, $line->amount);
            }

            $line->setBalancingInformation(
                $lineAllocatedGlobalShare,
                $currentGlobalShare,
                $targetValue,
                $deltaValue,
                $deltaValueInOriginalBase,
                $targetAmount,
                $deltaAmount,
            );

            $this->balance($line->subLines, $targetValue, $globalValue);
        }
    }

    /**
     * @return Collection<int, Asset>
     */
    public function getAllDescendentAssets(Allocation $allocation): Collection
    {
        $result = $allocation->assets;

        foreach ($allocation->childAllocations as $childAllocation) {
            $result = $result->concat($this->getAllDescendentAssets($childAllocation));
        }

        return $result;
    }

    /**
     * Get all descendent allocations
     *
     * The input allocation is also considered a descendent!
     *
     * @return Collection<int, Allocation>
     */
    public function getAllDescendentAllocations(Allocation $allocation): Collection
    {
        $result = new Collection([$allocation]);

        foreach ($allocation->childAllocations as $childAllocation) {
            $result = $result->concat($this->getAllDescendentAllocations($childAllocation));
        }

        return $result;
    }
}
