<?php

namespace App\Services\Export;

use App\Models\Asset;
use App\Models\AssetAlias;
use App\Models\Organization;
use Illuminate\Http\File;
use JetBrains\PhpStorm\ArrayShape;
use LogicException;
use RuntimeException;
use ZipArchive;

class ExportService
{
    public function createExportArchiveFile(Organization $organization): File
    {
        $files = $this->generateFiles($organization);
        if (count($files) === 0) {
            throw new LogicException('Did not generate any files...');
        }
        if (count($files) === 1) {
            return head($files);
        }
        return $this->zipFiles($files);
    }

    /**
     * @return File[]
     */
    #[ArrayShape(['assets.csv' => File::class, 'prices.csv' => File::class, 'transactions.csv' => File::class])]
    private function generateFiles(Organization $organization): array
    {
        return [
            'assets.csv' => $this->exportAssets($organization),
            'prices.csv' => $this->exportPrices($organization),
            'transactions.csv' => $this->exportTransactions($organization),
        ];
    }

    private function exportAssets(Organization $organization): File
    {
        $filename = tempnam(sys_get_temp_dir(), 'assets_') . '.csv';
        $handle = fopen($filename, 'wb');

        fputcsv($handle, [
            'uuid',
            'name',
            'aliases',
            'currency_symbol',
            'mode',
            'import_id',
            'created_at',
            'deleted_at',
        ]);

        foreach ($organization->assets()->withTrashed()->get() as $asset) {
            /** @var Asset $asset */
            fputcsv($handle, [
                $asset->uuid,
                $asset->name,
                $asset->aliases->map(fn (AssetAlias $alias) => $alias->alias)->implode(','),
                $asset->currency_symbol,
                $asset->mode->value,
                $asset->import_id,
                $asset->created_at->toIso8601String(),
                $asset->deleted_at !== null ? $asset->deleted_at->toIso8601String() : null,
            ]);
        }

        fclose($handle);

        return new File($filename);
    }

    private function exportPrices(Organization $organization): File
    {
        $filename = tempnam(sys_get_temp_dir(), 'prices_') . '.csv';
        $handle = fopen($filename, 'wb');

        fputcsv($handle, [
            'asset_uuid',
            'import_id',
            'base_asset_uuid',
            'value',
            'moment',
            'created_at',
        ]);

        foreach ($organization->assets()->withTrashed()->get() as $asset) {
            /** @var Asset $asset */
            $asset->load('prices.baseAsset');
            foreach ($asset->prices as $price) {
                fputcsv($handle, [
                    $asset->uuid,
                    $price->import_id,
                    $price->baseAsset->uuid,
                    $price->value,
                    $price->moment->toIso8601String(),
                    $price->created_at->toIso8601String(),
                ]);
            }
        }

        fclose($handle);

        return new File($filename);
    }

    private function exportTransactions(Organization $organization): File
    {
        $filename = tempnam(sys_get_temp_dir(), 'transactions_') . '.csv';
        $handle = fopen($filename, 'wb');

        fputcsv($handle, [
            'asset_uuid',
            'import_id',
            'amount',
            'location',
            'note',
            'is_temporary',
            'moment',
            'created_at',
        ]);

        foreach ($organization->assets()->withTrashed()->get() as $asset) {
            /** @var Asset $asset */
            foreach ($asset->transactions as $transaction) {
                fputcsv($handle, [
                    $asset->uuid,
                    $transaction->import_id,
                    $transaction->amount,
                    $transaction->location,
                    $transaction->note,
                    $transaction->is_temporary ? 'true' : 'false',
                    $transaction->moment->toIso8601String(),
                    $transaction->created_at->toIso8601String(),
                ]);
            }
        }

        fclose($handle);

        return new File($filename);
    }

    /**
     * @param File[] $files
     */
    private function zipFiles(array $files): File
    {
        $filename = tempnam(sys_get_temp_dir(), 'export_') . '.zip';
        $zip = new ZipArchive();

        if ($zip->open($filename, ZipArchive::CREATE) !== true) {
            throw new RuntimeException('Cannot open temporary zip file for writing.');
        }

        foreach ($files as $targetFilename => $file) {
            $zip->addFile($file->getRealPath(), $targetFilename);
        }

        $zip->close();

        return new File($filename);
    }
}
