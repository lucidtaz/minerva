<?php

namespace App\Services\Import;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;

class DownloadInputMethod implements InputMethod
{
    private string $url;

    private ClientInterface $httpClient;

    private LoggerInterface $logger;

    public function __construct(string $url, ClientInterface $httpClient, LoggerInterface $logger)
    {
        $this->url = $url;
        $this->httpClient = $httpClient;
        $this->logger = $logger;
    }

    public function getRawData(): string
    {
        $this->logger->info('Requesting HTTP GET ' . $this->url);

        try {
            $response = $this->httpClient->request('GET', $this->url, [
                RequestOptions::TIMEOUT => 10.0,
            ]);
        } catch (GuzzleException $e) {
            $this->logger->error('Error during HTTP request: ' . $e->getMessage());
            return '';
        }

        if ($response->getStatusCode() >= 300) {
            $this->logger->error('Response status code ' . $response->getStatusCode());
            $this->logger->info('Response contents: ' . $response->getBody()->getContents());
        }

        return $response->getBody()->getContents();
    }
}
