<?php

namespace App\Services\Import;

use App\Enums\AssetMode;
use App\Models\Asset;
use App\Models\AssetAlias;
use Illuminate\Support\Collection;
use Ramsey\Uuid\Uuid;

class AssetEntityImporter extends BaseEntityImporter
{
    public function run(array $arrayedData): Collection
    {
        $assetDatas = $this->parseArrayedDataToStructuredData(
            $arrayedData
        );

        $parsedAssets = $this->parseAssets(
            $assetDatas
        );

        return $this->eliminateDuplicateAssets(
            $parsedAssets
        );
    }

    /**
     * @return Collection&Asset[]
     */
    private function parseAssets(array $assetDatas): Collection
    {
        /** @var Collection&Asset[] $parsedAssets */
        $parsedAssets = new Collection();

        foreach ($assetDatas as $index => $assetData) {
            $assetName = $assetData['name'] ?? null;
            $assetCurrencySymbol = $assetData['symbol'] ?? null;
            $assetMode = $assetData['mode'] ?? AssetMode::Normal->value;
            $assetImportId = $assetData['import_id'] ?? null;
            $assetAliases = $assetData['aliases'] ?? [];

            if (empty(trim($assetName))) {
                $assetName = null;
            }
            if (empty(trim($assetCurrencySymbol))) {
                $assetCurrencySymbol = null;
            }
            if (empty(trim($assetImportId))) {
                $assetImportId = null;
            }

            if ($assetName === null) {
                $this->logger->error('Record with index ' . $index . ' has no name information');
                continue;
            }

            $mode = AssetMode::tryFrom($assetMode);
            if ($mode === null) {
                $this->logger->error('Record with index ' . $index . ' has invalid mode');
                continue;
            }

            $uuid = Uuid::uuid4();
            $asset = new Asset([
                'name' => $assetName,
                'currency_symbol' => $assetCurrencySymbol,
                'mode' => $mode,
                'import_id' => $assetImportId ?? $uuid,
            ]);
            $asset->uuid = $uuid;
            $asset->organization()->associate($this->organization);

            $aliases = new Collection();
            if ($assetAliases !== null) {
                foreach ($assetAliases as $aliasValue) {
                    $alias = new AssetAlias([
                        'alias' => $aliasValue,
                    ]);
                    $alias->organization()->associate($this->organization);
                    $aliases->push($alias);
                }
            } else {
                $nameAlias = new AssetAlias([
                    'alias' => $asset->name,
                ]);
                $nameAlias->organization()->associate($this->organization);
                $aliases->push($nameAlias);

                $uuidAlias = new AssetAlias([
                    'alias' => $asset->uuid,
                ]);
                $uuidAlias->organization()->associate($this->organization);
                $aliases->push($nameAlias);
            }

            $asset->setRelation('aliases', $aliases);
            Asset::saved(function (Asset $savedAsset) use ($asset, $aliases) {
                if ($savedAsset === $asset) {
                    $savedAsset->aliases()->saveMany($aliases);
                }
            });

            $parsedAssets->add($asset);
        }

        $this->logger->info('Parsed ' . $parsedAssets->count() . ' assets');

        return $parsedAssets;
    }

    /**
     * @param Collection&Asset[] $parsedAssets
     * @return Collection&Asset[]
     */
    private function eliminateDuplicateAssets(
        Collection $parsedAssets
    ): Collection {
        if ($parsedAssets->isEmpty()) {
            return $parsedAssets;
        }

        $assetsByImportId = $parsedAssets->keyBy('import_id');
        $difference = $parsedAssets->count() - $assetsByImportId->count();
        if ($difference !== 0) {
            $this->logger->warning('Found and removed ' . $difference . ' duplicate(s) within the input data');
            $parsedAssets = $assetsByImportId->values();
        }

        $storedDuplicateIds = $this->organization->assets()
            ->whereIn('import_id', $assetsByImportId->keys())
            ->pluck('import_id');
        $storedAssetAliases = $this->organization->assetAliases;

        /** @var Collection&Asset[] $newAssets */
        $newAssets = new Collection();
        foreach ($parsedAssets as $parsedAsset) {
            if ($storedDuplicateIds->contains($parsedAsset->import_id)) {
                $this->logger->info('Asset already exists. Name = ' . $parsedAsset->name);
                continue;
            }

            foreach ($parsedAsset->aliases as $parsedAssetAlias) {
                if ($storedAssetAliases->contains(fn(AssetAlias $storedAlias) => $storedAlias->hasSameName($parsedAssetAlias))) {
                    $this->logger->error('Asset alias already exists. Alias = "' . $parsedAssetAlias->alias . '". The offending Asset must be deleted, even if it is already soft-deleted.');
                    continue 2;
                }
            }

            $newAssets->add($parsedAsset);
            $this->logger->info('Asset is new. Name = ' . $parsedAsset->name);
        }

        $this->logger->info('Found ' . $newAssets->count() . ' new asset(s)');

        return $newAssets;
    }
}
