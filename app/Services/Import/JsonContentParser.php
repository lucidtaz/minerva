<?php

namespace App\Services\Import;

use JsonException;
use Psr\Log\LoggerInterface;

class JsonContentParser implements ContentParser
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function run(string $rawData): array
    {
        $this->logger->info('Interpreting raw data as JSON');
        try {
            return json_decode($rawData, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            $this->logger->error('Could not decode data as JSON. Error message: ' . $e->getMessage());
            $this->logger->debug('The data starts with: "' . substr($rawData, 0, 50) . '"');
            return [];
        }
    }
}
