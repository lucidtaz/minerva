<?php

namespace App\Services\Import;

interface ContentParser
{
    /**
     * Convert the input string into "arrayed" data
     *
     * This means that the result is represented as a PHP array, i.e. an array
     * or dictionary.
     *
     * If an error is encountered, a warning or error should be logged and the
     * offending element should be omitted from the result. If no result can be
     * formed at all, an empty array is returned.
     */
    public function run(string $rawData): array;
}
