<?php

namespace App\Services\Import;

use App\Enums\EntityType;
use App\Enums\InputContentType;
use App\Models\Asset;
use App\Models\ImportConfiguration;
use App\Models\ImportExecution;
use App\Models\Price;
use App\Models\Transaction;
use App\Notifications\ImportExecutionRaisedErrors;
use DB;
use GuzzleHttp\ClientInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;
use Psr\Log\LoggerInterface;
use RuntimeException;
use UnexpectedValueException;

class ImportService
{
    private ClientInterface $httpClient;

    public function __construct(ClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function run(ImportConfiguration $importConfiguration, ?UploadedFile $file): ImportExecution
    {
        $logger = new ImportLogger();

        $logger->info('Running import configuration ' . $importConfiguration->name . '...');

        $inputMethod = $this->buildInputMethod($importConfiguration, $logger, $file);
        $contentParser = $this->buildContentParser($importConfiguration, $logger);
        $entityImporter = $this->buildEntityImporter($importConfiguration, $logger);

        $rawData = $inputMethod->getRawData();
        $arrayedData = $contentParser->run($rawData);
        $createdEntities = $entityImporter->run($arrayedData);

        $logger->info('Import complete');

        $importExecution = $this->save($importConfiguration, $logger, $createdEntities);

        $this->notifyOnErrors($importExecution, $logger);

        return $importExecution;
    }

    private function buildInputMethod(ImportConfiguration $importConfiguration, LoggerInterface $logger, ?UploadedFile $file): InputMethod
    {
        if ($importConfiguration->input_method === \App\Enums\InputMethod::Download) {
            return new DownloadInputMethod($importConfiguration->url, $this->httpClient, $logger);
        }

        if ($importConfiguration->input_method === \App\Enums\InputMethod::Upload) {
            if ($file === null) {
                throw new RuntimeException('No uploaded file provided');
            }
            return new UploadInputMethod($file, $logger);
        }

        throw new UnexpectedValueException('Import configuration input method is unexpected: ' . $importConfiguration->input_method);
    }

    private function buildContentParser(ImportConfiguration $importConfiguration, LoggerInterface $logger): ContentParser
    {
        return match($importConfiguration->input_content_type) {
            InputContentType::JSON => new JsonContentParser($logger),
            InputContentType::CSV => new CsvContentParser(
                $importConfiguration->has_header_row,
                $importConfiguration->delimiter,
                $importConfiguration->enclosure_character,
                $importConfiguration->escape_character,
                $logger
            ),
        };
    }

    private function buildEntityImporter(ImportConfiguration $importConfiguration, LoggerInterface $logger): EntityImporter
    {
        return match ($importConfiguration->entity_type) {
            EntityType::Asset => new AssetEntityImporter(
                $importConfiguration->organization,
                $importConfiguration->date_format,
                $importConfiguration->parse_expression,
                $logger
            ),
            EntityType::Price => new PriceEntityImporter(
                $importConfiguration->organization,
                $importConfiguration->date_format,
                $importConfiguration->parse_expression,
                $logger
            ),
            EntityType::Transaction => new TransactionEntityImporter(
                $importConfiguration->organization,
                $importConfiguration->date_format,
                $importConfiguration->parse_expression,
                $logger
            ),
        };
    }

    /**
     * @param Collection&Model[] $createdEntities
     */
    private function save(
        ImportConfiguration $importConfiguration,
        ImportLogger $logger,
        Collection $createdEntities
    ): ImportExecution {
        return DB::transaction(static function () use ($importConfiguration, $logger, $createdEntities) {
            $execution = new ImportExecution([
                'log' => $logger->getLoggedText(),
                'entities_created' => $createdEntities->count(),
                'warnings' => $logger->getNumberOfWarnings(),
                'errors' => $logger->getNumberOfErrors(),
            ]);
            $importConfiguration->importExecutions()->save($execution);

            foreach ($createdEntities as $entity) {
                /** @var Asset|Price|Transaction $entity */
                $entity->importExecution()->associate($execution);
                $entity->save();
            }

            return $execution;
        });
    }

    private function notifyOnErrors(ImportExecution $execution, ImportLogger $logger): void
    {
        if ($logger->getNumberOfErrors() === 0) {
            return;
        }

        $users = $execution->importConfiguration->organization->users;
        Notification::send($users, new ImportExecutionRaisedErrors($execution));
    }
}
