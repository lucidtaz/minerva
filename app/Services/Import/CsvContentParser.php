<?php

namespace App\Services\Import;

use Psr\Log\LoggerInterface;

class CsvContentParser implements ContentParser
{
    private bool $hasHeaderRow;

    private string $delimiter;

    private string $enclosureCharacter;

    private string $escapeCharacter;

    private LoggerInterface $logger;

    public function __construct(bool $hasHeaderRow, string $delimiter, string $enclosureCharacter, string $escapeCharacter, LoggerInterface $logger)
    {
        $translation = [
            '\r' => "\r",
            '\n' => "\n",
            '\t' => "\t",
            '\\\\' => "\\",
        ];

        $this->hasHeaderRow = $hasHeaderRow;
        $this->delimiter = strtr($delimiter, $translation);
        $this->enclosureCharacter = strtr($enclosureCharacter, $translation);
        $this->escapeCharacter = strtr($escapeCharacter, $translation);
        $this->logger = $logger;
    }

    public function run(string $rawData): array
    {
        $this->logger->info('Interpreting raw data as CSV');

        $normalizedNewlinesData = str_replace(["\r\n", "\r"], "\n", $rawData);
        $lines = explode("\n", $normalizedNewlinesData);

        $headers = null;
        if ($this->hasHeaderRow) {
            $headerLine = array_shift($lines);
            $headers = str_getcsv(
                $headerLine,
                $this->delimiter,
                $this->enclosureCharacter,
                $this->escapeCharacter
            );
        }

        $result = [];

        foreach ($lines as $lineIndex => $line) {
            if ($this->hasHeaderRow) {
                $lineNumber = $lineIndex + 2;
            } else {
                $lineNumber = $lineIndex + 1;
            }

            if (empty($line)) {
                $this->logger->debug('Empty line number ' . $lineNumber . ' ignored');
                continue;
            }

            $parsedLine = str_getcsv(
                $line,
                $this->delimiter,
                $this->enclosureCharacter,
                $this->escapeCharacter
            );

            if ($headers !== null) {
                $lineWithKeys = array_combine($headers, $parsedLine);
                $parsedLine = array_merge($parsedLine, $lineWithKeys);
            }

            $result[] = $parsedLine;
        }

        return $result;
    }
}
