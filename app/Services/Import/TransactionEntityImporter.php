<?php

namespace App\Services\Import;

use App\Models\Transaction;
use Exception;
use Illuminate\Support\Collection;

class TransactionEntityImporter extends BaseEntityImporter
{
    public function run(array $arrayedData): Collection
    {
        $transactionDatas = $this->parseArrayedDataToStructuredData(
            $arrayedData
        );

        $parsedTransactions = $this->parseTransactions(
            $transactionDatas
        );

        return $this->eliminateDuplicateTransactions(
            $parsedTransactions
        );
    }

    /**
     * @return Collection&Transaction[]
     */
    private function parseTransactions(array $transactionDatas): Collection
    {
        /** @var Collection&Transaction[] $parsedTransactions */
        $parsedTransactions = new Collection();

        foreach ($transactionDatas as $index => $transactionData) {
            $transactionAsset = $transactionData['asset'] ?? null;
            $transactionMoment = $transactionData['moment'] ?? null;
            $transactionAmount = $transactionData['amount'] ?? null;
            $transactionLocation = $transactionData['location'] ?? null;
            $transactionNote = $transactionData['note'] ?? null;
            $transactionImportId = array_key_exists('import_id', $transactionData) ? (string)$transactionData['import_id'] : null;

            if (empty(trim($transactionAsset))) {
                $transactionAsset = null;
            }
            if (empty(trim($transactionMoment))) {
                $transactionMoment = null;
            }
            if (empty(trim($transactionAmount))) {
                $transactionAmount = null;
            }
            if (empty(trim($transactionLocation))) {
                $transactionLocation = null;
            }
            if (empty(trim($transactionNote))) {
                $transactionNote = null;
            }
            if (empty(trim($transactionImportId))) {
                $transactionImportId = null;
            }

            if ($transactionAsset === null) {
                $this->logger->error('Record with index ' . $index . ' has no asset information');
                continue;
            }

            if ($transactionMoment === null) {
                $this->logger->error('Record with index ' . $index . ' has no moment information');
                continue;
            }

            if ($transactionAmount === null) {
                $this->logger->error('Record with index ' . $index . ' has no amount information');
                continue;
            }

            $asset = $this->parseAsset($transactionAsset);
            if ($asset === null) {
                $this->logger->warning('Record with index ' . $index . ' has non-existing Asset ' . $transactionAsset);
                continue;
            }

            $moment = null;
            try {
                $moment = $this->parseMoment($transactionMoment);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->logger->error('Could not parse moment "' . $transactionMoment . '" for record with index ' . $index);
                continue;
            }

            $amount = $this->parseAmount($transactionAmount);
            if ($transactionImportId === null) {
                $transactionImportId = $asset->uuid . '|' . $moment->getTimestamp() . '|' . $amount;
            }

            $transaction = new Transaction([
                'moment' => $moment,
                'amount' => $amount,
                'location' => $transactionLocation,
                'note' => $transactionNote,
                'import_id' => $transactionImportId,
            ]);
            $transaction->asset()->associate($asset);

            $parsedTransactions->add($transaction);
        }

        $this->logger->info('Parsed ' . $parsedTransactions->count() . ' transactions');

        return $parsedTransactions;
    }

    /**
     * @param Collection&Transaction[] $parsedTransactions
     * @return Collection&Transaction[]
     */
    private function eliminateDuplicateTransactions(
        Collection $parsedTransactions
    ): Collection {
        if ($parsedTransactions->isEmpty()) {
            return $parsedTransactions;
        }

        $transactionsByImportId = $parsedTransactions->keyBy('import_id');
        $difference = $parsedTransactions->count() - $transactionsByImportId->count();
        if ($difference !== 0) {
            $this->logger->warning('Found and removed ' . $difference . ' duplicate(s) within the input data');
            $parsedTransactions = $transactionsByImportId->values();
        }

        $storedDuplicateIds = $this->organization->assets()
            ->join('transactions', 'transactions.asset_id', '=', 'assets.id')
            ->whereIn('transactions.import_id', $transactionsByImportId->keys())
            ->pluck('transactions.import_id');

        /** @var Collection&Transaction[] $newTransactions */
        $newTransactions = new Collection();
        foreach ($parsedTransactions as $parsedTransaction) {
            if ($storedDuplicateIds->contains($parsedTransaction->import_id)) {
                $this->logger->info('Transaction already exists. Asset = ' . $parsedTransaction->asset->name . ', Moment = ' . $parsedTransaction->moment . ', amount = ' . $parsedTransaction->amount);
                continue;
            }

            $newTransactions->add($parsedTransaction);
            $this->logger->info('Transaction is new. Asset = ' . $parsedTransaction->asset->name . ', Moment = ' . $parsedTransaction->moment . ', amount = ' . $parsedTransaction->amount);
        }

        $this->logger->info('Found ' . $newTransactions->count() . ' new transaction(s)');

        return $newTransactions;
    }
}
