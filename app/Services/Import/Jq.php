<?php

namespace App\Services\Import;

use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Process\Process;

/**
 * Apply a parse_expression using jq
 *
 * This makes use of the jq command-line process, since it's easiest to
 * implement. It does mean that a subprocess is needed, so as a possible
 * improvement it should use PHP's FFI and libjq, which will offer the same jq
 * implementation but in the same process.
 */
class Jq
{
    private string $expression;

    private LoggerInterface $logger;

    public function __construct(string $expression, LoggerInterface $logger)
    {
        $this->expression = $expression;
        $this->logger = $logger;
    }

    public function run(array $arrayedData): array
    {
        $inputJson = json_encode($arrayedData, JSON_THROW_ON_ERROR);

        $preparedExpression = str_replace(["\r", "\n"], " ", $this->expression);

        /** @var Process $process */
        $process = $this->withStringInFile($inputJson, static function (string $filename) use ($preparedExpression) {
            $process = new Process(['jq', '-r', '-M', $preparedExpression, $filename]);
            $process->run();
            return $process;
        });

        if (!$process->isSuccessful()) {
            $this->logger->error(
                'jq process failed with exit code ' . $process->getExitCode() .
                ', output: ' . $process->getErrorOutput()
            );
            return [];
        }

        $output = $process->getOutput();

        try {
            $result = json_decode($output, true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            $this->logger->error('Applying the parse expression on the input did not yield a valid result. Error message: ' . $e->getMessage());
            return [];
        }

        if (!is_array($result)) {
            $this->logger->error('Applying the parse expression on the input did not yield an array. This is needed for further processing of the data.');
            return [];
        }

        return $result;
    }

    /**
     * Write $input to a file, then call $callback with the filename as parameter
     */
    private function withStringInFile(string $input, callable $callback): mixed
    {
        $filename = tempnam('/tmp', 'jq_');
        $this->logger->debug('Writing to temporary file ' . $filename);

        file_put_contents($filename, $input);

        try {
            return $callback($filename);
        } finally {
            unlink($filename);
        }
    }
}
