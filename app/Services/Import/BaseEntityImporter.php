<?php

namespace App\Services\Import;

use App\Models\Asset;
use App\Models\AssetAlias;
use App\Models\Organization;
use Carbon\Carbon;
use Carbon\CarbonTimeZone;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use JetBrains\PhpStorm\Pure;
use Psr\Log\LoggerInterface;
use UnexpectedValueException;

abstract class BaseEntityImporter implements EntityImporter
{
    protected Organization $organization;

    private ?string $dateFormat;

    private string $parseExpression;

    protected LoggerInterface $logger;

    /**
     * Available Assets in the database to import entities for
     *
     * @var Collection&AssetAlias[]|null
     */
    private ?Collection $assetAliases = null;

    public function __construct(
        Organization $organization,
        ?string $dateFormat,
        string $parseExpression,
        LoggerInterface $logger
    ) {
        $this->organization = $organization;
        $this->dateFormat = $dateFormat;
        $this->parseExpression = $parseExpression;
        $this->logger = $logger;
    }

    protected function parseArrayedDataToStructuredData(array $arrayedData): array
    {
        $jq = new Jq($this->parseExpression, $this->logger);
        return $jq->run($arrayedData);
    }

    protected function parseAsset(string $inputAsset): ?Asset
    {
        if ($this->assetAliases === null) {
            $this->assetAliases = $this->organization
                ->assetAliases()
                ->with('asset')
                ->get();
        }

        $alias = $this->assetAliases->first(fn (AssetAlias $a) => strtolower($a->alias) === strtolower(trim($inputAsset)));

        if ($alias === null) {
            return null;
        }

        return $alias->asset;
    }

    protected function parseMoment(string $inputMoment): Carbon
    {
        try {
            if ($this->dateFormat !== null) {
                return Carbon::createFromFormat($this->dateFormat, $inputMoment, new CarbonTimeZone('UTC'));
            }

            return new Carbon($inputMoment, new CarbonTimeZone('UTC'));
        } catch (Exception $e) {
            throw new UnexpectedValueException('Input moment ' . $inputMoment . ' is not correctly formatted.', 0, $e);
        }
    }

    #[Pure]
    protected function parseAmount(string $inputAmount): string
    {
        return sprintf('%.8f', $inputAmount); // To cope with scientific notation
    }
}
