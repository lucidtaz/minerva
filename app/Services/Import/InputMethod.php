<?php

namespace App\Services\Import;

interface InputMethod
{
    public function getRawData(): string;
}
