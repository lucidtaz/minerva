<?php

namespace App\Services\Import;

use App\Models\Price;
use Exception;
use Illuminate\Support\Collection;

class PriceEntityImporter extends BaseEntityImporter
{
    public function run(array $arrayedData): Collection
    {
        $priceDatas = $this->parseArrayedDataToStructuredData(
            $arrayedData
        );

        $parsedPrices = $this->parsePrices(
            $priceDatas
        );

        return $this->eliminateDuplicatePrices(
            $parsedPrices
        );
    }

    /**
     * @return Collection&Price[]
     */
    private function parsePrices(array $priceDatas): Collection
    {
        /** @var Collection&Price[] $parsedPrices */
        $parsedPrices = new Collection();

        foreach ($priceDatas as $index => $priceData) {
            $priceAsset = $priceData['asset'] ?? null;
            $priceMoment = $priceData['moment'] ?? null;
            $priceValue = $priceData['value'] ?? null;
            $priceImportId = $priceData['import_id'] ?? null;
            $priceCurrency = $priceData['currency'] ?? null;

            if (empty(trim($priceAsset))) {
                $priceAsset = null;
            }
            if (empty(trim($priceMoment))) {
                $priceMoment = null;
            }
            if (empty(trim($priceValue))) {
                $priceValue = null;
            }
            if (empty(trim($priceImportId))) {
                $priceImportId = null;
            }
            if (empty(trim($priceCurrency))) {
                $priceCurrency = null;
            }

            if ($priceAsset === null) {
                $this->logger->error('Record with index ' . $index . ' has no asset information');
                continue;
            }

            if ($priceMoment === null) {
                $this->logger->error('Record with index ' . $index . ' has no moment information');
                continue;
            }

            if ($priceValue === null) {
                $this->logger->error('Record with index ' . $index . ' has no value information');
                continue;
            }

            if ($priceCurrency === null) {
                $this->logger->error('Record with index ' . $index . ' has no currency information');
                continue;
            }

            $asset = $this->parseAsset($priceAsset);
            if ($asset === null) {
                $this->logger->warning('Record with index ' . $index . ' has non-existing Asset ' . $priceAsset);
                continue;
            }

            $baseAsset = $this->parseAsset($priceCurrency);
            if ($baseAsset === null) {
                $this->logger->warning('Record with index ' . $index . ' has non-existing currency Asset ' . $priceCurrency);
                continue;
            }
            if (!$baseAsset->is_currency) {
                $this->logger->warning('Record with index ' . $index . ' has base Asset ' . $priceCurrency . ' which exists but is not a currency');
                continue;
            }

            $moment = null;
            try {
                $moment = $this->parseMoment($priceMoment);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->logger->error('Could not parse moment "' . $priceMoment . '" for record with index ' . $index);
                continue;
            }

            $value = $this->parseAmount($priceValue);
            if ($priceImportId === null) {
                $priceImportId = $asset->uuid . '|' . $moment->getTimestamp();
            }

            $price = new Price([
                'moment' => $moment,
                'value' => $value,
                'import_id' => $priceImportId,
            ]);
            $price->asset()->associate($asset);
            $price->baseAsset()->associate($baseAsset);

            $parsedPrices->add($price);
        }

        $this->logger->info('Parsed ' . $parsedPrices->count() . ' prices');

        return $parsedPrices;
    }

    /**
     * @param Collection&Price[] $parsedPrices
     * @return Collection&Price[]
     */
    private function eliminateDuplicatePrices(
        Collection $parsedPrices
    ): Collection {
        if ($parsedPrices->isEmpty()) {
            return $parsedPrices;
        }

        $pricesByImportId = $parsedPrices->keyBy('import_id');
        $difference = $parsedPrices->count() - $pricesByImportId->count();
        if ($difference !== 0) {
            $this->logger->warning('Found and removed ' . $difference . ' duplicate(s) within the input data');
            $parsedPrices = $pricesByImportId->values();
        }

        $storedDuplicateIds = $this->organization->assets()
            ->join('prices', 'prices.asset_id', '=', 'assets.id')
            ->whereIn('prices.import_id', $pricesByImportId->keys())
            ->pluck('prices.import_id');

        /** @var Collection&Price[] $newPrices */
        $newPrices = new Collection();
        foreach ($parsedPrices as $parsedPrice) {
            if ($storedDuplicateIds->contains($parsedPrice->import_id)) {
                $this->logger->info('Price already exists. Asset = ' . $parsedPrice->asset->name . ', Moment = ' . $parsedPrice->moment);
                continue;
            }

            $newPrices->add($parsedPrice);
            $this->logger->info('Price is new. Asset = ' . $parsedPrice->asset->name . ', Moment = ' . $parsedPrice->moment . ', value = ' . $parsedPrice->value . ', currency = ' . $parsedPrice->baseAsset->name);
        }

        $this->logger->info('Found ' . $newPrices->count() . ' new price(s)');

        return $newPrices;
    }
}
