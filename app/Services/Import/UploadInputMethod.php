<?php

namespace App\Services\Import;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\UploadedFile;
use Psr\Log\LoggerInterface;

class UploadInputMethod implements InputMethod
{
    private UploadedFile $file;

    private LoggerInterface $logger;

    public function __construct(UploadedFile $file, LoggerInterface $logger)
    {
        $this->file = $file;
        $this->logger = $logger;
    }

    public function getRawData(): string
    {
        $this->logger->info('Opening uploaded file ' . $this->file->getClientOriginalName());

        try {
            $bytes = $this->file->get();
        } catch (FileNotFoundException $e) {
            $this->logger->error($e->getMessage());
            $this->logger->error('Could not read uploaded file');
            return '';
        }

        if ($bytes === false || $bytes === '') {
            $this->logger->error('Uploaded file is empty');
            return '';
        }

        $encoding = $this->detectEncoding($bytes);

        if ($encoding === null) {
            $this->logger->error('Could not detect input encoding');
            return '';
        }

        $this->logger->info('Detected input encoding as ' . $encoding);

        $encoded = mb_convert_encoding($bytes, 'UTF-8', $encoding);

        $this->logger->debug('Encoded ' . strlen($bytes) . ' bytes into ' . mb_strlen($encoded) . ' UTF-8 characters consisting of ' . strlen($encoded) . ' bytes');

        return $encoded;
    }

    private function detectEncoding($bytes): ?string
    {
        // For some reason we cannot detect UTF-16 with mb_detect_encoding, it's even mentioned in the manual page
        $encoding = mb_detect_encoding($bytes, ['ASCII', 'UTF-8']);

        if ($encoding !== false) {
            return $encoding;
        }

        $bom = pack('H*', 'FEFF');
        if (strncmp($bytes, $bom, strlen($bom))) {
            return 'UTF-16LE';
        }

        return null;
    }
}
