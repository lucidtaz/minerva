<?php

namespace App\Services\Import;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface EntityImporter
{
    /**
     * Import and save entities
     *
     * @return Collection&Model[] The created entities
     */
    public function run(array $arrayedData): Collection;
}
