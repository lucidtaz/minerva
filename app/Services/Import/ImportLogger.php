<?php

namespace App\Services\Import;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use RuntimeException;

class ImportLogger extends AbstractLogger
{
    /**
     * @var resource
     */
    private $logStream;

    private LoggerInterface $inner;

    private array $countsPerLogLevel = [];

    public function __construct()
    {
        $this->logStream = fopen('php://memory', 'wb');
        if ($this->logStream === false) {
            throw new RuntimeException('Could not open memory stream for storing log text');
        }

        $this->inner = new Logger('Importer', [
            (new StreamHandler($this->logStream))->setFormatter(new LineFormatter(
                "%datetime% %level_name%: %message%\n",
                'Y-m-d H:i:s',
            )),
        ]);
    }

    public function __destruct()
    {
        if (is_resource($this->logStream)) {
            fclose($this->logStream);
        }
    }

    public function log($level, $message, array $context = []): void
    {
        if (!array_key_exists($level, $this->countsPerLogLevel)) {
            $this->countsPerLogLevel[$level] = 0;
        }
        $this->countsPerLogLevel[$level]++;

        $this->inner->log($level, $message, $context);
    }

    public function getLoggedText(): string
    {
        return stream_get_contents($this->logStream, -1, 0);
    }

    public function getNumberOfWarnings(): int
    {
        return $this->countsPerLogLevel[LogLevel::WARNING] ?? 0;
    }

    public function getNumberOfErrors(): int
    {
        return $this->countsPerLogLevel[LogLevel::ERROR] ?? 0;
    }
}
