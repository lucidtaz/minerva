<?php

namespace App\Services;

use App\Dtos\RenderedAllocation;
use App\Models\Allocation;
use App\Models\Asset;
use Carbon\Carbon;
use JetBrains\PhpStorm\Pure;

class RebalancingService
{
    private AllocationService $allocationService;

    public function __construct(AllocationService $allocationService)
    {
        $this->allocationService = $allocationService;
    }

    /**
     * @return RenderedAllocation|null The rendered Allocation if unbalanced, or null if balanced
     */
    public function checkForUnbalance(Allocation $allocation, Asset $displayAsset): ?RenderedAllocation
    {
        $now = Carbon::now();

         $renderedAllocation = $this->allocationService->render($allocation, $displayAsset, $now);

        if ($this->needsRebalancing($renderedAllocation)) {
            return $renderedAllocation;
        }

        return null;
    }

    #[Pure]
    private function needsRebalancing(RenderedAllocation $renderedAllocation): bool
    {
        return bccomp(
            bcmul($renderedAllocation->totalDeviationRatio, '100'),
            $renderedAllocation->allocation->tolerance
        ) === 1;
    }
}
