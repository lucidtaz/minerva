<?php

namespace App\Services;

use App\Models\Asset;
use App\Models\Price;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Enumerable;

class AssetPriceService
{
    public function calculatePrice(Asset $asset, ?Asset $base, Carbon $moment): Price
    {
        if ($base === null) {
            $price = new Price([
                'value' => '0.00000000',
                'moment' => Carbon::minValue(),
            ]);
            $price->asset()->associate($asset);
            return $price;
        }

        /** @var Price|null $price */
        $price = $asset->prices
            ->filter(
                fn (Price $price) => !$price->moment->isAfter($moment)
            )
            ->sortByDesc('moment')
            ->first();

        if ($price === null) {
            $price = new Price([
                'value' => $asset->id === $base->id ? '1.00000000' : '0.00000000', // Same number of decimals as what's in the database
                'moment' => Carbon::minValue(),
            ]);
            $price->asset()->associate($asset);
            $price->baseAsset()->associate($base);
        }

        $this->convertBase(new Collection([$price]), $base);
        return $price;
    }

    /**
     * @param Enumerable&Price[] $prices
     * @param Asset $base
     */
    public function convertBase(Enumerable $prices, Asset $base): void
    {
        $priceBaseAssets = $prices
            ->pluck('baseAsset')
            ->flatten()
            ->unique()
            // Remove the requested base asset from base assets that we need to
            // convert, since we would never need to do that
            ->reject(fn (Asset $priceBaseAsset) => $priceBaseAsset->id === $base->id);

        $priceBaseAssetPricesMap = $priceBaseAssets
            ->mapWithKeys(fn (Asset $baseAsset) => [
                $baseAsset->id => $baseAsset
                    ->prices
                    ->filter(fn (Price $price) => $price->base_asset_id === $base->id)
                    ->sortByDesc('moment')
            ]);

        // Note: we modify the Price collection by multiplying each original
        // Price with the (at that time) most recent base Asset's Price. This
        // means we do NOT inject new Price records where the base Asset Price
        // changed; if we have an infrequently updated Asset Price list with
        // frequently updated base Asset Prices, then we don't update the Asset
        // Price in between the original data points.

        foreach ($prices as $price) {
            if ($price->base_asset_id === $base->id) {
                $price->rebaseRate = '1';
                $price->rebasedValue = $price->value;
                $price->rebasedBase = $base;
            } else {
                // Convert the price to the requested $base Asset
                /** @var Collection&Price[] $baseAssetPrices */
                $baseAssetPrices = $priceBaseAssetPricesMap[$price->base_asset_id];

                /** @var Price|null $mostRecentBaseAssetPrice */
                $mostRecentBaseAssetPrice = $baseAssetPrices->first(
                    fn(Price $baseAssetPrice) => !$baseAssetPrice->moment->isAfter($price->moment)
                );

                // We found our conversion price. We could shrink the base asset
                // prices so the next search will be faster, but that is a potential
                // optimization left for later.

                if ($mostRecentBaseAssetPrice === null) {
                    // The base asset has no prices as early as the one we need.
                    // By returning zero this fact should become obvious to the
                    // user when looking at the graph. Later we may append a
                    // warning text to the result.
                    $price->rebaseRate = '0';
                    $price->rebasedValue = '0';
                    $price->rebasedBase = $base;
                } else {
                    $price->rebaseRate = $mostRecentBaseAssetPrice->value;
                    $price->rebasedValue = bcmul($price->value, $mostRecentBaseAssetPrice->value);
                    $price->rebasedBase = $base;
                }
            }
        }
    }
}
