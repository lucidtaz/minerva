<?php

namespace App\Services\Graph;

enum Resolution: string
{
    case Day = 'day';
    case Week = 'week';
    case Month = 'month';
}
