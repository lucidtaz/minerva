<?php

namespace App\Services\Graph;

enum EmptyBinMode
{
    case Skip;
    case Previous;
    case Zero;
}
