<?php

namespace App\Services\Graph;

use Carbon\CarbonImmutable;
use Carbon\CarbonTimeZone;

class GraphControls
{
    public CarbonImmutable $start;
    public CarbonImmutable $end;
    public Resolution $resolution;

    private CarbonTimeZone $timezone;

    public function __construct(?CarbonImmutable $start = null, ?CarbonImmutable $end = null, Resolution $resolution = Resolution::Week)
    {
        $this->start = $start ?? CarbonImmutable::minValue();
        $this->end = $end ?? CarbonImmutable::maxValue();
        $this->resolution = $resolution;
    }

    public function setTimezone(CarbonTimeZone $timezone): void
    {
        $this->timezone = $timezone;
        $this->start->setTimezone($timezone);
        $this->end->setTimezone($timezone);
    }

    public function setResolution(Resolution $resolution): void
    {
        $this->resolution = $resolution;
    }

    public function floorToResolution(CarbonImmutable $moment): CarbonImmutable
    {
        return match($this->resolution) {
            Resolution::Day => $moment->setTimezone($this->timezone)->startOfDay(),
            Resolution::Week => $moment->setTimezone($this->timezone)->startOfWeek(),
            Resolution::Month => $moment->setTimezone($this->timezone)->startOfMonth(),
        };
    }

    public function ceilToResolution(CarbonImmutable $moment): CarbonImmutable
    {
        return match($this->resolution) {
            Resolution::Day => $moment->setTimezone($this->timezone)->endOfDay(),
            Resolution::Week => $moment->setTimezone($this->timezone)->endOfWeek(),
            Resolution::Month => $moment->setTimezone($this->timezone)->endOfMonth(),
        };
    }

    public function successor(CarbonImmutable $moment): CarbonImmutable
    {
        return match($this->resolution) {
            Resolution::Day => $moment->addDay(),
            Resolution::Week => $moment->addWeek(),
            Resolution::Month => $moment->addMonth(),
        };
    }

    public function predecessor(CarbonImmutable $moment): CarbonImmutable
    {
        return match($this->resolution) {
            Resolution::Day => $moment->subDay(),
            Resolution::Week => $moment->subWeek(),
            Resolution::Month => $moment->subMonth(),
        };
    }
}
