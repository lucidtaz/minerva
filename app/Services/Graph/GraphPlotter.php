<?php

namespace App\Services\Graph;

use App\Dtos\NumberAtMoment;
use Carbon\CarbonImmutable;
use Illuminate\Support\Collection;

class GraphPlotter
{
    /**
     * @param Collection&NumberAtMoment[] $dataPoints
     * @return Collection&NumberAtMoment[]
     */
    public function averageIntoDateBins(Collection $dataPoints, GraphControls $graphControls, EmptyBinMode $emptyBinMode): Collection
    {
        $result = collect([]);

        $dataPoints = collect($dataPoints->sort(fn (NumberAtMoment $a, NumberAtMoment $b) => $a->moment <=> $b->moment));

        if ($dataPoints->isEmpty()) {
            $start = $graphControls->floorToResolution(CarbonImmutable::now());
            $end = $graphControls->ceilToResolution(CarbonImmutable::now());
        } else {
            $minMoment = $dataPoints->first()->moment;
            $maxMoment = $dataPoints->last()->moment;
            $start = $graphControls->floorToResolution($graphControls->start->max($minMoment));
            $end = $graphControls->ceilToResolution($graphControls->end->min($maxMoment));
        }

        for ($moment = $start; $moment < $end; $moment = $graphControls->successor($moment)) {
            $nextMoment = $graphControls->successor($moment);

            $isInBucket = static fn (NumberAtMoment $numberAtMoment) => $numberAtMoment->moment->isBetween($moment, $nextMoment);
            $initial = new NumberAtMoment('0', $moment);
            $add = fn (NumberAtMoment $acc, NumberAtMoment $numberAtMoment) => new NumberAtMoment(bcadd($acc->number, $numberAtMoment->number), $acc->moment);

            // By relying on a sorted input we only need to inspect the first elements without iterating over the entire collection
            $dataPointsInBucket = collect([]);
            foreach ($dataPoints as $key => $dataPoint) {
                if (!$isInBucket($dataPoint)) {
                    break;
                }
                $dataPointsInBucket->push($dataPoint);
                $dataPoints->offsetUnset($key);
            }

            /** @var NumberAtMoment $reducedDataPoint */
            $reducedDataPoint = $dataPointsInBucket
                ->reduce($add, $initial);

            if ($dataPointsInBucket->isEmpty()) {
                if ($emptyBinMode === EmptyBinMode::Zero) {
                    $result->push($reducedDataPoint);
                } elseif ($emptyBinMode === EmptyBinMode::Previous && $result->isNotEmpty()) {
                    $result->push(new NumberAtMoment(
                        $result->last()->number,
                        $reducedDataPoint->moment,
                    ));
                }
            } else {
                $averagedDataPoint = new NumberAtMoment(bcdiv($reducedDataPoint->number, $dataPointsInBucket->count()), $reducedDataPoint->moment);
                $result->push($averagedDataPoint);
            }
        }

        return $result;
    }

    /**
     * @param Collection&NumberAtMoment[] $dataPoints
     * @return Collection&NumberAtMoment[]
     */
    public function padRight(Collection $dataPoints, CarbonImmutable $end, GraphControls $graphControls): Collection
    {
        if ($dataPoints->isEmpty()) {
            // No idea where to pad from
            return $dataPoints;
        }

        $maxMoment = $dataPoints->last()->moment;
        $lastAvailable = $graphControls->floorToResolution($maxMoment);
        $start = $graphControls->successor($lastAvailable);

        for ($moment = $start; $moment <= $end; $moment = $graphControls->successor($moment)) {
            $dataPoints->push(new NumberAtMoment($dataPoints->last()->number, $moment));
        }

        return $dataPoints;
    }

    /**
     * @param Collection<int, NumberAtMoment> $dataPoints
     * @return Collection<int, NumberAtMoment>
     */
    public function padLeft(Collection $dataPoints, CarbonImmutable $start, GraphControls $graphControls): Collection
    {
        if ($dataPoints->isEmpty()) {
            // No idea where to pad to
            return $dataPoints;
        }

        $minMoment = $dataPoints->first()->moment;
        $firstAvailable = $graphControls->floorToResolution($minMoment);
        $end = $graphControls->predecessor($firstAvailable);

        $result = collect([]);
        for ($moment = $start; $moment <= $end; $moment = $graphControls->successor($moment)) {
            $result->push(new NumberAtMoment('0', $moment));
        }

        return $result->concat($dataPoints);
    }
}
