<?php

namespace App\Services;

use Carbon\CarbonImmutable;
use Carbon\CarbonPeriod;

class DateTimeHelper
{
    public static function timeIsInRange(string $time, string $timezone, CarbonImmutable $from, CarbonImmutable $to): bool
    {
        $localFrom = $from->shiftTimezone($timezone);
        $localTo = $to->shiftTimezone($timezone);

        $localDates = CarbonPeriod::between($localFrom->startOfDay(), $localTo->startOfDay());
        foreach ($localDates as $localDate) {
            $dateTime = (new CarbonImmutable($localDate, $timezone))->setTimeFrom($time);
            if (!$dateTime->isValid()) {
                continue;
            }
            if ($dateTime->betweenIncluded($from, $to)) {
                return true;
            }
        }

        return false;
    }
}
