<?php

namespace App\Services;

use App\Dtos\NumberAtMoment;
use App\Dtos\Value;
use App\Models\Asset;
use App\Models\Price;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Enumerable;

class AssetValueService
{
    private AssetPriceService $assetPriceService;

    public function __construct(AssetPriceService $assetPriceService)
    {
        $this->assetPriceService = $assetPriceService;
    }

    public function calculateAmount(Asset $asset, Carbon $moment): string
    {
        return
            $asset->transactions->filter(
                fn (Transaction $transaction) => !$transaction->moment->isAfter($moment)
            )->reduce(
                fn (string $acc, Transaction $transaction) => bcadd($acc, $transaction->amount),
                '0',
            );
    }

    public function calculateValue(Asset $asset, Asset $base, Carbon $moment): Value
    {
        $amount = $this->calculateAmount($asset, $moment);
        $price = $this->assetPriceService->calculatePrice($asset, $base, $moment);
        return new Value($asset, $base, bcmul($amount, $price->rebasedValue), $moment->min($price->moment));
    }

    /**
     * @return Enumerable<int, NumberAtMoment>
     */
    public function calculateValueHistory(Asset $asset, Asset $base): Enumerable
    {
        $result = new Collection();

        $prices = $asset->prices->sortBy('moment');
        $transactions = $asset->transactions->sortBy('moment');

        $this->assetPriceService->convertBase($prices, $base);

        $currentDate = null;
        $currentPrice = '0';
        $currentAmount = '0';
        $transactionsSeen = 0;
        while ($prices->isNotEmpty() || $transactions->isNotEmpty()) {
            /** @var Transaction|null $transaction */
            $transaction = $transactions->first();
            /** @var Price|null $price */
            $price = $prices->first();

            if ($price === null) {
                $currentDate = $transaction->moment;
                $currentAmount = bcadd($currentAmount, $transaction->amount);
                $transactionsSeen++;
                $transactions->shift();
            } elseif ($transaction === null) {
                $currentDate = $price->moment;
                $currentPrice = $price->rebasedValue;
                $prices->shift();
            } elseif ($transaction->moment->equalTo($price->moment)) {
                $currentDate = $transaction->moment;
                $currentAmount = bcadd($currentAmount, $transaction->amount);
                $currentPrice = $price->rebasedValue;
                $transactionsSeen++;
                $transactions->shift();
                $prices->shift();
            } elseif ($transaction->moment->isBefore($price->moment)) {
                $currentDate = $transaction->moment;
                $currentAmount = bcadd($currentAmount, $transaction->amount);
                $transactionsSeen++;
                $transactions->shift();
            } else {
                $currentDate = $price->moment;
                $currentPrice = $price->rebasedValue;
                $prices->shift();
            }

            if ($transactionsSeen === 0) {
                // We have no amount yet so it doesn't make sense to include
                // this data point in the value. All it means is that there is a
                // known price.
                continue;
            }
            $result->add(new NumberAtMoment(bcmul($currentPrice, $currentAmount), $currentDate->toImmutable()));
        }

        return $result;
    }

    /**
     * @return Enumerable<int, NumberAtMoment>
     */
    public function calculateInvestmentHistory(Asset $asset, Asset $base): Enumerable
    {
        $result = new Collection();

        $prices = $asset->prices->sortBy('moment');
        $transactions = $asset->transactions->sortBy('moment');

        $this->assetPriceService->convertBase($prices, $base);

        $currentDate = null;
        $currentPrice = '0';
        $currentAmount = '0';
        $currentInvestment = '0';
        $transactionsSeen = 0;
        while ($prices->isNotEmpty() || $transactions->isNotEmpty()) {
            /** @var Transaction|null $transaction */
            $transaction = $transactions->first();
            /** @var Price|null $price */
            $price = $prices->first();

            $isTransactionIteration = false;

            if ($price === null) {
                $currentDate = $transaction->moment;
                $currentAmount = bcadd($currentAmount, $transaction->amount);
                $transactionsSeen++;
                $transactions->shift();

                $isTransactionIteration = true;
            } elseif ($transaction === null) {
                $currentDate = $price->moment;
                $currentPrice = $price->rebasedValue;
                $prices->shift();
            } elseif ($transaction->moment->equalTo($price->moment)) {
                $currentDate = $transaction->moment;
                $currentAmount = bcadd($currentAmount, $transaction->amount);
                $currentPrice = $price->rebasedValue;
                $transactionsSeen++;
                $transactions->shift();
                $prices->shift();

                $isTransactionIteration = true;
            } elseif ($transaction->moment->isBefore($price->moment)) {
                $currentDate = $transaction->moment;
                $currentAmount = bcadd($currentAmount, $transaction->amount);
                $transactionsSeen++;
                $transactions->shift();

                $isTransactionIteration = true;
            } else {
                $currentDate = $price->moment;
                $currentPrice = $price->rebasedValue;
                $prices->shift();
            }

            if ($transactionsSeen === 0) {
                // We have no amount yet so it doesn't make sense to include
                // this data point in the value. All it means is that there is a
                // known price.
                continue;
            }
            if (!$isTransactionIteration) {
                continue;
            }
            $investment = bcmul($currentPrice, $transaction->amount);
            $currentInvestment = bcadd($currentInvestment, $investment);
            $result->add(new NumberAtMoment($currentInvestment, $currentDate->toImmutable()));
        }

        // Last data point at end of graph; even though there is no Transaction
        // of "today", there will be a Price of "today" so we should show the
        // investment of "today" as well.
        if ($currentDate !== null) {
            $result->add(new NumberAtMoment($currentInvestment, $currentDate->toImmutable()));
        }

        return $result;
    }
}
