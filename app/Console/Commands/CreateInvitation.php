<?php

namespace App\Console\Commands;

use App\Models\Invitation;
use Illuminate\Console\Command;
use Ramsey\Uuid\Uuid;

class CreateInvitation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invitation:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new invitation for user registration';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        $invitation = new Invitation([
            'token' => (string) Uuid::uuid4(),
            'note' => 'Created on the command line',
        ]);
        $invitation->save();

        $this->line(route('register') . '?invitation_token=' . $invitation->token);
    }
}
