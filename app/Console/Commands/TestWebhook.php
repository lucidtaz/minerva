<?php

namespace App\Console\Commands;

use App\Jobs\SendWebhook;
use App\Models\Webhook;
use Illuminate\Console\Command;

class TestWebhook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webhook:test {uri}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a test webhook to verify that the webhook configuration is working';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        $uri = $this->argument('uri');

        $webhook = new Webhook([
            'uri' => $uri,
        ]);
        $webhook->shared_secret = base64_encode(random_bytes(24));

        dispatch_sync(new SendWebhook($webhook, 'This is a sample webhook message.', 'text/plain'));

        $this->line('Webhook sent.');
        $this->info('The shared secret used to create the signature is ' . $webhook->shared_secret);
    }
}
