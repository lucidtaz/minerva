<?php

namespace App\Console\Commands;

use App\Jobs\SendWebhook;
use App\Models\Allocation;
use App\Models\ScheduledCheckExecution;
use App\Notifications\AllocationIsUnbalanced;
use App\Presentation\RebalancingReporter;
use App\Services\DateTimeHelper;
use App\Services\RebalancingService;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Notification;

class CheckUnbalancedAllocations extends Command
{
    protected $signature = 'check:unbalanced-allocations';

    protected $description = 'Check allocations to see whether they are in need of rebalancing';

    public function __construct(private RebalancingService $rebalancingService, private RebalancingReporter $rebalancingReporter)
    {
        parent::__construct();
    }

    public function handle(): int
    {
        /** @var $timeFrom CarbonImmutable */
        /** @var $timeTo CarbonImmutable */
        [$timeFrom, $timeTo] = $this->determineTimes();

        $execution = new ScheduledCheckExecution([
            'command' => self::class,
            'time_from' => $timeFrom,
            'time_to' => $timeTo,
            'finished' => false,
            'outcome' => 'In progress...',
        ]);
        $execution->save();

        $checkableAllocations = $this->findCheckableAllocations($timeFrom, $timeTo);
        $unbalancedAllocationsFound = 0;
        foreach ($checkableAllocations as $allocation) {
            $currency = $allocation->checkCurrency;

            $maybeUnbalancedRenderedAllocation = $this->rebalancingService->checkForUnbalance($allocation, $currency);
            if ($maybeUnbalancedRenderedAllocation !== null) {
                $unbalancedAllocationsFound++;

                $report = $this->rebalancingReporter->formatSummaryTextLines($maybeUnbalancedRenderedAllocation);
                $payload = json_encode([
                    'event' => 'allocation-unbalanced',
                    'allocation' => $allocation->toArray(),
                    'report' => $report,
                ], JSON_THROW_ON_ERROR);

                dispatch(new SendWebhook(
                    $allocation->checkWebhook,
                    $payload,
                    'application/json',
                ));

                $users = $allocation->organization->users;
                Notification::send($users, new AllocationIsUnbalanced($allocation));
            }
        }

        $execution->finished = true;
        $execution->outcome = sprintf(
            'Checked %d allocations and found %d out of balance',
            $checkableAllocations->count(),
            $unbalancedAllocationsFound,
        );
        $execution->save();

        return 0;
    }

    private function determineTimes(): array
    {
        /** @var ScheduledCheckExecution|null $last */
        $last = ScheduledCheckExecution::query()
            ->where('command', '=', self::class)
            ->where('finished', '=', true)
            ->orderByDesc('time_to')
            ->first();

        if ($last === null) {
            return [CarbonImmutable::now()->subMinutes(15), CarbonImmutable::now()];
        }

        return [$last->time_to->addSecond(), CarbonImmutable::now()];
    }

    /**
     * @return Collection&Allocation[]
     */
    private function findCheckableAllocations(CarbonImmutable $timeFrom, CarbonImmutable $timeTo): Collection
    {
        return Allocation::query()
            ->whereNotNull('check_at')
            ->whereNotNull('check_timezone')
            ->get()
            ->filter(fn (Allocation $allocation) => DateTimeHelper::timeIsInRange(
                $allocation->check_at,
                $allocation->check_timezone,
                $timeFrom,
                $timeTo,
            ));
    }
}
