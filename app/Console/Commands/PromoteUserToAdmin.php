<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class PromoteUserToAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:make-admin {email} {--reverse}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a (site wide) admin out of an existing user';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        $user = User::query()
            ->where('email', '=', $this->argument('email'))
            ->first();

        if ($user === null) {
            $this->error('Email not found.');
            return;
        }

        $reverse = $this->option('reverse');

        $user->is_admin = !$reverse;
        $user->save();

        if ($reverse) {
            $this->line('User ' . $user->email . ' is demoted to non-admin.');
        } else {
            $this->line('User ' . $user->email . ' is promoted to admin.');
        }
    }
}
