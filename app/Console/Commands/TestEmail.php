<?php

namespace App\Console\Commands;

use App\Mail\Test;
use Illuminate\Console\Command;
use Mail;

class TestEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:test {to}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a test email to verify that the email configuration is working';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        $to = $this->argument('to');

        Mail::to($to)
            ->send(new Test());

        $this->line('Email sent.');
    }
}
