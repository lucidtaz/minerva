<?php

namespace App\Models;

use App\Casts\CarbonDateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Price extends Model
{
    use HasFactory;

    public string $rebaseRate = '0';
    public string $rebasedValue = '0';
    public ?Asset $rebasedBase = null;

    protected $fillable = [
        'base_asset_id',
        'value',
        'moment',
        'import_id', // Unique string used to deduplicate imported entities
    ];

    protected $casts = [
        'value' => 'string', // To be able to use bcmath
        'moment' => CarbonDateTime::class,
    ];

    public function asset(): BelongsTo
    {
        return $this->belongsTo(Asset::class);
    }

    public function baseAsset(): BelongsTo
    {
        return $this->belongsTo(Asset::class, 'base_asset_id');
    }

    public function importExecution(): BelongsTo
    {
        return $this->belongsTo(ImportExecution::class);
    }
}
