<?php

namespace App\Models;

use App\Enums\OrganizationUserRole;
use Illuminate\Database\Eloquent\Relations\Pivot;

class OrganizationUser extends Pivot
{
    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role',
    ];

    protected $casts = [
        'role' => OrganizationUserRole::class,
    ];
}
