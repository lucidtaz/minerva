<?php

namespace App\Models;

use App\Casts\CarbonDateTime;
use App\Enums\OrganizationUserRole;
use App\Scopes\ExpiringScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * User invitation
 *
 * There are generally two types:
 * 1. The user can register themselves onto the platform, in case open
 *    registration is disabled in the application configuration.
 * 2. The user registers and will become a member of the specified organization,
 *    with a specified role.
 */
class Invitation extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'token',
        'note',
        'role',
        'expires_at',
    ];

    protected $casts = [
        'role' => OrganizationUserRole::class,
        'expires_at' => CarbonDateTime::class,
    ];

    protected static function booted(): void
    {
        static::addGlobalScope(new ExpiringScope());
    }

    public function organization(): BelongsTo
    {
        return $this->belongsTo(Organization::class);
    }
}
