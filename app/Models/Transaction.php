<?php

namespace App\Models;

use App\Casts\CarbonDateTime;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'amount',
        'note',
        'location',
        'moment',
        'import_id', // Unique string used to deduplicate imported entities
        'is_temporary',
    ];

    protected $casts = [
        'amount' => 'string', // To be able to use bcmath
        'moment' => CarbonDateTime::class,
        'is_temporary' => 'boolean',
    ];

    public function getIsExpiredTemporaryAttribute(): bool
    {
        if (!$this->is_temporary) {
            return false;
        }

        $threshold = CarbonImmutable::now()->subDays(config('app.temporary_transactions_expire_days'));
        return $this->created_at->isBefore($threshold);
    }

    public function asset(): BelongsTo
    {
        return $this->belongsTo(Asset::class);
    }

    public function importExecution(): BelongsTo
    {
        return $this->belongsTo(ImportExecution::class);
    }
}
