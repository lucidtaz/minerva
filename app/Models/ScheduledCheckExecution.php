<?php

namespace App\Models;

use App\Casts\CarbonDateTime;
use Illuminate\Database\Eloquent\Model;

class ScheduledCheckExecution extends Model
{
    protected $fillable = [
        'command',
        'time_from',
        'time_to',
        'finished',
        'outcome',
    ];

    protected $casts = [
        'finished' => 'boolean',
        'time_from' => CarbonDateTime::class,
        'time_to' => CarbonDateTime::class,
    ];
}
