<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use JetBrains\PhpStorm\Pure;

/**
 * Alternate name for an Asset
 *
 * This facilitates the import process, by being able to accept multiple names
 * of the same logical Asset, for example when importing from multiple sources.
 */
class AssetAlias extends Model
{
    protected $fillable = [
        'alias',
    ];

    #[Pure]
    public function hasSameName(self $that): bool
    {
        return strtolower(trim($this->alias)) === strtolower(trim($that->alias));
    }

    public function asset(): BelongsTo
    {
        return $this->belongsTo(Asset::class);
    }

    public function organization(): BelongsTo
    {
        return $this->belongsTo(Organization::class);
    }
}
