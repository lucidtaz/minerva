<?php

namespace App\Models;

use App\Enums\AllocationViewMode;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Allocation extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'tolerance',
        'check_at',
        'check_timezone',
    ];

    protected $casts = [
        'tolerance' => 'string', // To be able to use bcmath
    ];

    protected $appends = [
        'link',
    ];

    protected $visible = [
        'name',
        'tolerance',
        'check_at',
        'check_timezone',
        'link',
    ];

    public function unsetCheck(): void
    {
        $this->check_at = null;
        $this->check_timezone = null;
        $this->checkCurrency()->dissociate();
        $this->checkWebhook()->dissociate();
    }

    public function getLinkAttribute(): string
    {
        return route('allocations.show', [$this, AllocationViewMode::Detailed->value]);
    }

    public function organization(): BelongsTo
    {
        return $this->belongsTo(Organization::class);
    }

    public function parentAllocations(): MorphToMany
    {
        return $this->morphToMany(self::class, 'allocatable')
            ->withPivot(['id', 'weight']);
    }

    public function childAllocations(): MorphToMany
    {
        return $this->morphedByMany(self::class, 'allocatable')
            ->withPivot(['id', 'weight']);
    }

    public function assets(): MorphToMany
    {
        return $this->morphedByMany(Asset::class, 'allocatable')
            ->withPivot(['id', 'weight']);
    }

    public function checkWebhook(): BelongsTo
    {
        return $this->belongsTo(Webhook::class, 'check_webhook_id', 'id');
    }

    public function checkCurrency(): BelongsTo
    {
        return $this->belongsTo(Asset::class, 'check_currency_id', 'id');
    }
}
