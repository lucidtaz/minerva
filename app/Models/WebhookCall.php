<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class WebhookCall extends Model
{
    use HasFactory;

    protected $fillable = [
        'request',
        'debug',
        'response',
        'status_code',
        'error',
        'short_error',
        'duration_ms',
    ];

    public function getIsErrorAttribute(): bool
    {
        return !empty($this->error);
    }

    public function webhook(): BelongsTo
    {
        return $this->belongsTo(Webhook::class);
    }
}
