<?php

namespace App\Models;

use App\Casts\CarbonTimeZone;
use App\Enums\EntityType;
use App\Enums\InputContentType;
use App\Enums\InputMethod;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class ImportConfiguration extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'note',
        'input_method',
        'input_content_type',
        'date_format',
        'timezone',
        'url',
        'has_header_row',
        'delimiter',
        'enclosure_character',
        'escape_character',
        'entity_type',
        'parse_expression',
    ];

    protected $casts = [
        'timezone' => CarbonTimeZone::class,
        'input_method' => InputMethod::class,
        'input_content_type' => InputContentType::class,
        'entity_type' => EntityType::class,
    ];

    public function getReadableMethodAttribute(): string
    {
        if ($this->input_method === InputMethod::Download) {
            return "{$this->input_method->value} {$this->entity_type->value} {$this->input_content_type->value} from $this->url";
        }
        return "{$this->input_method->value} {$this->entity_type->value} {$this->input_content_type->value}";
    }

    public function organization(): BelongsTo
    {
        return $this->belongsTo(Organization::class);
    }

    public function asset(): BelongsTo
    {
        return $this->belongsTo(Asset::class);
    }

    public function importExecutions(): HasMany
    {
        return $this->hasMany(ImportExecution::class);
    }
}
