<?php

namespace App\Models;

use App\Enums\AssetMode;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asset extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'currency_symbol',
        'import_id',
        'mode',
    ];

    protected $casts = [
        'mode' => AssetMode::class,
    ];

    public function getIsCurrencyAttribute(): bool
    {
        return $this->currency_symbol !== null;
    }

    public function scopeCurrency(Builder $query): Builder
    {
        return $query->whereNotNull('currency_symbol');
    }

    public function organization(): BelongsTo
    {
        return $this->belongsTo(Organization::class);
    }

    public function prices(): HasMany
    {
        return $this->hasMany(Price::class);
    }

    public function transactions(): HasMany
    {
        return $this->hasMany(Transaction::class);
    }

    public function aliases(): HasMany
    {
        return $this->hasMany(AssetAlias::class);
    }

    public function importExecution(): BelongsTo
    {
        return $this->belongsTo(ImportExecution::class);
    }

    public function allocations(): MorphToMany
    {
        return $this->morphToMany(Allocation::class, 'allocatable')
            ->withPivot(['id', 'weight']);
    }
}
