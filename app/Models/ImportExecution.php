<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ImportExecution extends Model
{
    protected $fillable = [
        'log',
        'entities_created',
        'warnings',
        'errors',
    ];

    public function importConfiguration(): BelongsTo
    {
        return $this->belongsTo(ImportConfiguration::class);
    }

    public function assets(): HasMany
    {
        return $this->hasMany(Asset::class);
    }

    public function prices(): HasMany
    {
        return $this->hasMany(Price::class);
    }

    public function transactions(): HasMany
    {
        return $this->hasMany(Transaction::class);
    }
}
