<?php

namespace App\Models;

use App\Casts\CarbonTimeZone;
use App\Enums\OrganizationUserRole;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'display_asset_id',
        'timezone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'timezone' => CarbonTimeZone::class,
        'is_admin' => 'boolean',
    ];

    public function hasRoleInside(Organization $organization, OrganizationUserRole $role): bool
    {
        $matchingOrganization = $this->getMaybeOrganization($organization->id);

        if ($matchingOrganization === null) {
            // No membership at all
            return false;
        }

        return $matchingOrganization->pivot->role === $role;
    }

    public function hasAnyRoleInside(Organization $organization): bool
    {
        $matchingOrganization = $this->getMaybeOrganization($organization->id);
        return $matchingOrganization !== null;
    }

    public function getMaybeOrganization(int $id): ?Organization
    {
        return $this
            ->organizations()
            ->withTrashed()
            ->where('organizations.id', '=', $id)
            ->first();
    }

    public function displayAsset(): BelongsTo
    {
        return $this->belongsTo(Asset::class);
    }

    public function organizations(): BelongsToMany
    {
        return $this->belongsToMany(Organization::class)
            ->using(OrganizationUser::class)
            ->withPivot('role')
            ->withTimestamps();
    }
}
