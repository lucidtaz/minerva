<?php

namespace App\Scopes;

use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class ExpiringScope implements Scope
{
    public function apply(Builder $builder, Model $model): void
    {
        $builder->where('expires_at', '>', new CarbonImmutable());
    }
}
