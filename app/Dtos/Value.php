<?php

namespace App\Dtos;

use App\Models\Asset;
use Carbon\Carbon;

/**
 * A computed monetary value
 *
 * This class is designed to mirror the functionality of the Price class, with
 * properties such as:
 *
 * - asset
 * - currency (base asset)
 * - numeric value
 * - moment
 *
 * The difference is that Price is an Eloquent model while Value isn't, since
 * it's computed instead of stored.
 */
class Value
{
    public readonly ?Asset $asset;
    public readonly Asset $baseAsset;
    public string $value;
    public readonly Carbon $moment;

    public function __construct(?Asset $asset, Asset $baseAsset, string $value, Carbon $moment)
    {
        $this->asset = $asset;
        $this->baseAsset = $baseAsset;
        $this->value = $value;
        $this->moment = $moment;
    }
}
