<?php

namespace App\Dtos;

use Illuminate\Support\Enumerable;

class DataSet
{
    /**
     * @param Enumerable&NumberAtMoment[] $data
     */
    public function __construct(public readonly string $title, public readonly Enumerable $data)
    {
        //
    }
}
