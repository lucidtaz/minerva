<?php

namespace App\Dtos;

use App\Models\Allocation;
use App\Models\Price;
use Carbon\CarbonInterface;

class AllocationLine
{
    /**
     * The Allocation of which this line is a part
     */
    public Allocation $allocation;

    /**
     * The Allocation that this line sums, which happens for sum lines of sub-
     * allocations
     */
    public ?Allocation $sumsAllocation;

    public ?string $url;
    public string $name;
    public int $weight;
    public ?Price $price;
    public ?string $amount;
    public Value $value;

    /**
     * The age of the Asset's current price, or that of the Allocation's most outdated child
     */
    public CarbonInterface $priceAge;

    public string $targetShare;
    public string $currentShare;
    public Value $targetValue;
    public Value $deltaValue;
    public ?Value $deltaValueInOriginalBase;
    public ?string $targetAmount;
    public ?string $deltaAmount;

    /**
     * @var AllocationLine[]
     */
    public array $subLines;

    /**
     * @param AllocationLine[] $subLines
     */
    public function __construct(
        Allocation $allocation,
        ?Allocation $sumsAllocation,
        ?string $url,
        string $name,
        int $weight,
        ?Price $price,
        ?string $amount,
        Value $value,
        CarbonInterface $priceAge,
        array $subLines = []
    ) {
        $this->allocation = $allocation;
        $this->sumsAllocation = $sumsAllocation;
        $this->url = $url;
        $this->name = $name;
        $this->weight = $weight;
        $this->price = $price;
        $this->amount = $amount;
        $this->value = $value;
        $this->priceAge = $priceAge;
        $this->subLines = $subLines;
    }

    public function setBalancingInformation(
        string $targetShare,
        string $currentShare,
        Value $targetValue,
        Value $deltaValue,
        ?Value $deltaValueInOriginalBase,
        ?string $targetAmount,
        ?string $deltaAmount,
    ): void
    {
        $this->targetShare = $targetShare;
        $this->currentShare = $currentShare;
        $this->targetValue = $targetValue;
        $this->deltaValue = $deltaValue;
        $this->deltaValueInOriginalBase = $deltaValueInOriginalBase;
        $this->targetAmount = $targetAmount;
        $this->deltaAmount = $deltaAmount;
    }
}
