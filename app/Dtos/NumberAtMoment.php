<?php

namespace App\Dtos;

use Carbon\CarbonImmutable;

class NumberAtMoment
{
    public function __construct(public readonly string $number, public readonly CarbonImmutable $moment)
    {
        //
    }
}
