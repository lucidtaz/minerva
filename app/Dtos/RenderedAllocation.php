<?php

namespace App\Dtos;

use App\Models\Allocation;

class RenderedAllocation
{
    public readonly Allocation $allocation;

    /**
     * @var AllocationLine[]
     */
    public readonly array $lines;

    public readonly AllocationLine $sum;

    public Value $totalDeviation;
    public string $totalDeviationRatio;

    /**
     * @param AllocationLine[] $lines
     */
    public function __construct(Allocation $allocation, array $lines, AllocationLine $sum)
    {
        $this->allocation = $allocation;
        $this->lines = $lines;
        $this->sum = $sum;
    }

    public function calculateDeviation(): void
    {
        $this->totalDeviation = new Value(null, $this->sum->value->baseAsset, '0', $this->sum->value->moment);
        foreach ($this->lines as $line) {
            if (bccomp($line->deltaValue->value, '0') === 1) {
                $this->totalDeviation->value = bcadd($this->totalDeviation->value, $line->deltaValue->value);
            } else {
                $this->totalDeviation->value = bcadd($this->totalDeviation->value, bcmul($line->deltaValue->value, '-1'));
            }
        }
        $this->totalDeviation->value = bcdiv($this->totalDeviation->value, '2'); // Deviation is counted double, positive on one asset and negative on the other

        if (bccomp($this->sum->value->value, '0') === 0) {
            $this->totalDeviationRatio = '0';
        } else {
            $this->totalDeviationRatio = bcdiv($this->totalDeviation->value, $this->sum->value->value);
        }
    }
}
