<?php

namespace App\Providers;

use App\Models\Allocation;
use App\Models\Asset;
use App\Models\ImportConfiguration;
use App\Models\Invitation;
use App\Models\Organization;
use App\Scopes\ExpiringScope;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Route::bind('trashedAsset', function ($uuid) {
            return Asset::query()
                ->onlyTrashed()
                ->where('uuid', '=', $uuid)
                ->firstOrFail();
        });
        Route::bind('trashedAllocation', function ($id) {
            return Allocation::query()
                ->onlyTrashed()
                ->where('id', '=', $id)
                ->firstOrFail();
        });
        Route::bind('trashedOrganization', function ($id) {
            return Organization::query()
                ->onlyTrashed()
                ->where('id', '=', $id)
                ->firstOrFail();
        });
        Route::bind('trashedImportConfiguration', function ($id) {
            return ImportConfiguration::query()
                ->onlyTrashed()
                ->where('id', '=', $id)
                ->firstOrFail();
        });
        Route::bind('maybeExpiredInvitation', function ($id) {
            return Invitation::query()
                ->withoutGlobalScope(ExpiringScope::class)
                ->where('id', '=', $id)
                ->firstOrFail();
        });
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }
}
