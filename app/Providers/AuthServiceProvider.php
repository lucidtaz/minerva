<?php

namespace App\Providers;

use App\Models\Allocation;
use App\Models\Asset;
use App\Models\ImportConfiguration;
use App\Models\ImportExecution;
use App\Models\Invitation;
use App\Models\Organization;
use App\Models\Price;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Webhook;
use App\Models\WebhookCall;
use App\Policies\AllocationPolicy;
use App\Policies\AssetPolicy;
use App\Policies\ExecutionPolicy;
use App\Policies\ImportPolicy;
use App\Policies\InvitationPolicy;
use App\Policies\NotificationPolicy;
use App\Policies\OrganizationPolicy;
use App\Policies\PricePolicy;
use App\Policies\TransactionPolicy;
use App\Policies\WebhookCallPolicy;
use App\Policies\WebhookPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Allocation::class => AllocationPolicy::class,
        Asset::class => AssetPolicy::class,
        ImportConfiguration::class => ImportPolicy::class,
        ImportExecution::class => ExecutionPolicy::class,
        Invitation::class => InvitationPolicy::class,
        Organization::class => OrganizationPolicy::class,
        Price::class => PricePolicy::class,
        Transaction::class => TransactionPolicy::class,
        Webhook::class => WebhookPolicy::class,
        WebhookCall::class => WebhookCallPolicy::class,
        DatabaseNotification::class => NotificationPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('admin-actions', function (User $user) {
            return $user->is_admin;
        });
    }
}
