<?php

namespace App\Providers;

use App\Models\Allocation;
use App\Models\Asset;
use Config;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ClientInterface::class, Client::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (function_exists('bcscale')) {
            // The reason for the check is that "artisan" sometimes runs without
            // all extensions installed, for example when building the project
            // instead of running it.
            bcscale(Config::get('bcmath.scale'));
        }

        Paginator::useBootstrap();

        Relation::morphMap([
            'allocation' => Allocation::class,
            'asset' => Asset::class,
        ]);
    }
}
