<?php

namespace App\Http\Controllers;

use App\Models\ImportExecution;
use Illuminate\Http\Request;

class ExecutionController extends Controller
{
    public function index(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('viewAny', [ImportExecution::class, $organization]);

        $importConfigurations = $organization->importConfigurations()
            ->withTrashed()
            ->get();

        $executions = ImportExecution::query()
            ->whereIn('import_configuration_id', $importConfigurations->pluck('id'))
            ->with('importConfiguration')
            ->withCount(['assets', 'prices', 'transactions'])
            ->orderBy('created_at', 'desc')
            ->paginate();

        return view('executions.index', [
            'executions' => $executions,
        ]);
    }

    public function show(ImportExecution $execution)
    {
        $this->authorize('view', $execution);

        $execution->loadCount(['assets', 'prices', 'transactions']);

        return view('executions.show', [
            'execution' => $execution,
        ]);
    }

    public function delete(ImportExecution $execution)
    {
        $this->authorize('delete', $execution);

        // "undo" the import, i.e. remove all imported entities, via cascading foreign keys
        $execution->delete();

        return redirect(route('executions.index'));
    }
}
