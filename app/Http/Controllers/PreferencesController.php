<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\CarbonTimeZone;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PreferencesController extends Controller
{
    public function edit(Request $request)
    {
        $organization = $this->getOrganization($request);

        $user = $request->user();

        $baseAssets = $organization->assets()->currency()->get();

        return view('preferences.edit', [
            'user' => $user,
            'baseAssets' => $baseAssets,
            'selectableTimezones' => CarbonTimeZone::listIdentifiers(),
        ]);
    }

    public function update(Request $request)
    {
        /** @var User $user */
        $user = $request->user();

        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignoreModel($user)],
            'display_asset_id' => ['exists:assets,id'],
            'timezone' => ['required', Rule::in(CarbonTimeZone::listIdentifiers())],
        ]);

        $user->fill($data);
        $user->saveOrFail();

        $request->session()->flash('update_successful', true);

        return redirect(route('preferences.edit'));
    }
}
