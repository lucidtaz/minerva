<?php

namespace App\Http\Controllers;

use App\Enums\AllocationViewMode;
use App\Models\Allocation;
use App\Models\Asset;
use App\Models\Webhook;
use App\Presentation\RebalancingReporter;
use App\Services\RebalancingService;
use Carbon\CarbonTimeZone;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Session;

class AllocationCheckController extends Controller
{
    public function __construct(
        private RebalancingService $rebalancingService
    ) {
        //
    }

    public function edit(Allocation $allocation)
    {
        $this->authorize('update', $allocation);

        $allocation->fill(Session::getOldInput());

        if (Session::hasOldInput('check_currency_id')) {
            $checkCurrency = $allocation->organization->assets()->where('id', '=', Session::getOldInput('check_currency_id'))->first();
            $allocation->checkCurrency()->associate($checkCurrency);
        }

        if (Session::hasOldInput('check_webhook_id')) {
            $checkWebhook = $allocation->organization->webhooks()->where('id', '=', Session::getOldInput('check_webhook_id'))->first();
            $allocation->checkWebhook()->associate($checkWebhook);
        }

        return view('allocations.edit_check', [
            'allocation' => $allocation,
            'timezones' => CarbonTimeZone::listIdentifiers(),
            'currencies' => $allocation->organization->assets()->currency()->get(),
            'webhooks' => $allocation->organization->webhooks,
        ]);
    }

    public function update(Request $request, Allocation $allocation)
    {
        $this->authorize('update', $allocation);

        $data = $request->validate([
            'check_at' => ['required', 'date_format:H:i:s'],
            'check_timezone' => ['required', 'timezone'],
            'check_currency_id' => ['required', Rule::exists(Asset::class, 'id')->where('organization_id', $allocation->organization->id)->whereNotNull('currency_symbol')],
            'check_webhook_id' => ['required', Rule::exists(Webhook::class, 'id')->where('organization_id', $allocation->organization->id)],
        ]);

        $allocation->fill($data);

        if ($request->has('check_currency_id')) {
            $checkCurrency = $allocation->organization->assets()->where('id', '=', $request->get('check_currency_id'))->firstOrFail();
            $allocation->checkCurrency()->associate($checkCurrency);
        }

        if ($request->has('check_webhook_id')) {
            $checkWebhook = $allocation->organization->webhooks()->where('id', '=', $request->get('check_webhook_id'))->firstOrFail();
            $allocation->checkWebhook()->associate($checkWebhook);
        }

        $allocation->save();

        return redirect(route('allocations.check.edit', $allocation));
    }

    public function delete(Request $request, Allocation $allocation)
    {
        $this->authorize('update', $allocation);

        $allocation->unsetCheck();
        $allocation->save();

        $request->session()->flash('delete_message', 'The check configuration has been cleared');

        return redirect(route('allocations.check.edit', $allocation));
    }

    /**
     * Check for rebalancing of any existing Allocations
     */
    public function check(Request $request, Allocation $allocation): JsonResponse
    {
        $this->authorize('check', $allocation);

        /** @var Asset|null $displayAsset */
        $displayAsset = $request->user()->displayAsset;

        if ($displayAsset === null) {
            return response()->json(
                ['error' => 'You must configure a display currency via your preferences before any values can be calculated'],
                422
            );
        }

        $maybeRenderedUnbalancedAllocation = $this->rebalancingService->checkForUnbalance(
            $allocation,
            $displayAsset
        );
        if ($maybeRenderedUnbalancedAllocation !== null) {
            $renderedUnbalancedAllocations = [$maybeRenderedUnbalancedAllocation];
        } else {
            $renderedUnbalancedAllocations = [];
        }

        $result = [];

        $reporter = new RebalancingReporter();
        foreach ($renderedUnbalancedAllocations as $renderedAllocation) {
            $result[] = [
                'name' => $renderedAllocation->allocation->name,
                'report' => $reporter->formatSummaryTextLines($renderedAllocation),
                'link' => route('allocations.show', [$renderedAllocation->allocation, AllocationViewMode::Detailed->value]),
            ];
        }

        return response()->json($result);
    }
}
