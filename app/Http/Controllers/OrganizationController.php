<?php

namespace App\Http\Controllers;

use App\Enums\OrganizationUserRole;
use App\Models\Organization;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Session;
use Throwable;

class OrganizationController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('viewAny', Organization::class);

        /** @var User $user */
        $user = $request->user();

        $organizations = $user->organizations()
            ->withCount('users')
            ->orderBy('name')
            ->paginate();

        return view('organizations.index', [
            'organizations' => $organizations,
        ]);
    }

    public function show(Organization $organization)
    {
        $this->authorize('view', $organization);

        return view('organizations.show', [
            'organization' => $organization,
        ]);
    }

    public function create()
    {
        $this->authorize('create', Organization::class);

        $organization = new Organization();

        $organization->fill(Session::getOldInput());

        return view('organizations.create', [
            'organization' => $organization,
        ]);
    }

    /**
     * @throws Throwable
     */
    public function store(Request $request)
    {
        $this->authorize('create', Organization::class);

        $user = $request->user();

        $data = $request->validate([
            'name' => ['required', 'max:255'],
        ]);

        $organization = new Organization($data);
        $organization->saveOrFail();

        $organization->users()->attach($user, ['role' => OrganizationUserRole::Admin]);

        $request->session()->put('organization_id', $organization->id);

        return redirect(route('organizations.show', $organization));
    }

    public function edit(Organization $organization)
    {
        $this->authorize('update', $organization);

        $organization->fill(Session::getOldInput());

        return view('organizations.edit', [
            'organization' => $organization,
        ]);
    }

    /**
     * @throws Throwable
     */
    public function update(Request $request, Organization $organization)
    {
        $this->authorize('update', $organization);

        $data = $request->validate([
            'name' => ['required', 'max:255'],
        ]);

        $organization->fill($data);
        $organization->saveOrFail();

        return redirect(route('organizations.show', $organization));
    }

    /**
     * @throws Exception
     */
    public function delete(Organization $organization)
    {
        $this->authorize('delete', $organization);

        $organization->delete();

        return redirect(route('organizations.index'));
    }

    public function deleted(Request $request)
    {
        $this->authorize('viewAny', Organization::class);

        /** @var User $user */
        $user = $request->user();

        $organizations = $user->organizations()
            ->onlyTrashed()
            ->withCount('users')
            ->orderBy('deleted_at', 'desc')
            ->paginate();

        return view('organizations.deleted', [
            'organizations' => $organizations,
        ]);
    }

    public function restore(Organization $trashedOrganization)
    {
        $this->authorize('restore', $trashedOrganization);

        $trashedOrganization->restore();

        return redirect(route('organizations.index'));
    }

    public function switch(Request $request, Organization $organization)
    {
        $this->authorize('switch', $organization);

        $request->session()->put('organization_id', $organization->id);

        return redirect(route('home'));
    }
}
