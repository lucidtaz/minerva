<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ScheduledCheckExecution;

class ScheduledChecksController extends Controller
{
    public function index()
    {
        $executions = ScheduledCheckExecution::query()
            ->orderByDesc('id')
            ->paginate();

        return view('admin.scheduled_check_executions.index', [
            'executions' => $executions,
        ]);
    }
}
