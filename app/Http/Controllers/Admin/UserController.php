<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::query()
            ->withCount('organizations')
            ->paginate();

        return view('admin.users.index', [
            'users' => $users,
        ]);
    }

    public function show(User $user)
    {
        return view('admin.users.show', [
            'user' => $user,
        ]);
    }

    public function promote(User $user)
    {
        $user->is_admin = true;
        $user->save();

        return redirect()->back();
    }

    public function demote(User $user)
    {
        $user->is_admin = false;
        $user->save();

        return redirect()->back();
    }
}
