<?php

namespace App\Http\Controllers;

use App\Models\Webhook;
use App\Models\WebhookCall;

class WebhookCallController extends Controller
{
    public function index(Webhook $webhook)
    {
        $this->authorize('viewAny', [WebhookCall::class, $webhook]);

        $calls = $webhook->calls()
            ->orderBy('created_at', 'desc')
            ->paginate();

        return view('webhookcalls.index', [
            'webhook' => $webhook,
            'calls' => $calls,
        ]);
    }

    public function show(Webhook $webhook, WebhookCall $call)
    {
        $this->authorize('view', $call);

        return view('webhookcalls.show', [
            'webhook' => $webhook,
            'call' => $call,
        ]);
    }
}
