<?php

namespace App\Http\Controllers;

use App\Dtos\DataSet;
use App\Http\Controllers\Display\RemembersDisplaySettings;
use App\Models\Allocation;
use App\Models\Asset;
use App\Services\AllocationService;
use App\Services\AssetValueService;
use App\Services\Graph\EmptyBinMode;
use App\Services\Graph\GraphControls;
use App\Services\Graph\GraphPlotter;
use App\Services\RebalancingService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Session;

class AllocationController extends Controller
{
    use RemembersDisplaySettings;

    public function __construct(
        private AllocationService $allocationService,
        private RebalancingService $rebalancingService,
        private AssetValueService $assetValueService,
        private GraphPlotter $graphPlotter
    ) {
        //
    }

    public function index(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('viewAny', [Allocation::class, $organization]);

        $allocations = $organization->allocations;

        return view('allocations.index', [
            'allocations' => $allocations,
        ]);
    }

    public function create(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('create', [Allocation::class, $organization]);

        $allocation = new Allocation([
            'tolerance' => '20',
        ]);

        $allocation->fill(Session::getOldInput());

        return view('allocations.create', [
            'allocation' => $allocation,
        ]);
    }

    public function store(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('create', [Allocation::class, $organization]);

        $data = $request->validate([
            'name' => ['required', 'max:255', 'unique:allocations'],
            'tolerance' => ['required', 'integer', 'between:0,99'],
        ]);
        $allocation = new Allocation($data);

        $allocation->organization()->associate($organization);

        $allocation->saveOrFail();

        return redirect(route('allocations.edit', $allocation));
    }

    public function show(Request $request, Allocation $allocation, string $view)
    {
        $this->authorize('view', $allocation);

        /** @var Asset|null $displayAsset */
        $displayAsset = $request->user()->displayAsset;

        if ($displayAsset === null) {
            $request->session()->flash('redirect_reason', 'You need to set a display currency before being able to view an allocation');
            return redirect(route('preferences.edit'));
        }

        if ($request->has('moment')) {
            $moment = Carbon::parse($request->get('moment'), $request->user()->timezone)->endOfDay();
        } else {
            $moment = Carbon::now()->endOfDay();
        }

        $renderedAllocation = $this->allocationService->render($allocation, $displayAsset, $moment);

        return view("allocations.show.$view", [
            'allocation' => $allocation,
            'renderedAllocation' => $renderedAllocation,
            'numberDisplayOptions' => $this->getNumberDisplayOptions(),
            'moment' => $moment,
        ]);
    }

    public function value(Request $request, Allocation $allocation)
    {
        $this->authorize('view', $allocation);

        /** @var Asset|null $displayAsset */
        $displayAsset = $request->user()->displayAsset;

        if ($displayAsset === null) {
            $request->session()->flash('redirect_reason', 'You need to set a display currency before being able to view an allocation');
            return redirect(route('preferences.edit'));
        }

        $graphControls = $this->getGraphControls($request->session(), $request->user()->timezone);

        $assets = $this->allocationService->getAllDescendentAssets($allocation);

        $assets->load(['prices.baseAsset']);

        $valueDataSets = $assets->map(fn (Asset $asset) => $this->calculateValueDataSet($asset, $displayAsset, $graphControls));

        $minMoment = $valueDataSets->min(fn (DataSet $dataSet) => $dataSet->data->first()?->moment);
        $maxMoment = $valueDataSets->max(fn (DataSet $dataSet) => $dataSet->data->last()?->moment);
        $valueDataSets = $valueDataSets->map(fn (DataSet $dataSet) => new DataSet($dataSet->title, $this->graphPlotter->padRight($this->graphPlotter->padLeft($dataSet->data, $minMoment, $graphControls), $maxMoment, $graphControls)));

        // Make the datasets stack in order of: earlier first appearance = lower, but if they contain no data, put them on top
        $stackingSorter = function (DataSet $a, DataSet $b): int {
            if ($a->data->isEmpty()) {
                return 1;
            }
            if ($b->data->isEmpty()) {
                return -1;
            }
            return $a->data->first()->moment <=> $b->data->first()->moment;
        };
        $valueDataSets = $valueDataSets->sort($stackingSorter)->values();

        return view('allocations.value', [
            'allocation' => $allocation,
            'dataSets' => $valueDataSets,
        ]);
    }

    private function calculateValueDataSet(Asset $asset, Asset $displayAsset, GraphControls $graphControls): DataSet
    {
        $valueHistory = $this->assetValueService->calculateValueHistory($asset, $displayAsset);
        $binnedValueHistory = $this->graphPlotter->averageIntoDateBins($valueHistory, $graphControls, EmptyBinMode::Previous);

        return new DataSet(
            $asset->name,
            $binnedValueHistory,
        );
    }

    public function edit(Allocation $allocation)
    {
        $this->authorize('update', $allocation);

        $totalWeight = $allocation->childAllocations->sum(fn (Allocation $childAllocation) => $childAllocation->pivot->weight)
            + $allocation->assets->sum(fn (Asset $asset) => $asset->pivot->weight);

        $descendentAssets = $this->allocationService->getAllDescendentAssets($allocation);
        $descendentAllocations = $this->allocationService->getAllDescendentAllocations($allocation);

        $selectableAssets = $allocation->organization->assets->diff($descendentAssets);
        $selectableAllocations = $allocation->organization->allocations()
            ->with(['assets'])
            ->get()
            // Filter out any Allocations that are already included
            ->diff($descendentAllocations)
            // Filter out any Allocations that have assets which are already included (TODO: their sub-allocations as well...)
            ->filter(fn (Allocation $selectableAllocation) => $selectableAllocation->assets->intersect($descendentAssets)->isEmpty());

        $allocation->fill(Session::getOldInput());

        return view('allocations.edit', [
            'allocation' => $allocation,
            'totalWeight' => $totalWeight,
            'selectableAssets' => $selectableAssets,
            'selectableAllocations' => $selectableAllocations,
        ]);
    }

    public function update(Request $request, Allocation $allocation)
    {
        $this->authorize('update', $allocation);

        $data = $request->validate([
            'name' => ['required', 'max:255', Rule::unique('allocations')->ignoreModel($allocation)],
            'tolerance' => ['required', 'integer', 'between:0,100'],
            'assetWeights.*' => ['required', 'integer', 'min:0'],
            'childAllocationWeights.*' => ['required', 'integer', 'min:0'],
            'unassignAssets.*' => ['boolean'],
            'unassignChildAllocations.*' => ['boolean'],
            'assignAssets.*' => ['boolean'],
            'assignChildAllocations.*' => ['boolean'],
        ]);

        $allocation->fill($data);

        // TODO: Add validation to guard against:
        // 1. Including the same asset multiple times, using sub-allocations
        // 2. Infinite loops by two allocations including each other

        foreach ($allocation->assets as $asset) {
            $asset->pivot->weight = $data['assetWeights'][$asset->pivot->id];
            if ($data['unassignAssets'][$asset->pivot->id] ?? false) {
                $allocation->assets()->detach($asset);
            }
        }
        foreach ($allocation->childAllocations as $childAllocation) {
            $childAllocation->pivot->weight = $data['childAllocationWeights'][$childAllocation->pivot->id];
            if ($data['unassignChildAllocations'][$childAllocation->pivot->id] ?? false) {
                $allocation->childAllocations()->detach($childAllocation);
            }
        }
        foreach ($data['assignAssets'] ?? [] as $assignAssetId => $shouldAssign) {
            if ($shouldAssign) {
                $allocation->assets()->attach($assignAssetId);
            }
        }
        foreach ($data['assignChildAllocations'] ?? [] as $assignChildAllocationId => $shouldAssign) {
            if ($shouldAssign) {
                $allocation->childAllocations()->attach($assignChildAllocationId);
            }
        }

        $allocation->push();

        return redirect(route('allocations.edit', $allocation));
    }

    /**
     * @throws Exception
     */
    public function delete(Allocation $allocation)
    {
        $this->authorize('delete', $allocation);

        $allocation->delete();

        return redirect(route('allocations.index'));
    }

    public function deleted(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('viewAny', $organization);

        $allocations = $organization->allocations()
            ->onlyTrashed()
            ->withCount(['assets', 'childAllocations'])
            ->orderBy('deleted_at', 'desc')
            ->paginate();

        return view('allocations.deleted', [
            'allocations' => $allocations,
        ]);
    }

    public function restore(Allocation $trashedAllocation)
    {
        $this->authorize('restore', $trashedAllocation);

        $trashedAllocation->restore();

        return redirect(route('allocations.index'));
    }
}
