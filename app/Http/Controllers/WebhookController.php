<?php

namespace App\Http\Controllers;

use App\Jobs\SendWebhook;
use App\Models\Webhook;
use Exception;
use Illuminate\Http\Request;
use Session;
use Throwable;

class WebhookController extends Controller
{
    public function index(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('viewAny', [Webhook::class, $organization]);

        $webhooks = $organization->webhooks()
            ->withCount('allocations')
            ->withCount('calls')
            ->orderBy('uri')
            ->paginate();

        return view('webhooks.index', [
            'webhooks' => $webhooks,
        ]);
    }

    public function create(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('create', [Webhook::class, $organization]);

        $webhook = new Webhook();

        $webhook->fill(Session::getOldInput());

        return view('webhooks.create', [
            'webhook' => $webhook,
        ]);
    }

    /**
     * @throws Throwable
     */
    public function store(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('create', [Webhook::class, $organization]);

        $data = $request->validate([
            'name' => ['required', 'max:255'],
            'uri' => ['required', 'max:255', 'url'],
        ]);

        $webhook = new Webhook($data);
        $webhook->shared_secret = base64_encode(random_bytes(24));
        $request->session()->flash('shared_secret', $webhook->shared_secret);

        $webhook->organization()->associate($organization);

        $webhook->saveOrFail();

        return redirect(route('webhooks.index'));
    }

    public function edit(Webhook $webhook)
    {
        $this->authorize('update', $webhook);

        $webhook->fill(Session::getOldInput());

        return view('webhooks.edit', [
            'webhook' => $webhook,
        ]);
    }

    /**
     * @throws Throwable
     */
    public function update(Request $request, Webhook $webhook)
    {
        $this->authorize('update', $webhook);

        $data = $request->validate([
            'name' => ['required', 'max:255'],
            'uri' => ['required', 'max:255', 'url'],
        ]);

        $webhook->fill($data);
        $webhook->saveOrFail();

        return redirect(route('webhooks.index'));
    }

    /**
     * @throws Exception
     */
    public function delete(Webhook $webhook)
    {
        $this->authorize('delete', $webhook);

        foreach ($webhook->allocations as $allocation) {
            $allocation->unsetCheck();
            $allocation->save();
        }

        $webhook->delete();

        return redirect(route('webhooks.index'));
    }

    public function test(Request $request, Webhook $webhook)
    {
        $this->authorize('test', $webhook);

        $job = new SendWebhook($webhook, 'This is a sample message', 'text/plain');
        $job->tries = 1; // Contrary to a system-sent webhook call, we don't need retries when manually testing...
        $this->dispatch($job);

        $request->session()->flash('test_success', 'The test request has been dispatched. It may take a couple of seconds before it completes. Note that automatic retries on failure have been disabled on this request.');

        return redirect()->back();
    }
}
