<?php

namespace App\Http\Controllers;

use App\Enums\EntityType;
use App\Enums\InputContentType;
use App\Enums\InputMethod;
use App\Models\ImportConfiguration;
use App\Services\Import\ImportService;
use Carbon\CarbonTimeZone;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;
use Session;
use Throwable;

class ImportController extends Controller
{
    private ImportService $importService;

    public function __construct(ImportService $importService)
    {
        $this->importService = $importService;
    }

    public function index(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('viewAny', [ImportConfiguration::class, $organization]);

        $importConfigurations = $organization->importConfigurations()
            ->orderBy('name')
            ->paginate();

        return view('imports.index', [
            'importConfigurations' => $importConfigurations,
        ]);
    }

    public function show(ImportConfiguration $importConfiguration)
    {
        $this->authorize('view', $importConfiguration);

        return view('imports.show', [
            'importConfiguration' => $importConfiguration,
        ]);
    }

    public function create(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('create', [ImportConfiguration::class, $organization]);

        $importConfiguration = new ImportConfiguration([
            'timezone' => config('app.timezone'),
            'delimiter' => ',',
            'enclosure_character' => '"',
            'escape_character' => '\\\\',
            'parse_expression' => '[.[] | {define your object here...}]',
        ]);

        $importConfiguration->fill(Session::getOldInput());

        return view('imports.create', [
            'importConfiguration' => $importConfiguration,
            'selectableTimezones' => CarbonTimeZone::listIdentifiers(),
        ]);
    }

    /**
     * @throws Throwable
     */
    public function store(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('create', [ImportConfiguration::class, $organization]);

        $data = $request->validate([
            'name' => ['required', 'max:255', 'unique:import_configurations'],
            'note' => ['nullable'],
            'input_method' => ['required', new Enum(InputMethod::class)],
            'input_content_type' => ['required', new Enum(InputContentType::class)],
            'date_format' => ['nullable', 'string'],
            'timezone' => [Rule::in(CarbonTimeZone::listIdentifiers())],
            'url' => ['exclude_unless:input_method,' . InputMethod::Download->value, 'required', 'max:255', 'url'],
            'has_header_row' => ['exclude_unless:input_content_type,' . InputContentType::CSV->value, 'required', 'boolean'],
            'delimiter' => ['exclude_unless:input_content_type,' . InputContentType::CSV->value, 'required', 'between:1,2'],
            'enclosure_character' => ['exclude_unless:input_content_type,' . InputContentType::CSV->value, 'required', 'between:1,2'],
            'escape_character' => ['exclude_unless:input_content_type,' . InputContentType::CSV->value, 'required', 'between:1,2'],
            'entity_type' => ['required', new Enum(EntityType::class)],
            'parse_expression' => ['required', 'max:4095'],
        ]);

        $importConfiguration = new ImportConfiguration($data);
        $importConfiguration->organization()->associate($organization);
        $importConfiguration->saveOrFail();

        return redirect(route('imports.show', $importConfiguration));
    }

    public function edit(ImportConfiguration $importConfiguration)
    {
        $this->authorize('update', $importConfiguration);

        $importConfiguration->fill(Session::getOldInput());

        return view('imports.edit', [
            'importConfiguration' => $importConfiguration,
            'selectableTimezones' => CarbonTimeZone::listIdentifiers(),
        ]);
    }

    /**
     * @throws Throwable
     */
    public function update(Request $request, ImportConfiguration $importConfiguration)
    {
        $this->authorize('update', $importConfiguration);

        $data = $request->validate([
            'name' => ['required', 'max:255', Rule::unique('import_configurations')->ignoreModel($importConfiguration)],
            'note' => ['nullable'],
            'input_method' => ['required', new Enum(InputMethod::class)],
            'input_content_type' => ['required', new Enum(InputContentType::class)],
            'date_format' => ['nullable', 'string'],
            'timezone' => [Rule::in(CarbonTimeZone::listIdentifiers())],
            'url' => ['exclude_unless:input_method,' . InputMethod::Download->value, 'required', 'max:255', 'url'],
            'has_header_row' => ['exclude_unless:input_content_type,' . InputContentType::CSV->value, 'required', 'boolean'],
            'delimiter' => ['exclude_unless:input_content_type,' . InputContentType::CSV->value, 'required', 'between:1,2'],
            'enclosure_character' => ['exclude_unless:input_content_type,' . InputContentType::CSV->value, 'required', 'between:1,2'],
            'escape_character' => ['exclude_unless:input_content_type,' . InputContentType::CSV->value, 'required', 'between:1,2'],
            'entity_type' => ['required', new Enum(EntityType::class)],
            'parse_expression' => ['required', 'max:4095'],
        ]);

        $importConfiguration->fill($data);
        $importConfiguration->saveOrFail();

        return redirect(route('imports.show', $importConfiguration));
    }

    public function run(Request $request, ImportConfiguration $importConfiguration)
    {
        // Note: run from API endpoint like so:
        // curl -X "POST" -H "Accept: application/json" -H "Authorization: Bearer <token>" http://localhost:8080/api/imports/<id>/run
        // Or if a file needs to be supplied:
        // curl -F 'import_file=@/path/to/file.csv' -H "Accept: application/json" -H "Authorization: Bearer <token>" http://localhost:8080/api/imports/<id>/run

        $this->authorize('run', $importConfiguration);

        $rules = [
            'import_file' => ['file', 'max:1024'], // size in KiB
        ];
        if ($importConfiguration->input_method === InputMethod::Upload) {
            $rules['import_file'][] = 'required';
        }
        $request->validate($rules);

        $execution = $this->importService->run($importConfiguration, $request->file('import_file'));

        if ($request->expectsJson()) {
            return response()->json([
                'result' => 'success',
                'data' => $execution,
                'href' => route('executions.show', $execution),
            ]);
        }

        return redirect(route('executions.show', $execution));
    }

    /**
     * @throws Exception
     */
    public function delete(ImportConfiguration $importConfiguration)
    {
        $this->authorize('delete', $importConfiguration);

        $importConfiguration->delete();

        return redirect(route('imports.index'));
    }

    public function deleted(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('viewAny', [ImportConfiguration::class, $organization]);

        $importConfigurations = $organization->importConfigurations()
            ->onlyTrashed()
            ->orderBy('deleted_at', 'desc')
            ->paginate();

        return view('imports.deleted', [
            'importConfigurations' => $importConfigurations,
        ]);
    }

    public function restore(ImportConfiguration $trashedImportConfiguration)
    {
        $this->authorize('restore', $trashedImportConfiguration);

        $trashedImportConfiguration->restore();

        return redirect(route('imports.index'));
    }
}
