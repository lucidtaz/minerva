<?php

namespace App\Http\Controllers;

use App\Models\Organization;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @throws AuthorizationException
     */
    protected function getUser(Request $request): User
    {
        /** @var User|null $user */
        $user = $request->user();
        if ($user === null) {
            throw new AuthorizationException();
        }
        return $user;
    }

    /**
     * @throws AuthorizationException
     */
    protected function getOrganization(Request $request): Organization
    {
        $user = $this->getUser($request);
        $organization = $user->getMaybeOrganization($request->session()->get('organization_id'));
        if ($organization === null) {
            throw new AuthorizationException('Organization not found or unauthorized');
        }
        return $organization;
    }
}
