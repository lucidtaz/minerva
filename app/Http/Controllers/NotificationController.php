<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;

class NotificationController extends Controller
{
    public function index(Request $request)
    {
        /** @var User $user */
        $user = $request->user();

        return view('notifications.index', [
            'notifications' => $user->notifications()->paginate(),
        ]);
    }

    public function show(DatabaseNotification $notification)
    {
        $this->authorize('view', $notification);

        $notification->markAsRead();

        return view('notifications.show', [
            'notification' => $notification,
        ]);
    }
}
