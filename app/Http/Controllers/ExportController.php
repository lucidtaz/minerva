<?php

namespace App\Http\Controllers;

use App\Models\Organization;
use App\Services\Export\ExportService;
use Illuminate\Http\Request;

class ExportController extends Controller
{
    private ExportService $exportService;

    public function __construct(ExportService $exportService)
    {
        $this->exportService = $exportService;
    }

    public function index(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('export', $organization);

        return view('exports.index');
    }

    public function create(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('export', $organization);

        return view('exports.create');
    }

    public function run(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('export', $organization);

        $file = $this->exportService->createExportArchiveFile($organization);

        return response()->download($file);
    }

    public function runApi(Organization $organization)
    {
        // curl -v -H "Authorization: Bearer <token>" http://localhost:8080/api/exports/1/run

        $this->authorize('export', $organization);

        $file = $this->exportService->createExportArchiveFile($organization);

        return response()->download($file);
    }
}
