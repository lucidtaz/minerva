<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Models\Transaction;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Session;
use Throwable;

class TransactionController extends Controller
{
    public function create(Request $request, Asset $asset)
    {
        $this->authorize('create', [Transaction::class, $asset]);

        $transaction = new Transaction([
            'moment' => Carbon::now(),
        ]);

        $transaction->fill(Session::getOldInput());

        if (Session::getOldInput('moment') !== null) {
            // The client sent the moment as a string without timezone info, so
            // we must take extra measures to interpret it in the timezone which
            // is configured in the account
            $transaction->moment = $transaction->moment->shiftTimezone($request->user()->timezone);
        }

        return view('transactions.create', [
            'asset' => $asset,
            'transaction' => $transaction,
        ]);
    }

    public function store(Request $request, Asset $asset)
    {
        $this->authorize('create', [Transaction::class, $asset]);

        $data = $request->validate([
            'amount' => ['required', 'numeric'],
            'moment' => ['required', 'date'],
            'note' => ['nullable'],
            'location' => ['nullable', 'max:255'],
            'is_temporary' => ['boolean'],
        ]);

        $transaction = new Transaction($data);
        $transaction->moment = $transaction->moment->shiftTimezone($request->user()->timezone);
        $asset->transactions()->save($transaction);

        return redirect(route('assets.show', $asset));
    }

    public function edit(Asset $asset, Transaction $transaction)
    {
        $this->authorize('update', $transaction);

        $transaction->fill(Session::getOldInput());

        return view('transactions.edit', [
            'transaction' => $transaction,
        ]);
    }

    /**
     * @throws Throwable
     */
    public function update(Request $request, Asset $asset, Transaction $transaction)
    {
        $this->authorize('update', $transaction);

        $data = $request->validate([
            'amount' => ['required', 'numeric'],
            'moment' => ['required', 'date'],
            'note' => ['nullable'],
            'location' => ['nullable', 'max:255'],
            'is_temporary' => ['boolean'],
        ]);

        $transaction->fill($data);
        $transaction->moment = $transaction->moment->shiftTimezone($request->user()->timezone);
        $transaction->saveOrFail();

        return redirect(route('assets.show', $transaction->asset));
    }

    /**
     * @throws Exception
     */
    public function delete(Asset $asset, Transaction $transaction)
    {
        $this->authorize('delete', $transaction);

        $transaction->delete();

        return redirect(route('assets.show', $transaction->asset));
    }
}
