<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Models\Price;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Session;

class PriceController extends Controller
{
    public function create(Request $request, Asset $asset)
    {
        $this->authorize('create', [Price::class, $asset]);

        $organization = $this->getOrganization($request);

        // Note we don't filter out $asset from this list. This is done in the view to make it greyed out instead of hidden entirely
        $baseAssets = $organization->assets()->currency()->get();

        /** @var Asset|null $displayAsset */
        $displayAsset = $request->user()->displayAsset;

        $price = new Price([
            'moment' => Carbon::now(),
        ]);
        $price->asset()->associate($asset);
        $price->baseAsset()->associate($displayAsset);

        $price->fill(Session::getOldInput());

        if (Session::getOldInput('moment') !== null) {
            // The client sent the moment as a string without timezone info, so
            // we must take extra measures to interpret it in the timezone which
            // is configured in the account
            $price->moment = $price->moment->shiftTimezone($request->user()->timezone);
        }

        return view('prices.create', [
            'asset' => $asset,
            'baseAssets' => $baseAssets,
            'price' => $price,
        ]);
    }

    public function store(Request $request, Asset $asset)
    {
        $this->authorize('create', [Price::class, $asset]);

        $data = $request->validate([
            'base_asset_id' => ['required', Rule::exists('assets', 'id'), Rule::notIn($asset->id)],
            'value' => ['required', 'numeric'],
            'moment' => ['required', 'date'],
        ]);

        $price = new Price($data);
        $price->moment = $price->moment->shiftTimezone($request->user()->timezone);
        $asset->prices()->save($price);

        return redirect(route('assets.show', $asset));
    }

    /**
     * @throws Exception
     */
    public function delete(Asset $asset, int $priceId)
    {
        $price = Price::findOrFail($priceId);

        $this->authorize('delete', $price);

        $price->delete();

        return redirect(route('assets.show', $asset));
    }
}
