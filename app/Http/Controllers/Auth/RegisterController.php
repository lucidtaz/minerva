<?php

namespace App\Http\Controllers\Auth;

use App\Enums\OrganizationUserRole;
use App\Http\Controllers\Controller;
use App\Models\Invitation;
use App\Models\Organization;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Config;
use DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use LogicException;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    private bool $openRegistration;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->openRegistration = Config::get('app.open_registration');
    }

    public function showRegistrationForm(Request $request)
    {
        $invitation = $this->retrieveInvitation($request);

        if ($invitation !== null && $invitation->organization !== null) {
            $organization = $invitation->organization;
            $lockOrganization = true;
        } else {
            $organization = new Organization([
                'name' => 'Default',
            ]);
            $lockOrganization = false;
        }

        return view('auth.register', [
            'open_registration' => $this->openRegistration,
            'organization' => $organization,
            'lockOrganization' => $lockOrganization,
            'invitation' => $invitation,
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'organization_name' => ['required', 'string', 'max:255'],
        ];

        if (!$this->openRegistration) {
            $rules['invitation_token'] = ['required', 'exists:invitations,token'];
        }

        return Validator::make($data, $rules);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    protected function registered(Request $request, User $user): void
    {
        $invitation = $this->retrieveInvitation($request);

        if ($invitation !== null && $invitation->organization !== null) {
            $organization = $invitation->organization;
            DB::transaction(function () use ($user, $invitation) {
                $invitation->organization->users()->attach($user, ['role' => $invitation->role ?? OrganizationUserRole::Member]);

                $invitation->delete();
            });
        } elseif ($invitation !== null && $invitation->organization === null) {
            $organization = new Organization([
                'name' => $request->get('organization_name'),
            ]);
            DB::transaction(function () use ($organization, $user, $invitation) {
                $organization->saveOrFail();
                $organization->users()->attach($user, ['role' => $invitation->role ?? OrganizationUserRole::Admin]);

                $invitation->delete();
            });
        } elseif ($this->openRegistration) {
            $organization = new Organization([
                'name' => $request->get('organization_name'),
            ]);
            DB::transaction(function () use ($organization, $user) {
                $organization->saveOrFail();
                $organization->users()->attach($user, ['role' => OrganizationUserRole::Admin]);
            });
        } else {
            // Should not happen because of input validation
            throw new LogicException('Registration is closed and user registered without an invitation...');
        }

        $request->session()->put('organization_id', $organization->id);
    }

    private function retrieveInvitation(Request $request): ?Invitation
    {
        $invitationToken = $request->get('invitation_token');
        if ($invitationToken === null) {
            return null;
        }

        /** @var Invitation $invitation */
        $invitation = Invitation::query()
            ->where('token', '=', $invitationToken)
            ->first();

        return $invitation;
    }
}
