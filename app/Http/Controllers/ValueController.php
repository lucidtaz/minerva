<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Models\Price;
use App\Models\Transaction;
use App\Services\AssetPriceService;
use App\Services\AssetValueService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Session;

/**
 * A convenient way to store a price and transaction in one go
 */
class ValueController extends Controller
{
    public function __construct(private readonly AssetPriceService $assetPriceService, private readonly AssetValueService $assetValueService)
    {
        //
    }

    public function create(Request $request, Asset $asset)
    {
        $this->authorize('create', [Transaction::class, $asset]);
        $this->authorize('create', [Price::class, $asset]);

        $organization = $this->getOrganization($request);

        // Note we don't filter out $asset from this list. This is done in the view to make it greyed out instead of hidden entirely
        $baseAssets = $organization->assets()->currency()->get();

        /** @var Asset|null $displayAsset */
        $displayAsset = $request->user()->displayAsset;

        $price = new Price([
            'moment' => Carbon::now(),
        ]);
        $price->asset()->associate($asset);
        $price->baseAsset()->associate($displayAsset);

        $transaction = new Transaction([
            'moment' => Carbon::now(),
        ]);

        $transaction->fill(Session::getOldInput());

        if (Session::getOldInput('moment') !== null) {
            // The client sent the moment as a string without timezone info, so
            // we must take extra measures to interpret it in the timezone which
            // is configured in the account
            $price->moment = $price->moment->shiftTimezone($request->user()->timezone);
            $transaction->moment = $transaction->moment->shiftTimezone($request->user()->timezone);
        }

        return view('values.create', [
            'asset' => $asset,
            'baseAssets' => $baseAssets,
            'price' => $price,
            'transaction' => $transaction,
        ]);
    }

    public function store(Request $request, Asset $asset)
    {
        $this->authorize('create', [Transaction::class, $asset]);
        $this->authorize('create', [Price::class, $asset]);

        $organization = $this->getOrganization($request);

        $data = $request->validate([
            'value' => ['required', 'numeric'],
            'base_asset_id' => ['required', Rule::exists('assets', 'id'), Rule::notIn($asset->id)],
            'moment' => ['required', 'date'],
            'note' => ['nullable'],
        ]);

        /** @var Asset $baseAsset */
        $baseAsset = $organization->assets()->where('id', '=', $data['base_asset_id'])->firstOrFail();
        $moment = Carbon::make($data['moment'])->shiftTimezone($request->user()->timezone);
        $currentPrice = $this->assetPriceService->calculatePrice($asset, $baseAsset, $moment);
        $currentValue = $this->assetValueService->calculateValue($asset, $baseAsset, $moment);
        $deltaValue = bcsub($data['value'], $currentValue->value);

        if (bccomp($currentPrice->value, '1') !== 0) {
            $price = new Price([
                'base_asset_id' => $data['base_asset_id'],
                'moment' => $data['moment'],
                'value' => '1',
            ]);
            $price->moment = $price->moment->shiftTimezone($request->user()->timezone);
            $asset->prices()->save($price);
        }

        $transaction = new Transaction([
            'amount' => $deltaValue,
            'moment' => $data['moment'],
            'note' => $data['note'],
        ]);
        $transaction->moment = $transaction->moment->shiftTimezone($request->user()->timezone);
        $asset->transactions()->save($transaction);

        // TODO: Recognize if there needs to be a correction *after* so that the previously valid value at the *next* moment is consistent again.

        return redirect(route('assets.show', $asset));
    }
}
