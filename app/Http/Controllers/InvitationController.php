<?php

namespace App\Http\Controllers;

use App\Enums\OrganizationUserRole;
use App\Models\Invitation;
use App\Scopes\ExpiringScope;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;
use Ramsey\Uuid\Uuid;

class InvitationController extends Controller
{
    public function index(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('viewAny', [Invitation::class, $organization]);

        $organization = $this->getOrganization($request);

        $invitations = $organization->invitations()
            ->withoutGlobalScope(ExpiringScope::class)
            ->orderBy('created_at')
            ->paginate();

        return view('invitations.index', ['invitations' => $invitations]);
    }

    public function create(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('create', [Invitation::class, $organization]);

        $invitation = new Invitation([
            'role' => OrganizationUserRole::Member,
        ]);
        $roles = OrganizationUserRole::cases();

        return view('invitations.create', [
            'invitation' => $invitation,
            'roles' => $roles,
        ]);
    }

    public function store(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('create', [Invitation::class, $organization]);

        $data = $request->validate([
            'note' => ['required'],
            'role' => ['required', new Enum(OrganizationUserRole::class)]
        ]);

        $invitation = new Invitation($data);
        $invitation->token = (string) Uuid::uuid4();
        $invitation->expires_at = CarbonImmutable::now()->addDays(config('app.invitation_expire_days'));

        $invitation->organization()->associate($organization);
        $invitation->save();

        $request->session()->flash('created_invitation_token_url', route('register') . '?invitation_token=' . $invitation->token);

        return redirect(route('invitations.index'));
    }

    public function delete(Invitation $invitation)
    {
        $this->authorize('delete', $invitation);

        $invitation->delete();

        return redirect(route('invitations.index'));
    }
}
