<?php

namespace App\Http\Controllers;

use App\Dtos\DataSet;
use App\Dtos\NumberAtMoment;
use App\Enums\AssetMode;
use App\Http\Controllers\Display\RemembersDisplaySettings;
use App\Models\Asset;
use App\Models\AssetAlias;
use App\Models\Price;
use App\Models\User;
use App\Services\AssetPriceService;
use App\Services\AssetValueService;
use App\Services\Graph\EmptyBinMode;
use App\Services\Graph\GraphPlotter;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Ramsey\Uuid\Uuid;
use Session;
use Throwable;

class AssetController extends Controller
{
    use RemembersDisplaySettings;

    private AssetPriceService $assetPriceService;
    private AssetValueService $assetValueService;
    private GraphPlotter $graphPlotter;

    public function __construct(AssetPriceService $assetPriceService, AssetValueService $assetValueService, GraphPlotter $graphPlotter)
    {
        $this->assetPriceService = $assetPriceService;
        $this->assetValueService = $assetValueService;
        $this->graphPlotter = $graphPlotter;
    }

    public function index(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('viewAny', [Asset::class, $organization]);

        $organization = $this->getOrganization($request);

        $assets = $organization->assets()
            ->with('prices.baseAsset')
            ->withCount('transactions')
            ->orderBy('name')
            ->get();

        /** @var Asset|null $displayAsset */
        $displayAsset = $request->user()->displayAsset;

        $now = Carbon::now();
        $prices = $assets->mapWithKeys(function (Asset $asset) use ($displayAsset, $now) {
            return [
                $asset->id => $this->assetPriceService->calculatePrice($asset, $displayAsset, $now),
            ];
        });

        return view('assets.index', [
            'assets' => $assets,
            'prices' => $prices,
        ]);
    }

    public function show(Request $request, Asset $asset)
    {
        $this->authorize('view', $asset);

        $asset->load([
            'prices.baseAsset.prices',
            'prices.asset',
        ]);
        $asset->loadCount(['transactions']);

        /** @var Asset $displayAsset */
        $displayAsset = $request->user()->displayAsset ?? $asset; // Falling back to $asset to avoid crashes.

        $now = Carbon::now();
        $amount = $this->assetValueService->calculateAmount($asset, $now);
        $price = $this->assetPriceService->calculatePrice($asset, $displayAsset, $now);
        $value = $this->assetValueService->calculateValue($asset, $displayAsset, $now);

        $transactions = $asset->transactions()->orderByDesc('moment')->paginate(null, ['*'], 'transactions_page')->withQueryString();
        $paginatedPrices = $asset->prices()->orderByDesc('moment')->paginate(null, ['*'], 'prices_page')->withQueryString();
        $allPrices = $asset->prices;

        $paginatedPrices->getCollection()->load('baseAsset');

        // We unfortunately need to convert two price arrays: one for ALL prices
        // (graph) and one for a single page of prices (list)
        // Doing it differently means we need to add more logic on the querying
        // and paginator side.
        $this->assetPriceService->convertBase($allPrices, $displayAsset);
        $this->assetPriceService->convertBase($paginatedPrices->getCollection(), $displayAsset);

        $graphControls = $this->getGraphControls($request->session(), $request->user()->timezone);

        $priceHistory = $allPrices
            ->sortBy('moment')
            ->values()
            ->map(fn (Price $price) => new NumberAtMoment(
                $price->rebasedValue,
                $price->moment->toImmutable(),
            ));
        $binnedPriceHistory = $this->graphPlotter->averageIntoDateBins($priceHistory, $graphControls, EmptyBinMode::Skip);

        $priceDataSet = new DataSet(
            $asset->name,
            $binnedPriceHistory,
        );

        $valueHistory = $this->assetValueService->calculateValueHistory($asset, $displayAsset);
        $binnedValueHistory = $this->graphPlotter->averageIntoDateBins($valueHistory, $graphControls, EmptyBinMode::Skip);

        $valueDataSet = new DataSet(
            $asset->name,
            $binnedValueHistory,
        );

        $investmentHistory = $this->assetValueService->calculateInvestmentHistory($asset, $displayAsset);
        $binnedInvestmentHistory = $this->graphPlotter->averageIntoDateBins($investmentHistory, $graphControls, EmptyBinMode::Skip);

        $investmentDataSet = new DataSet(
            $asset->name . ' Invested',
            $binnedInvestmentHistory,
        );

        return view('assets.show', [
            'asset' => $asset,
            'amount' => $amount,
            'price' => $price,
            'value' => $value,
            'transactions' => $transactions,
            'prices' => $paginatedPrices,
            'priceDataSets' => collect([$priceDataSet]),
            'valueDataSets' => collect([$valueDataSet, $investmentDataSet]),
        ]);
    }

    public function create(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('create', [Asset::class, $organization]);

        $asset = new Asset();
        $asset->mode = AssetMode::Normal;

        $asset->fill(Session::getOldInput());

        return view('assets.create', [
            'asset' => $asset,
        ]);
    }

    /**
     * @throws Throwable
     */
    public function store(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('create', [Asset::class, $organization]);

        $data = $request->validate([
            'name' => ['required', 'max:255', function ($attribute, $value, $fail) use ($organization) {
                if ($organization->assets()->where('name', '=', $value)->exists()) {
                    $fail(trans('validation.unique'));
                }
            }],
            'currency_symbol' => ['max:10'],
            'mode' => [Rule::enum(AssetMode::class)],
        ]);

        $currencyCountBefore = Asset::currency()->count();

        $asset = new Asset($data);
        $asset->uuid = Uuid::uuid4()->toString();
        $asset->import_id = $asset->uuid;
        $asset->organization()->associate($organization);
        $asset->saveOrFail();

        $nameAlias = new AssetAlias([
            'alias' => $asset->name,
        ]);
        $uuidAlias = new AssetAlias([
            'alias' => $asset->uuid,
        ]);
        $nameAlias->organization()->associate($asset->organization);
        $uuidAlias->organization()->associate($asset->organization);
        $asset->aliases()->saveMany([$nameAlias, $uuidAlias]);

        $currencyCountAfter = Asset::currency()->count();

        if ($currencyCountBefore === 0 && $currencyCountAfter !== 0) {
            // The first currency was added. Set it as the user's display currency for convenience
            /** @var User $user */
            $user = $request->user();
            $user->displayAsset()->associate($asset);
            $user->save();
        }

        return redirect(route('assets.show', $asset));
    }

    public function edit(Asset $asset)
    {
        $this->authorize('update', $asset);

        $asset->fill(Session::getOldInput());

        return view('assets.edit', [
            'asset' => $asset,
        ]);
    }

    /**
     * @throws Throwable
     */
    public function update(Request $request, Asset $asset)
    {
        $this->authorize('update', $asset);

        $data = $request->validate([
            'name' => ['required', 'max:255', Rule::unique('assets')->ignoreModel($asset)],
            'currency_symbol' => ['max:10'],
            'mode' => [Rule::enum(AssetMode::class)],
            'aliases' => ['array'],
            'aliases.*' => ['max:64'],
            'new_aliases' => ['array'],
            'new_aliases.*' => ['max:64'],
        ]);

        $asset->fill($data);
        $asset->saveOrFail();

        $aliasErrors = [];

        foreach ($data['aliases'] ?? [] as $updateAliasId => $updateAliasValue) {
            /** @var ?Asset $alias */
            $alias = $asset->aliases->first(fn (AssetAlias $a) => $a->id === $updateAliasId);

            if (empty($updateAliasValue)) {
                $alias?->delete();
            } else {
                if ($alias === null) {
                    continue;
                }

                if ($alias->alias === $updateAliasValue) {
                    continue;
                }

                $valueAlreadyExists = $asset->organization->assetAliases()
                    ->where('alias', '=', $updateAliasValue)
                    ->exists();
                if ($valueAlreadyExists) {
                    $aliasErrors[] = 'Cannot update alias to ' . $updateAliasValue . ', value already exists.';
                    continue;
                }

                $alias->alias = $updateAliasValue;
                $alias->save();
            }
        }

        foreach ($data['new_aliases'] as $newAliasValue) {
            if (empty($newAliasValue)) {
                continue;
            }

            $valueAlreadyExists = $asset->organization->assetAliases()
                ->where('alias', '=', $newAliasValue)
                ->exists();
            if ($valueAlreadyExists) {
                $aliasErrors[] = 'Cannot add alias ' . $newAliasValue . ', value already exists.';
                continue;
            }

            $alias = new AssetAlias([
                'alias' => $newAliasValue,
            ]);
            $alias->organization()->associate($asset->organization);
            $asset->aliases()->save($alias);
        }

        $asset->load('aliases');

        $assetNameAsAliasAlreadyExists = $asset->organization
            ->assetAliases()
            ->where('alias', '=', $asset->name)
            ->exists();
        if (!$assetNameAsAliasAlreadyExists) {
            $alias = new AssetAlias([
                'alias' => $asset->name,
            ]);
            $alias->organization()->associate($asset->organization);
            $asset->aliases()->save($alias);
        }

        if (!empty($aliasErrors)) {
            $request->session()->flash('aliasErrors', $aliasErrors);
            return redirect()->back();
        }

        return redirect(route('assets.show', $asset));
    }

    /**
     * @throws Exception
     */
    public function delete(Asset $asset)
    {
        $this->authorize('delete', $asset);

        $asset->delete();

        return redirect(route('assets.index'));
    }

    public function deleted(Request $request)
    {
        $organization = $this->getOrganization($request);

        $this->authorize('viewAny', [Asset::class, $organization]);

        $organization = $this->getOrganization($request);

        $assets = $organization->assets()
            ->onlyTrashed()
            ->withCount('transactions')
            ->orderBy('deleted_at', 'desc')
            ->paginate();

        return view('assets.deleted', [
            'assets' => $assets,
        ]);
    }

    public function restore(Asset $trashedAsset)
    {
        $this->authorize('restore', $trashedAsset);

        $trashedAsset->restore();

        return redirect(route('assets.index'));
    }

    public function forceDelete(Asset $trashedAsset)
    {
        $this->authorize('forceDelete', $trashedAsset);

        DB::transaction(function () use ($trashedAsset) {
            foreach ($trashedAsset->allocations as $allocation) {
                $allocation->assets()->detach($trashedAsset);
            }
            $trashedAsset->forceDelete();
        });

        return redirect(route('assets.index'));
    }
}
