<?php

namespace App\Http\Controllers\Display;

use App\Http\Controllers\Controller;
use App\Services\Graph\Resolution;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\Enum;

class DisplayController extends Controller
{
    use RemembersDisplaySettings;

    public function setGraphControls(Request $request): RedirectResponse
    {
        $data = $request->validate([
            'resolution' => new Enum(Resolution::class),
        ]);

        $graphControls = $this->getGraphControls($request->session(), $request->user()->timezone);

        if (array_key_exists('resolution', $data)) {
            $graphControls->setResolution(Resolution::from($data['resolution']));
        }

        $this->storeGraphControls($graphControls, $request->session());

        return redirect()->back();
    }
}
