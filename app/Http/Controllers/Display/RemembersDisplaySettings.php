<?php

namespace App\Http\Controllers\Display;

use App\Presentation\NumberDisplayOptions;
use App\Services\Graph\GraphControls;
use App\Services\Graph\Resolution;
use Carbon\CarbonImmutable;
use Carbon\CarbonTimeZone;
use Illuminate\Contracts\Session\Session;
use JetBrains\PhpStorm\Pure;

trait RemembersDisplaySettings
{
    private function storeGraphControls(GraphControls $graphControls, Session $session): void
    {
        $session->put('graphControls', $graphControls);
    }

    private function getGraphControls(Session $session, CarbonTimeZone $timezone): GraphControls
    {
        if ($session->has('graphControls')) {
            return $session->get('graphControls');
        }
        $graphControls = new GraphControls(CarbonImmutable::minValue(), CarbonImmutable::maxValue(), Resolution::Day);
        $graphControls->setTimezone($timezone);
        return $graphControls;
    }

    #[Pure]
    private function getNumberDisplayOptions(): NumberDisplayOptions
    {
        return new NumberDisplayOptions(2, 2, 2, 0);
    }
}
