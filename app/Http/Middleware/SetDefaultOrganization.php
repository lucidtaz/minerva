<?php

namespace App\Http\Middleware;

use App\Models\Organization;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use RuntimeException;

class SetDefaultOrganization
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->session()->has('organization_id')) {
            return $next($request);
        }

        /** @var User|null $user */
        $user = $request->user();

        if ($user === null) {
            return $next($request);
        }

        /** @var Organization|null $organization */
        $organization = $user->organizations->first(); // Take first, we may want to specify a "default" later per user

        if ($organization === null) {
            throw new RuntimeException('User has no Organization');
        }

        $request->session()->put('organization_id', $organization->id);

        return $next($request);
    }
}
