<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Config;
use Illuminate\Http\Request;
use Log;
use View;

class ShareViewVariables
{
    public function handle(Request $request, Closure $next)
    {
        View::share('openRegistration', Config::get('app.open_registration'));

        /** @var User|null $user */
        $user = $request->user();

        if ($user === null) {
            View::share('timezone', 'UTC');
            return $next($request);
        }

        View::share('timezone', $user->timezone);

        $organizationId = $request->session()->get('organization_id');

        // Take organization via User relation so authorization is automatically checked
        $organization = $user->getMaybeOrganization($organizationId);

        if ($organization === null) {
            // Could happen when a User's access to an Organization is revoked
            Log::warning('Nonexistent or unauthorized Organization found in session');
            return $next($request);
        }

        $assets = $organization->assets()
            ->orderBy('name')
            ->get();
        View::share('allAssets', $assets);

        $allocations = $organization->allocations()
            ->orderBy('name')
            ->get();
        View::share('allAllocations', $allocations);

        $importConfigurations = $organization->importConfigurations()
            ->orderBy('name')
            ->get();
        View::share('allImportConfigurations', $importConfigurations);

        return $next($request);
    }
}
