require('./bootstrap');

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

const clipboard = new ClipboardJS('.clipboard_copy_link');

clipboard.on('success', function(e) {
    $(e.trigger).attr('data-original-title', 'Copied!').tooltip('show');
    e.clearSelection();
});
