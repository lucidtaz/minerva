<?php

use App\Models\Invitation;
use App\Models\Organization;

/** @var bool $open_registration */
/** @var Organization $organization */
/** @var bool $lockOrganization */
/** @var Invitation|null $invitation */

?>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if(!$open_registration && $invitation === null)
                <div class="alert alert-danger">
                    You need a valid invitation to register.
                </div>
            @endif

            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        @if($invitation !== null)
                            <input type="hidden" name="invitation_token" value="{{ $invitation->token }}" />
                        @endif

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="organization-name" class="col-md-4 col-form-label text-md-right">Organization name</label>

                            <div class="col-md-6">
                                <input id="organization-name" type="text" class="form-control @error('organization_name') is-invalid @enderror" name="organization_name" value="{{ $organization->name }}" @if($lockOrganization) disabled="disabled" @endif autocomplete="organization">
                                @if($lockOrganization) <input type="hidden" name="organization_name" value="dummy" /> @endif @php /* Ensure there is an organization_name even when it's not needed; we're bound by a bit of strange architecture on the validation side, so we cannot easily make the organization_name optional based on what kind of token you supply. To properly fix this we should probably just make two signup forms, one for each type of invitation. */ @endphp

                                @if($invitation !== null && $invitation->role !== null)
                                    <small id="invitationOrganizationHelp" class="form-text text-muted">You have been invited into this organization with the role {{ $invitation->role->value }}</small>
                                @endif

                                @error('organization_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
