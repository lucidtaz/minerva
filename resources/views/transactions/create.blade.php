<?php

use App\Models\Asset;
use App\Models\Transaction;

/** @var Asset $asset */
/** @var Transaction $transaction */

?>
@extends('layouts.app')

@section('content')
    <h1>Add transaction for '{{ $asset->name }}'</h1>

    <form class="needs-validation" action="{!! route('transactions.store', $asset) !!}" method="post" novalidate>
        @csrf

        @include('transactions._form', ['transaction' => $transaction])

        <button type="submit" class="btn btn-primary">Add transaction</button>
    </form>
@endsection
