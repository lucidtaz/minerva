<?php

use App\Models\Transaction;

/** @var Transaction $transaction */

?>
<div class="modal" id="deleteTransactionModal{{ $transaction->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteTransactionModalLabel{{ $transaction->id }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteTransactionModalLabel{{ $transaction->id }}">Delete transaction</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this transaction? You cannot undo this.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                <form action="{{ route('transactions.delete', [$transaction->asset, $transaction]) }}" method="post">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>
