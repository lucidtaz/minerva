<?php

use App\Models\Transaction;

/** @var Transaction $transaction */

?>
@extends('layouts.app')

@section('content')
    <h1>Edit transaction for '{{ $transaction->asset->name }}'</h1>

    <form class="needs-validation" action="{!! route('transactions.update', [$transaction->asset, $transaction]) !!}" method="post" novalidate>
        @method('put')
        @csrf

        @include('transactions._form', ['transaction' => $transaction])

        <button type="submit" class="btn btn-primary">Update transaction</button>
    </form>
@endsection
