<?php

use App\Models\Transaction;

/** @var Transaction $transaction */

?>
<div class="form-group">
    <label for="amount">Amount</label>
    <input type="number" class="form-control @if ($errors->has('amount')) is-invalid @endif" name="amount" id="amount" value="{{ $transaction->amount }}" required />
    @error('amount')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="note">Note (optional)</label>
    <textarea class="form-control @if ($errors->has('note')) is-invalid @endif" rows="5" name="note" id="note">{{ $transaction->note }}</textarea>
    @error('note')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="location">Location (optional)</label>
    <input type="text" class="form-control @if ($errors->has('location')) is-invalid @endif" name="location" id="location" value="{{ $transaction->location }}" />
    <small id="locationHelp" class="form-text text-muted">For example a bank account number, an exchange, a wallet name, ...</small>
    @error('location')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="moment">Moment</label>
    <input type="text" class="form-control @if ($errors->has('moment')) is-invalid @endif" name="moment" id="moment" value="{{ $transaction->moment !== null ? $transaction->moment->setTimeZone($timezone)->format('Y-m-d H:i:s') : '' }}" required />
    @error('moment')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group form-check">
    <input type="hidden" name="is_temporary" value="0">
    <input type="checkbox" class="form-check-input @if ($errors->has('is_temporary')) is-invalid @endif" name="is_temporary" id="is_temporary" value="1" {{ $transaction->is_temporary ? 'checked="checked"' : '' }} required />
    <label class="form-check-label" for="is_temporary">Temporary</label>
    <small id="isTemporaryHelp" class="form-text text-muted">Mark this transaction as temporary if you intend to remove it again, for example to replace it with an automatically imported transaction. Temporary transactions show a warning when older than {{ config('app.temporary_transactions_expire_days') }} days.</small>
    @error('is_temporary')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>
