<?php

use App\Models\Asset;
use App\Models\User;

/** @var User $user */
/** @var Asset[] $baseAssets */
/** @var string[] $selectableTimezones */

?>
@extends('layouts.app')

@section('content')
    <h1>Preferences</h1>

    @if(Session::has('redirect_reason'))
        <div class="alert alert-warning">
            {{ Session::get('redirect_reason') }}
        </div>
    @endif

    @if(Session::get('update_successful') === true)
        <div class="alert alert-success">
            Your preferences have been updated
        </div>
    @endif

    <form class="needs-validation" action="{!! route('preferences.update') !!}" method="post" novalidate>
        @method('put')
        @csrf

        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" id="name" value="{{ $user->name }}" required autocomplete="name" />
            @error('name')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label for="email">E-mail address</label>
            <input type="email" class="form-control @if ($errors->has('email')) is-invalid @endif" name="email" id="email" value="{{ $user->email }}" required autocomplete="email" />
            @error('email')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label for="display_asset_id">Display currency</label>
            <select class="form-control @if ($errors->has('display_asset_id')) is-invalid @endif" name="display_asset_id" id="display_asset_id" required>
                <option value="-1">Please select...</option>
                @foreach($baseAssets as $baseAsset)
                    <option value="{{ $baseAsset->id }}" @if($user->display_asset_id === $baseAsset->id) selected="selected" @endif>{{ $baseAsset->currency_symbol }} - {{ $baseAsset->name }}</option>
                @endforeach
            </select>
            <small id="displayAssetHelp" class="form-text text-muted">Any assets with a currency symbol defined show up in this list</small>
            @error('display_asset_id')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label for="timezone">Timezone</label>
            <select class="form-control @if ($errors->has('timezone')) is-invalid @endif" name="timezone" id="timezone" required>
                <option value="-1">Please select...</option>
                @foreach($selectableTimezones as $selectableTimezone)
                    <option value="{{ $selectableTimezone }}" @if($user->timezone->getName() === $selectableTimezone) selected="selected" @endif>{{ $selectableTimezone }}</option>
                @endforeach
            </select>
            <small id="timezoneHelp" class="form-text text-muted">This value affects data display but also data input, make sure it's set correctly or inserted data will have the wrong time</small>
            @error('timezone')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <button type="submit" class="btn btn-primary">Update</button>
    </form>

    <h2 class="mt-3">API Access</h2>

    <form class="needs-validation" action="{!! route('token.update') !!}" method="post" novalidate>
        @method('put')
        @csrf

        @if(Session::has('api_token'))
            <div class="alert alert-success" role="alert">
                <p>
                    Your new API Token is: <span id="newApiToken" class="text-monospace">{{ Session::get('api_token') }}</span>

                    <button type="button" class="clipboard_copy_link btn btn-sm btn-link"  data-clipboard-target="#newApiToken" data-toggle="tooltip" data-placement="top" title="Copy to clipboard">
                        <i class="fa fa-copy" aria-hidden="true"></i>
                    </button>
                </p>

                <p>
                    Please save it. It will not be shown again.
                </p>

                <p>Try it out:</p>

                <p>
                    <span id="exampleCurl" class="text-monospace">
                        curl -H "Accept: application/json" -H "Authorization: Bearer {{ Session::get('api_token') }}" {{ route('api.users.me') }}
                    </span>

                    <button type="button" class="clipboard_copy_link btn btn-sm btn-link"  data-clipboard-target="#exampleCurl" data-toggle="tooltip" data-placement="top" title="Copy to clipboard">
                        <i class="fa fa-copy" aria-hidden="true"></i>
                    </button>
                </p>
            </div>
        @endif

        <div class="form-group">
            <label for="api_token">API Token</label>
            <input type="text" class="form-control" name="api_token" id="api_token" value="{{ $user->api_token !== null ? '********' : 'Not set' }}" disabled />
        </div>

        <button type="submit" class="btn btn-primary">Generate new API token</button>
    </form>
@endsection
