<?php

use App\Models\User;

/** @var User $user */

?>
@extends('layouts.admin')

@section('content')
    <h1>{{ $user->name }}</h1>

    <div class="card-deck">
        <div class="card">
            <div class="card-header">Contact details</div>

            <div class="card-body">
                Name: {{ $user->name }}<br />
                E-mail: {{ $user->email }}
            </div>
        </div>

        <div class="card">
            <div class="card-header">Organizations</div>

            <div class="card-body">
                <ul>
                    @forelse($user->organizations as $organization)
                        <li>{{ $organization->name }} ({{ $organization->pivot->role->value }})</li>
                    @empty
                        <li>None</li>
                    @endforelse
                </ul>
            </div>
        </div>
    </div>

    <div class="card-deck mt-3">
        <div class="card">
            <div class="card-header">Account</div>

            <div class="card-body">
                Type: @if($user->is_admin) Admin @else Non-admin @endif<br />
                Created: <span title="{{ $user->created_at->toDateTimeString() }}">{{ $user->created_at->ago() }}</span><br />
                Updated: <span title="{{ $user->updated_at->toDateTimeString() }}">{{ $user->updated_at->ago() }}</span><br />

                @if($user->is_admin)
                    <form class="mt-2" action="{{ route('admin.users.demote', $user) }}" method="post">
                        @csrf

                        <button type="submit" class="btn btn-danger">Demote to non-admin</button>
                    </form>
                @else
                    <form class="mt-2" action="{{ route('admin.users.promote', $user) }}" method="post">
                        @csrf

                        <button type="submit" class="btn btn-primary">Promote to admin</button>
                    </form>
                @endif
            </div>
        </div>

        <div class="card">
            <div class="card-header">API</div>

            <div class="card-body">
                Authentication token: @if($user->api_token !== null) present @else absent @endif
            </div>
        </div>
    </div>
@endsection
