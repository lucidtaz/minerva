<?php

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/** @var LengthAwarePaginator&\App\Models\User[] $users */

?>
@extends('layouts.admin')

@section('content')
    <h1>Users</h1>

    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>Name</th>
                <th>E-mail</th>
                <th>Type</th>
                <th>Registered</th>
                <th>Organizations</th>
            </tr>

            @forelse($users as $user)
                <tr>
                    <td>
                        <a href="{!! route('admin.users.show', $user) !!}">{{ $user->name }}</a>
                    </td>
                    <td>
                        {{ $user->email }}
                    </td>
                    <td>
                        {{ $user->is_admin ? 'Admin' : 'Non-admin' }}
                    </td>
                    <td>
                        <span title="{{ $user->created_at->toDateTimeString() }}">{{ $user->created_at->ago() }}</span>
                    </td>
                    <td>
                        {{ $user->organizations_count }}
                    </td>
                </tr>
            @empty
                <tr><td colspan="999">There are no users.</td></tr>
            @endforelse
        </table>

        {!! $users->links() !!}
    </div>
@endsection
