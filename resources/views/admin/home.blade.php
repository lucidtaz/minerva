@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-deck">
                <div class="card">
                    <div class="card-header">Admin Dashboard</div>

                    <div class="card-body">
                        Welcome to the admin dashboard!
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">Scheduled Checks</div>

                    <div class="card-body">
                        <a href="{{ route('admin.scheduled-checks.index') }}">View past executions</a>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">User Management</div>

                    <div class="card-body">
                        <a href="{{ route('admin.users.index') }}">View all users</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
