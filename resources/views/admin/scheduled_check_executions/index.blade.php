<?php

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/** @var LengthAwarePaginator&\App\Models\ScheduledCheckExecution[] $executions */

?>
@extends('layouts.admin')

@section('content')
    <h1>Scheduled Check Executions</h1>

    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>Command</th>
                <th>Created</th>
                <th>Time from</th>
                <th>Time to</th>
                <th>Finished?</th>
                <th>Outcome</th>
            </tr>

            @forelse($executions as $execution)
                <tr>
                    <td>
                        {{ $execution->command }}
                    </td>
                    <td>
                        <span title="{{ $execution->created_at->toDateTimeString() }}">{{ $execution->created_at->ago() }}</span>
                    </td>
                    <td>
                        <span title="{{ $execution->time_from->toDateTimeString() }}">{{ $execution->time_from->format('H:i:s') }}</span>
                    </td>
                    <td>
                        <span title="{{ $execution->time_to->toDateTimeString() }}">{{ $execution->time_to->format('H:i:s') }}</span>
                    </td>
                    <td>
                        @if($execution->finished) Yes @else No @endif
                    </td>
                    <td>
                        {{ $execution->outcome }}
                    </td>
                </tr>
            @empty
                <tr><td colspan="999">There are no executions yet. Wait for the schedule to run at least once.</td></tr>
            @endforelse
        </table>

        {!! $executions->links() !!}
    </div>
@endsection
