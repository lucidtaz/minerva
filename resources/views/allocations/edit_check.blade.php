<?php

use App\Models\Allocation;
use App\Models\Asset;
use App\Models\Webhook;

/** @var Allocation $allocation */
/** @var string[] $timezones */
/** @var Asset[] $currencies */
/** @var Webhook[] $webhooks */

?>
@extends('layouts.app')

@section('content')
    <h1>'{{ $allocation->getOriginal('name') }}' - Configure rebalancing checks</h1>

    <a href="{!! route('allocations.edit', $allocation) !!}">
        <i class="fa fa-edit"></i>
        Edit
    </a>

    <a href="{!! route('allocations.check.edit', $allocation) !!}">
        <i class="fa fa-flag"></i>
        Rebalancing checks
    </a>

    @if(Session::has('delete_message'))
        <div class="alert alert-success">
            {{ Session::get('delete_message') }}
        </div>
    @endif

    <form class="needs-validation" action="{!! route('allocations.check.update', $allocation) !!}" method="post" novalidate>
        @method('put')
        @csrf

        <div class="form-group">
            <label for="check_at">Check at</label>
            <input type="text" class="form-control @if ($errors->has('check_at')) is-invalid @endif" name="check_at" id="check_at" value="{{ $allocation->check_at }}" required />
            @error('check_at')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
            <small id="checkAtHelp" class="form-text text-muted">Format: HH:MM:SS. If this value is used, a daily check will be performed to see if the allocation is out of balance. To avoid confusion, avoid a time at which daylight saving time changes can happen.</small>
        </div>

        <div class="form-group">
            <label for="check_timezone">Timezone</label>
            <select class="form-control @if ($errors->has('check_timezone')) is-invalid @endif" name="check_timezone" id="check_timezone" required>
                <option value="-1">Please select...</option>
                @foreach($timezones as $timezone)
                    <option value="{{ $timezone }}" @if($allocation->check_timezone === $timezone) selected="selected" @endif>{{ $timezone }}</option>
                @endforeach
            </select>
            @error('check_timezone')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label for="check_currency_id">Currency</label>
            <select class="form-control @if ($errors->has('check_currency_id')) is-invalid @endif" name="check_currency_id" id="check_currency_id" required>
                <option value="-1">Please select...</option>
                @foreach($currencies as $currency)
                    <option value="{{ $currency->id }}" @if($allocation?->checkCurrency?->id === $currency->id) selected="selected" @endif>{{ $currency->currency_symbol }} - {{ $currency->name }}</option>
                @endforeach
            </select>
            @error('check_currency_id')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label for="check_webhook_id">Webhook</label>
            <select class="form-control @if ($errors->has('check_webhook_id')) is-invalid @endif" name="check_webhook_id" id="check_webhook_id" required>
                <option value="-1">Please select...</option>
                @foreach($webhooks as $webhook)
                    <option value="{{ $webhook->id }}" @if($allocation?->checkWebhook?->id === $webhook->id) selected="selected" @endif>{{ $webhook->name }} - {{ $webhook->uri }}</option>
                @endforeach
            </select>
            @error('check_webhook_id')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
            <small id="checkWebhookHelp" class="form-text text-muted">Webhooks can be managed <a href="{{ route('webhooks.index') }}">here</a>.</small>
        </div>

        <button type="submit" class="btn btn-primary">Update</button>
    </form>

    <form action="{!! route('allocations.check.delete', $allocation) !!}" method="post">
        @method('delete')
        @csrf
        <button type="submit" class="btn btn-danger">Clear</button>
    </form>

@endsection
