<?php

use App\Models\Allocation;

/** @var Allocation $allocation */

?>
<div class="modal" id="deleteAllocationModal{{ $allocation->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteAllocationModalLabel{{ $allocation->id }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteAllocationModalLabel{{ $allocation->id }}">Delete allocation '{{ $allocation->name }}'</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this allocation? You can still undo it later.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                <form action="{{ route('allocations.delete', $allocation) }}" method="post">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>
