<?php

use App\Dtos\DataSet;
use App\Models\Allocation;
use Illuminate\Support\Enumerable;

/** @var Allocation $allocation */
/** @var Enumerable&DataSet[] $dataSets */

?>
@extends('layouts.app')

@section('content')
    <h1>{{ $allocation->name }}</h1>

    <h2>Value history</h2>

    <x-chart :dataSets="$dataSets" :stacked="true" />
@endsection
