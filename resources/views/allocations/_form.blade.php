<?php

use App\Models\Allocation;

/** @var Allocation $allocation */

?>
<div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" id="name" value="{{ $allocation->name }}" required />
    @error('name')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="tolerance">Tolerance (%)</label>
    <input type="number" class="form-control @if ($errors->has('tolerance')) is-invalid @endif" name="tolerance" id="tolerance" min="0" max="100" value="{{ $allocation->tolerance }}" aria-describedby="toleranceHelp" required />
    @error('tolerance')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
    <small id="toleranceHelp" class="form-text text-muted">The highest relative amount that the combined value of assets may deviate from its ideal allocation, without triggering a rebalancing alert. The combined deviation is calculated by adding each asset’s absolute deviation, then dividing by two.</small>
</div>
