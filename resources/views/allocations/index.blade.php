<?php

use App\Models\Allocation;

/** @var Allocation[] $allocations */

?>
@extends('layouts.app')

@section('content')
    <h1>Allocations</h1>

    <a href="{!! route('allocations.create') !!}">
        <i class="fa fa-plus-square"></i>
        Create new
    </a>

    <a href="{!! route('allocations.deleted') !!}">
        <i class="fa fa-trash-restore"></i>
        Show deleted
    </a>

    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>Name</th>
                <th>Operations</th>
            </tr>

            @forelse($allocations as $allocation)
                <tr>
                    <td>
                        <a href="{!! route('allocations.show', [$allocation, \App\Enums\AllocationViewMode::Detailed->value]) !!}">{{ $allocation->name }}</a>
                    </td>
                    <td>
                        <a href="{!! route('allocations.edit', $allocation) !!}" title="Edit..."><i class="fa fa-edit"></i></a>

                        <a href="#" data-toggle="modal" data-target="#deleteAllocationModal{{ $allocation->id }}" title="Delete..."><i class="fa fa-trash"></i></a>
                        @include('allocations._delete', ['allocation' => $allocation])
                    </td>
                </tr>
            @empty
                <tr><td colspan="999">There are no allocations.</td></tr>
            @endforelse
        </table>
    </div>
@endsection
