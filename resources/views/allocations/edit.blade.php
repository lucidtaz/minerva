<?php

use App\Models\Allocation;
use App\Models\Asset;
use App\Models\Webhook;
use Illuminate\Database\Eloquent\Collection;

/** @var Allocation $allocation */
/** @var string[] $timezones */
/** @var Asset[] $currencies */
/** @var Webhook[] $webhooks */
/** @var int $totalWeight */
/** @var Collection&Asset[] $selectableAssets */
/** @var Collection&Allocation[] $selectableAllocations */

?>
@extends('layouts.app')

@section('content')
    <h1>Edit '{{ $allocation->getOriginal('name') }}'</h1>

    <a href="{!! route('allocations.edit', $allocation) !!}">
        <i class="fa fa-edit"></i>
        Edit
    </a>

    <a href="{!! route('allocations.check.edit', $allocation) !!}">
        <i class="fa fa-flag"></i>
        Rebalancing checks
    </a>

    <form class="needs-validation" action="{!! route('allocations.update', $allocation) !!}" method="post" novalidate>
        @method('put')
        @csrf

        @include('allocations._form', ['allocation' => $allocation])

        <div class="table-responsive">
            <table class="table table-bordered mt-3">
                <tr>
                    <th>Asset class</th>
                    <th>Weight</th>
                    <th>Share</th>
                    <th>Operations</th>
                </tr>

                <tr>
                    <th class="text-center" colspan="99">Assigned</th>
                </tr>

                @foreach($allocation->childAllocations as $childAllocation)
                    <tr>
                        <td>{{ $childAllocation->name }}</td>
                        <td><input type="number" class="form-control" name="childAllocationWeights[{{ $childAllocation->pivot->id }}]" value="{{ $childAllocation->pivot->weight }}" /></td>
                        <td>{{ number_format(100 * $childAllocation->pivot->weight / $totalWeight) }}%</td>
                        <td>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="unassignChildAllocations[{{ $childAllocation->pivot->id }}]" name="unassignChildAllocations[{{ $childAllocation->pivot->id }}]" value="1" />
                                <label class="form-check-label text-danger" for="unassignChildAllocations[{{ $childAllocation->pivot->id }}]">Remove</label>
                            </div>
                        </td>
                    </tr>
                @endforeach

                @foreach($allocation->assets as $asset)
                    <tr>
                        <td>{{ $asset->name }}</td>
                        <td><input type="number" class="form-control" name="assetWeights[{{ $asset->pivot->id }}]" value="{{ $asset->pivot->weight }}" /></td>
                        <td>{{ number_format(100 * $asset->pivot->weight / $totalWeight) }}%</td>
                        <td>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="unassignAssets[{{ $asset->pivot->id }}]" name="unassignAssets[{{ $asset->pivot->id }}]" value="1" />
                                <label class="form-check-label text-danger" for="unassignAssets[{{ $asset->pivot->id }}]">Remove</label>
                            </div>
                        </td>
                    </tr>
                @endforeach

                <tr>
                    <th class="text-center" colspan="99">Available</th>
                </tr>

                @foreach($selectableAllocations as $childAllocation)
                    <tr>
                        <td>{{ $childAllocation->name }}</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="assignChildAllocations[{{ $childAllocation->id }}]" name="assignChildAllocations[{{ $childAllocation->id }}]" value="1" />
                                <label class="form-check-label" for="assignChildAllocations[{{ $childAllocation->id }}]">Add</label>
                            </div>
                        </td>
                    </tr>
                @endforeach

                @foreach($selectableAssets as $asset)
                    <tr>
                        <td>{{ $asset->name }}</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="assignAssets[{{ $asset->id }}]" name="assignAssets[{{ $asset->id }}]" value="1" />
                                <label class="form-check-label" for="assignAssets[{{ $asset->id }}]">Add</label>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>

        <button type="submit" class="btn btn-primary">Update allocation</button>

    </form>

@endsection
