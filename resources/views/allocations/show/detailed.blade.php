<?php

use App\Dtos\RenderedAllocation;
use App\Models\Allocation;
use App\Presentation\NumberDisplayOptions;
use Carbon\Carbon;

/** @var Allocation $allocation */
/** @var RenderedAllocation $renderedAllocation */
/** @var NumberDisplayOptions $numberDisplayOptions */
/** @var Carbon $moment */

?>
@extends('layouts.app')

@section('content')
    <h1>{{ $allocation->name }} - Overview</h1>

    <a href="{!! route('allocations.show', [$allocation, \App\Enums\AllocationViewMode::Detailed->value]) !!}">
        <i class="fa fa-eye"></i>
        Detailed
    </a>

    <a href="{!! route('allocations.show', [$allocation, \App\Enums\AllocationViewMode::Simple->value]) !!}">
        <i class="fa fa-eye"></i>
        Simple
    </a>

    <a href="{!! route('allocations.show', [$allocation, \App\Enums\AllocationViewMode::Purchase->value]) !!}">
        <i class="fa fa-eye"></i>
        Purchase
    </a>

    <a href="{!! route('allocations.edit', $allocation) !!}">
        <i class="fa fa-edit"></i>
        Edit
    </a>

    <a href="{!! route('allocations.value', $allocation) !!}">
        <i class="fa fa-chart-line"></i>
        Show value history
    </a>

    <a href="#" data-toggle="modal" data-target="#deleteAllocationModal{{ $allocation->id }}"><i class="fa fa-trash"></i> Delete</a>
    @include('allocations._delete', ['allocation' => $allocation])

    <form class="form-inline">
        <label class="sr-only" for="moment">Date</label>
        <input type="text" class="form-control mb-2 mr-sm-2" id="moment" name="moment" value="{{ $moment->format('Y-m-d') }}" placeholder="yyyy-mm-dd">

        <button type="submit" class="btn btn-primary mb-2">Refresh</button>
    </form>

    <div class="table-responsive">
        <table class="table table-bordered mb-5">
            <tr>
                <th>Asset class</th>
                <th>Share (target)</th>
                <th>Amount</th>
                <th>Price</th>
                <th>Value</th>
                <th>Target value</th>
                <th>±&nbsp;value</th>
                <th>±&nbsp;amount</th>
            </tr>

            @include('allocations._lines', ['lines' => $renderedAllocation->lines, 'indentLevel' => '0', 'collapsedByDefault' => false, 'moment' => $moment])

            <tr>
                <td>{{ $renderedAllocation->sum->name }}</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>{{ $renderedAllocation->sum->value->baseAsset->currency_symbol }} {{ number_format($renderedAllocation->sum->value->value, $numberDisplayOptions->valueDecimalPlaces) }}</td>
                <td>{{ $renderedAllocation->sum->value->baseAsset->currency_symbol }} {{ number_format($renderedAllocation->sum->value->value, $numberDisplayOptions->valueDecimalPlaces) }}</td>
                <td>{{ $renderedAllocation->sum->value->baseAsset->currency_symbol }} {{ number_format('0', $numberDisplayOptions->valueDecimalPlaces) }}</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>

    <div>
        Total value deviation: {{ $renderedAllocation->totalDeviation->baseAsset->currency_symbol }} {{ number_format($renderedAllocation->totalDeviation->value, $numberDisplayOptions->valueDecimalPlaces) }} ({{ number_format($renderedAllocation->totalDeviationRatio * 100, $numberDisplayOptions->fractionDecimalPlaces) }}%)
    </div>
@endsection
