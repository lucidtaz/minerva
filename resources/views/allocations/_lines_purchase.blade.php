<?php

use App\Dtos\AllocationLine;

/** @var AllocationLine[] $lines */
/** @var int $indentLevel */
/** @var bool $collapsedByDefault */

?>
@foreach($lines as $line)
    <!-- Render sublines above their parent line, so the parent line becomes a summation -->
    @include('allocations._lines_purchase', ['lines' => $line->subLines, 'indentLevel' => $indentLevel + 1, 'collapsedByDefault' => true])

    <tr class="allocation-lines allocation-{{ $line->allocation->id }}-lines @if($collapsedByDefault) collapse @endif">
        <td style="padding-left: {{ $indentLevel*2+1 }}em">
            @if($line->url !== null)
                <a href="{{ $line->url }}">{{ $line->name }}</a>
            @else
                {{ $line->name }}
            @endif

            @if($line->sumsAllocation !== null)
                <button class="btn btn-outline-primary btn-sm ml-1" type="button" data-toggle="collapse" data-target=".allocation-{{ $line->sumsAllocation->id }}-lines" aria-expanded="false">
                    {{ trans_choice('allocation.sub_lines', count($line->subLines)) }}
                </button>
            @endif
        </td>
        <td>
            @if($line->deltaAmount !== null)
                @if(bccomp($line->deltaAmount, '0') === -1)
                    <span class="text-danger">{{ number_format($line->deltaAmount, $numberDisplayOptions->amountDecimalPlaces) }}</span>
                @elseif(bccomp($line->deltaAmount, '0') === 1)
                    <span class="text-success">{{ number_format($line->deltaAmount, $numberDisplayOptions->amountDecimalPlaces) }}</span>
                @else
                    <span class="text-muted">{{ number_format($line->deltaAmount, $numberDisplayOptions->amountDecimalPlaces) }}</span>
                @endif
            @else
                &nbsp;
            @endif
        </td>
        <td>
            @if($line->deltaValueInOriginalBase !== null)
                @if(bccomp($line->deltaValueInOriginalBase->value, '0') === -1)
                    <span class="text-danger">{{ $line->deltaValueInOriginalBase->baseAsset->currency_symbol }} {{ number_format($line->deltaValueInOriginalBase->value, $numberDisplayOptions->valueDecimalPlaces) }}</span>
                @elseif(bccomp($line->deltaValueInOriginalBase->value, '0') === 1)
                    <span class="text-success">{{ $line->deltaValueInOriginalBase->baseAsset->currency_symbol }} {{ number_format($line->deltaValueInOriginalBase->value, $numberDisplayOptions->valueDecimalPlaces) }}</span>
                @else
                    <span class="text-muted">{{ $line->deltaValueInOriginalBase->baseAsset->currency_symbol }} {{ number_format($line->deltaValueInOriginalBase->value, $numberDisplayOptions->valueDecimalPlaces) }}</span>
                @endif
            @else
                &nbsp;
            @endif
        </td>
        <td>
            @if($line->deltaValue !== null)
                @if(bccomp($line->deltaValue->value, '0') === -1)
                    <span class="text-danger">{{ $line->deltaValue->baseAsset->currency_symbol }} {{ number_format($line->deltaValue->value, $numberDisplayOptions->valueDecimalPlaces) }}</span>
                @elseif(bccomp($line->deltaValue->value, '0') === 1)
                    <span class="text-success">{{ $line->deltaValue->baseAsset->currency_symbol }} {{ number_format($line->deltaValue->value, $numberDisplayOptions->valueDecimalPlaces) }}</span>
                @else
                    <span class="text-muted">{{ $line->deltaValue->baseAsset->currency_symbol }} {{ number_format($line->deltaValue->value, $numberDisplayOptions->valueDecimalPlaces) }}</span>
                @endif
            @else
                &nbsp;
            @endif
        </td>
    </tr>
@endforeach
