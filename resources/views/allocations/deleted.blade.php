<?php

use App\Models\Allocation;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/** @var LengthAwarePaginator&Allocation[] $allocations */

?>
@extends('layouts.app')

@section('content')
    <h1>Deleted allocations</h1>

    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>Name</th>
                <th>Number of entries</th>
                <th>Date deleted</th>
                <th>Operations</th>
            </tr>

            @forelse($allocations as $allocation)
                <tr>
                    <td>
                        {{ $allocation->name }}
                    </td>
                    <td>
                        {{ $allocation->assets_count + $allocation->child_allocations_count }}
                    </td>
                    <td>
                        {{ $allocation->deleted_at->ago() }}
                    </td>
                    <td>
                        <form action="{{ route('allocations.restore', $allocation) }}" method="post">
                            @csrf
                            <button type="submit" class="btn btn-primary">Restore</button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr><td colspan="999">There are no deleted allocations.</td></tr>
            @endforelse
        </table>

        {!! $allocations->links() !!}
    </div>
@endsection
