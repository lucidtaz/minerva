<?php

use App\Models\Allocation;

/** @var Allocation $allocation */

?>
@extends('layouts.app')

@section('content')
    <h1>New allocation</h1>

    <form class="needs-validation" action="{!! route('allocations.store') !!}" method="post" novalidate>
        @csrf

        @include('allocations._form', ['allocation' => $allocation])

        <button type="submit" class="btn btn-primary">Create allocation</button>
    </form>
@endsection
