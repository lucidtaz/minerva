<?php

use App\Dtos\AllocationLine;

/** @var AllocationLine[] $lines */
/** @var int $indentLevel */
/** @var bool $collapsedByDefault */

?>
@foreach($lines as $line)
    <!-- Render sublines above their parent line, so the parent line becomes a summation -->
    @include('allocations._lines_simple', ['lines' => $line->subLines, 'indentLevel' => $indentLevel + 1, 'collapsedByDefault' => true])

    <tr class="allocation-lines allocation-{{ $line->allocation->id }}-lines @if($collapsedByDefault) collapse @endif">
        <td style="padding-left: {{ $indentLevel*2+1 }}em">
            @if($line->url !== null)
                <a href="{{ $line->url }}">{{ $line->name }}</a>
            @else
                {{ $line->name }}
            @endif

            @if($line->sumsAllocation !== null)
                <button class="btn btn-outline-primary btn-sm ml-1" type="button" data-toggle="collapse" data-target=".allocation-{{ $line->sumsAllocation->id }}-lines" aria-expanded="false">
                    {{ trans_choice('allocation.sub_lines', count($line->subLines)) }}
                </button>
            @endif
        </td>
        <td>
            {{ number_format($line->currentShare * 100, $numberDisplayOptions->fractionDecimalPlaces) }}% ({{ number_format($line->targetShare * 100, $numberDisplayOptions->fractionDecimalPlaces) }}%)
        </td>
        <td>{{ $line->value->baseAsset->currency_symbol }} {{ number_format($line->value->value, $numberDisplayOptions->valueDecimalPlaces) }}</td>
        <td>{{ $line->value->baseAsset->currency_symbol }} {{ number_format($line->targetValue->value, $numberDisplayOptions->valueDecimalPlaces) }}</td>
        <td>
            @if($line->deltaValue !== null)
                @if(bccomp($line->deltaValue->value, '0') === -1)
                    <span class="text-danger">{{ $line->deltaValue->baseAsset->currency_symbol }} {{ number_format($line->deltaValue->value, $numberDisplayOptions->valueDecimalPlaces) }}</span>
                @elseif(bccomp($line->deltaValue->value, '0') === 1)
                    <span class="text-success">{{ $line->deltaValue->baseAsset->currency_symbol }} {{ number_format($line->deltaValue->value, $numberDisplayOptions->valueDecimalPlaces) }}</span>
                @else
                    <span class="text-muted">{{ $line->deltaValue->baseAsset->currency_symbol }} {{ number_format($line->deltaValue->value, $numberDisplayOptions->valueDecimalPlaces) }}</span>
                @endif
            @else
                &nbsp;
            @endif
        </td>
    </tr>
@endforeach
