<?php

use App\Models\Asset;

/** @var Asset $asset */

?>
<div class="modal" id="deleteAssetModal{{ $asset->uuid }}" tabindex="-1" role="dialog" aria-labelledby="deleteAssetModalLabel{{ $asset->uuid }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteAssetModalLabel{{ $asset->uuid }}">Delete asset '{{ $asset->name }}'</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this asset? You can still undo it later.</p>
                <p>{{ trans_choice('asset.transaction_count', $asset->transactions_count) }}.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                <form action="{{ route('assets.delete', $asset) }}" method="post">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>
