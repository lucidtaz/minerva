<?php

use App\Models\Asset;

/** @var Asset $asset */

?>
@extends('layouts.app')

@section('content')
    <h1>Edit '{{ $asset->getOriginal('name') }}'</h1>

    <form class="needs-validation" action="{!! route('assets.update', $asset) !!}" method="post" novalidate>
        @method('put')
        @csrf

        @include('assets._form', ['asset' => $asset])

        @include('assets._aliases_form', ['asset' => $asset])

        <button type="submit" class="btn btn-primary">Update asset</button>
    </form>
@endsection
