<?php

use App\Models\Asset;

/** @var Asset $asset */

?>
<div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" id="name" value="{{ $asset->name }}" required />
    @error('name')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="mode">Mode</label>

    <div class="form-check @if ($errors->has('mode')) is-invalid @endif">
        <input class="form-check-input" type="radio" name="mode" id="mode-normal" value="{{ \App\Enums\AssetMode::Normal->value }}" @if($asset->mode === \App\Enums\AssetMode::Normal) checked @endif>
        <label class="form-check-label" for="mode-normal">
            Normal
        </label>
    </div>

    <div class="form-check @if ($errors->has('mode')) is-invalid @endif">
        <input class="form-check-input" type="radio" name="mode" id="mode-simple" value="{{ \App\Enums\AssetMode::Simple->value }}" @if($asset->mode === \App\Enums\AssetMode::Simple) checked @endif>
        <label class="form-check-label" for="mode-simple">
            Simple
        </label>
    </div>

    @error('mode')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror

    <small id="modeHelp" class="form-text text-muted">For assets in "normal" mode, you will be able to control the price and transactions of this asset separately. For "simple" assets you only need to control the total value, there is no concept of price movement.</small>
</div>

<div class="form-group">
    <label for="currency_symbol">Currency symbol (optional)</label>
    <input type="text" class="form-control @if ($errors->has('currency_symbol')) is-invalid @endif" name="currency_symbol" id="currency_symbol" value="{{ $asset->currency_symbol }}" />
    <small id="currencySymbolHelp" class="form-text text-muted">Specifying a currency symbol makes this asset selectable as a currency for prices of any other asset</small>
    @error('currency_symbol')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>
