<?php

use App\Models\Asset;

/** @var Asset $asset */

?>
@extends('layouts.app')

@section('content')
    <h1>New asset</h1>

    <form class="needs-validation" action="{!! route('assets.store') !!}" method="post" novalidate>
        @csrf

        @include('assets._form', ['asset' => $asset])

        <button type="submit" class="btn btn-primary">Create asset</button>
    </form>
@endsection
