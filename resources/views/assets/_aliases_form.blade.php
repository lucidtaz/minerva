<?php

use App\Models\Asset;

/** @var Asset $asset */

?>
<label>Aliases</label>

@if(Session::exists('aliasErrors'))
    <div class="invalid-feedback d-block">{{ implode(', ', Session::get('aliasErrors')) }}</div>
@endif

@foreach($asset->aliases as $assetAlias)
    <div class="form-group">
        <input type="text" class="form-control" name="aliases[{{ $assetAlias->id }}]" maxlength="64" value="{{ $assetAlias->alias }}" />
    </div>
@endforeach

<div class="form-group">
    <input type="text" class="form-control" name="new_aliases[]" maxlength="64" placeholder="New alias..." />
    <small id="assetAliasHelp" class="form-text text-muted">Asset aliases are used to determine the asset during imports of prices and transactions. An asset can have multiple aliases, for example a human-readable name, an international stock identification number and a uuid. If any of the aliases matches against the input data, the asset is selected. The asset alias must be unique within your organization.</small>
</div>
