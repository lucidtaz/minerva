<?php

use App\Models\Asset;
use App\Models\Price;

/** @var Asset[] $assets */
/** @var Price[] $prices */

?>
@extends('layouts.app')

@section('content')
    <h1>Assets</h1>

    <a href="{!! route('assets.create') !!}">
        <i class="fa fa-plus-square"></i>
        Create new
    </a>

    <a href="{!! route('assets.deleted') !!}">
        <i class="fa fa-trash-restore"></i>
        Show deleted
    </a>

    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>Asset</th>
                <th>Price</th>
                <th>Price age</th>
                <th>Operations</th>
            </tr>

            @forelse($assets as $asset)
                <tr>
                    <td>
                        <a href="{!! route('assets.show', $asset) !!}">{{ $asset->name }}</a>
                    </td>
                    <td>
                        @if($prices[$asset->id]->baseAsset !== null)
                            {{ $prices[$asset->id]->rebasedBase->currency_symbol }} {{ $prices[$asset->id]->rebasedValue }}
                        @else
                            -
                        @endif
                    </td>
                    <td>
                        @if($prices[$asset->id] !== null && $prices[$asset->id]->moment->notEqualTo(\Carbon\Carbon::minValue()))
                            <time title="{{ $prices[$asset->id]->moment->setTimeZone($timezone)->toDayDateTimeString() }}" datetime="{{ $prices[$asset->id]->moment->toISOString() }}">
                                {{ $prices[$asset->id]->moment->ago() }}
                            </time>
                        @else
                            <i>Never</i>
                        @endif
                    </td>
                    <td>
                        <a href="{!! route('assets.edit', $asset) !!}" title="Edit..."><i class="fa fa-edit"></i></a>

                        <a href="#" data-toggle="modal" data-target="#deleteAssetModal{{ $asset->uuid }}" title="Delete..."><i class="fa fa-trash"></i></a>
                        @include('assets._delete', ['asset' => $asset])
                    </td>
                </tr>
            @empty
                <tr><td colspan="999">There are no assets.</td></tr>
            @endforelse
        </table>
    </div>
@endsection
