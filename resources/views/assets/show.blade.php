<?php

use App\Dtos\DataSet;
use App\Dtos\Value;
use App\Models\Asset;
use App\Models\Price;
use App\Models\Transaction;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Enumerable;

/** @var Asset $asset */
/** @var string $amount */
/** @var Price $price */
/** @var Value $value */
/** @var LengthAwarePaginator&Transaction[] */
/** @var LengthAwarePaginator&Price[] $prices */
/** @var Enumerable&DataSet[] $priceDataSets */
/** @var Enumerable&DataSet[] $valueDataSets */

?>
@extends('layouts.app')

@section('content')
    <h1>{{ $asset->name }}</h1>

    <a href="{!! route('assets.edit', $asset) !!}">
        <i class="fa fa-edit"></i>
        Edit
    </a>

    @if($asset->mode === \App\Enums\AssetMode::Normal)
        <a href="{!! route('prices.create', $asset) !!}">
            <i class="fa fa-hand-holding-usd"></i>
            Add price
        </a>

        <a href="{!! route('transactions.create', $asset) !!}">
            <i class="fa fa-cash-register"></i>
            Add transaction
        </a>
    @elseif($asset->mode === \App\Enums\AssetMode::Simple)
        <a href="{!! route('values.create', $asset) !!}">
            <i class="fa fa-cash-register"></i>
            Add value
        </a>
    @endif

    <a href="#" data-toggle="modal" data-target="#deleteAssetModal{{ $asset->uuid }}"><i class="fa fa-trash"></i> Delete</a>
    @include('assets._delete', ['asset' => $asset])

    <ul>
        <li>id: {{ $asset->id }}</li>
        <li>uuid: {{ $asset->uuid }}</li>
        <li>name: {{ $asset->name }}</li>
        @if($asset->mode === \App\Enums\AssetMode::Normal)
            <li>amount: {{ $amount }}</li>
            <li>price: {{ $price->rebasedBase->currency_symbol}} {{ $price->rebasedValue }}</li>
        @endif
        <li>value: {{ $value->baseAsset->currency_symbol }} {{ $value->value }}</li>
        <li>aliases: {!! $asset->aliases->pluck('alias')->map(fn (string $alias) => '<code>' . e($alias) . '</code>')->join(', ') !!} </li>
        <li>mode: {{ $asset->mode->value }}</li>
    </ul>

    <h2>Value history</h2>

    <x-chart :dataSets="$valueDataSets" />

    <h2>Transactions</h2>
    <div class="table-responsive">
        <table class="table table-bordered">
            <tr>
                <th>Moment</th>
                <th>Amount</th>
                <th>Location</th>
                <th>Operations</th>
            </tr>
            @forelse($transactions as $transaction)
                <tr>
                    <td>{{ $transaction->moment->setTimeZone($timezone)->toDateTimeString() }}</td>
                    <td>{{ $transaction->amount }} @if(!empty($transaction->note)) <i class="fa fa-file-alt text-primary" data-toggle="tooltip" title="{{ $transaction->note }}"></i> @endif  @if($transaction->is_temporary) <i class="fa fa-info-circle @if($transaction->is_expired_temporary) text-danger @else text-primary @endif" data-toggle="tooltip" title="Temporary, {{ $transaction->created_at->ago() }}"></i> @endif</td>
                    <td>{{ $transaction->location }}</td>
                    <td>
                        <a href="{!! route('transactions.edit', [$asset, $transaction]) !!}" title="Edit..."><i class="fa fa-edit"></i></a>

                        <a href="#" data-toggle="modal" data-target="#deleteTransactionModal{{ $transaction->id }}" title="Delete..."><i class="fa fa-trash"></i></a>
                        @include('transactions._delete', ['transaction' => $transaction])
                    </td>
                </tr>
            @empty
                <tr><td colspan="999">There are no transactions.</td></tr>
            @endforelse
        </table>

        {!! $transactions->links() !!}
    </div>

    <h2>Price history</h2>

    <x-chart :dataSets="$priceDataSets" />

    <div class="table-responsive">
        <table class="table table-bordered">
            <tr>
                <th>Moment</th>
                <th>Value</th>
                <th>Operations</th>
            </tr>
            @forelse($prices as $price)
                <tr>
                    <td>{{ $price->moment->setTimeZone($timezone)->toDateTimeString() }}</td>
                    @if($price->rebasedBase->id !== $price->base_asset_id)
                        <td data-toggle="tooltip" title="{{ $price->baseAsset->currency_symbol }} {{ $price->value }}">
                            <i>{{ $price->rebasedBase->currency_symbol }} {{ $price->rebasedValue }}</i>
                        </td>
                    @else
                        <td>
                            {{ $price->rebasedBase->currency_symbol }} {{ $price->rebasedValue }}
                        </td>
                    @endif
                    <td>
                        <a href="#" data-toggle="modal" data-target="#deletePriceModal{{ $price->id }}" title="Delete..."><i class="fa fa-trash"></i></a>
                        @include('prices._delete', ['price' => $price])
                    </td>
                </tr>
            @empty
                <tr><td colspan="999">There are no prices.</td></tr>
            @endforelse
        </table>

        {!! $prices->links() !!}
    </div>
@endsection
