<?php

use App\Models\Asset;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/** @var LengthAwarePaginator&Asset[] $assets */

?>
@extends('layouts.app')

@section('content')
    <h1>Deleted assets</h1>

    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>Asset</th>
                <th>Transactions</th>
                <th>Date deleted</th>
                <th>Operations</th>
            </tr>

            @forelse($assets as $asset)
                <tr>
                    <td>
                        {{ $asset->name }}
                    </td>
                    <td>
                        {{ $asset->transactions_count }}
                    </td>
                    <td>
                        {{ $asset->deleted_at->ago() }}
                    </td>
                    <td>
                        <form action="{{ route('assets.restore', $asset->uuid) }}" method="post" class="form-inline d-inline-flex">
                            @csrf
                            <button type="submit" class="btn btn-primary">Restore</button>
                        </form>
                        <form action="{{ route('assets.forceDelete', $asset->uuid) }}" method="post" class="form-inline d-inline-flex">
                            @csrf
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr><td colspan="999">There are no deleted assets.</td></tr>
            @endforelse
        </table>

        {!! $assets->links() !!}
    </div>
@endsection
