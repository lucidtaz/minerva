@component('mail::message')
# Test Message

This is a message to verify that your {{ config('app.name') }} installation is working.

Regards,<br />
{{ config('app.name') }}
@endcomponent
