<?php

use App\Models\Price;

/** @var Price $price */

?>
<div class="modal" id="deletePriceModal{{ $price->id }}" tabindex="-1" role="dialog" aria-labelledby="deletePriceModalLabel{{ $price->id }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deletePriceModalLabel{{ $price->id }}">Delete price</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this price? You cannot undo this.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                <form action="{{ route('prices.delete', [$asset, $price]) }}" method="post">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>
