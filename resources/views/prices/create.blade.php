<?php

use App\Models\Asset;
use App\Models\Price;

/** @var Asset $asset */
/** @var Price $price */
/** @var Asset[] $baseAssets */

?>
@extends('layouts.app')

@section('content')
    <h1>Add price for {{ $asset->name }}</h1>

    <form class="needs-validation" action="{!! route('prices.store', $asset) !!}" method="post" novalidate>
        @csrf

        @include('prices._form', ['price' => $price, 'asset' => $asset, 'baseAssets' => $baseAssets])

        <button type="submit" class="btn btn-primary">Add price</button>
    </form>
@endsection
