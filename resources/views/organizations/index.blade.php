<?php

use App\Models\Organization;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/** @var LengthAwarePaginator&Organization[] $organizations */

?>
@extends('layouts.app')

@section('content')
    <h1>Organizations</h1>

    <a href="{!! route('organizations.create') !!}">
        <i class="fa fa-plus-square"></i>
        Create new
    </a>

    <a href="{!! route('organizations.deleted') !!}">
        <i class="fa fa-trash-restore"></i>
        Show deleted
    </a>

    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>Organization</th>
                <th>Users</th>
                <th>Operations</th>
            </tr>

            @foreach($organizations as $organization)
                <tr>
                    <td>
                        <a href="{!! route('organizations.show', $organization) !!}">{{ $organization->name }}</a>
                    </td>
                    <td>
                        {{ $organization->users_count }}
                    </td>
                    <td>
                        <a href="{!! route('organizations.edit', $organization) !!}" title="Edit..."><i class="fa fa-edit"></i></a>

                        <a href="#" data-toggle="modal" data-target="#deleteOrganizationModal{{ $organization->id }}" title="Delete..."><i class="fa fa-trash"></i></a>
                        @include('organizations._delete', ['organization' => $organization])
                    </td>
                </tr>
            @endforeach
        </table>

        {!! $organizations->links() !!}
    </div>
@endsection
