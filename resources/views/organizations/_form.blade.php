<?php

use App\Models\Organization;

/** @var Organization $organization */

?>
<div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" id="name" value="{{ $organization->name }}" required autocomplete="organization" />
    @error('name')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>
