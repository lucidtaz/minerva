<?php

use App\Models\Organization;

/** @var Organization $organization */

?>
@extends('layouts.app')

@section('content')
    <h1>{{ $organization->name }}</h1>

    <a href="{!! route('organizations.edit', $organization) !!}">
        <i class="fa fa-edit"></i>
        Edit
    </a>

    <a href="{!! route('invitations.index') !!}">
        <i class="fa fa-id-badge"></i>
        Manage invitations
    </a>

    <a href="{!! route('exports.index') !!}">
        <i class="fa fa-file-export"></i>
        Manage exports
    </a>

    <a href="#" data-toggle="modal" data-target="#deleteOrganizationModal{{ $organization->id }}"><i class="fa fa-trash"></i> Delete</a>
    @include('organizations._delete', ['organization' => $organization])

    <h2>Users</h2>
    <ul>
        @foreach($organization->users as $user)
            <li>{{ $user->name }} - {{ $user->email }} ({{ ucfirst($user->pivot->role->value) }} role)</li>
        @endforeach()
    </ul>
@endsection
