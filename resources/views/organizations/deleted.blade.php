<?php

use App\Models\Organization;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/** @var LengthAwarePaginator&Organization[] $organizations */

?>
@extends('layouts.app')

@section('content')
    <h1>Deleted organizations</h1>

    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>Organization</th>
                <th>Users</th>
                <th>Date deleted</th>
                <th>Operations</th>
            </tr>

            @forelse($organizations as $organization)
                <tr>
                    <td>
                        {{ $organization->name }}
                    </td>
                    <td>
                        {{ $organization->users_count }}
                    </td>
                    <td>
                        {{ $organization->deleted_at->ago() }}
                    </td>
                    <td>
                        <form action="{{ route('organizations.restore', $organization) }}" method="post">
                            @csrf
                            <button type="submit" class="btn btn-primary">Restore</button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr><td colspan="999">There are no deleted organizations.</td></tr>
            @endforelse
        </table>

        {!! $organizations->links() !!}
    </div>
@endsection
