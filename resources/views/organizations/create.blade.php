<?php

use App\Models\Organization;

/** @var Organization $organization */

?>
@extends('layouts.app')

@section('content')
    <h1>New organization</h1>

    <form class="needs-validation" action="{!! route('organizations.store') !!}" method="post" novalidate>
        @csrf

        @include('organizations._form', ['organization' => $organization])

        <button type="submit" class="btn btn-primary">Create organization</button>
    </form>
@endsection
