<?php

use App\Models\Organization;

/** @var Organization $organization */

?>
@extends('layouts.app')

@section('content')
    <h1>Edit '{{ $organization->getOriginal('name') }}'</h1>

    <form class="needs-validation" action="{!! route('organizations.update', $organization) !!}" method="post" novalidate>
        @method('put')
        @csrf

        @include('organizations._form', ['organization' => $organization])

        <button type="submit" class="btn btn-primary">Update organization</button>
    </form>
@endsection
