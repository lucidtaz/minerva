<?php

use App\Models\ImportConfiguration;

/** @var ImportConfiguration $importConfiguration */
/** @var string[] $selectableTimezones */

?>
<div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" id="name" value="{{ $importConfiguration->name }}" required />
    @error('name')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="note">Note</label>
    <textarea class="form-control @if ($errors->has('note')) is-invalid @endif" rows="5" name="note" id="note" aria-describedby="noteHelp">{{ $importConfiguration->note }}</textarea>
    <small id="noteHelp" class="form-text text-muted">Use this field for your own convenience, for example to show instructions how to create an export in another system</small>
    @error('note')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="input_method">Input method</label>

    <div class="form-check @if ($errors->has('input_method')) is-invalid @endif">
        <input class="form-check-input" type="radio" name="input_method" id="input_method-download" value="{{ \App\Enums\InputMethod::Download->value }}" @if($importConfiguration->input_method === \App\Enums\InputMethod::Download) checked @endif>
        <label class="form-check-label" for="input_method-download">
            Download
        </label>
    </div>

    <div class="form-check @if ($errors->has('input_method')) is-invalid @endif">
        <input class="form-check-input" type="radio" name="input_method" id="input_method-upload" value="{{ \App\Enums\InputMethod::Upload->value }}" @if($importConfiguration->input_method === \App\Enums\InputMethod::Upload) checked @endif>
        <label class="form-check-label" for="input_method-upload">
            Upload
        </label>
    </div>

    @error('input_method')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="input_content_type">Input content type</label>

    <div class="form-check @if ($errors->has('input_content_type')) is-invalid @endif">
        <input class="form-check-input" type="radio" name="input_content_type" id="input_content_type-json" value="{{ \App\Enums\InputContentType::JSON->value }}" @if($importConfiguration->input_content_type === \App\Enums\InputContentType::JSON) checked @endif>
        <label class="form-check-label" for="input_content_type-json">
            JSON
        </label>
    </div>

    <div class="form-check @if ($errors->has('input_content_type')) is-invalid @endif">
        <input class="form-check-input" type="radio" name="input_content_type" id="input_content_type-csv" value="{{ \App\Enums\InputContentType::CSV->value }}" @if($importConfiguration->input_content_type === \App\Enums\InputContentType::CSV) checked @endif>
        <label class="form-check-label" for="input_content_type-csv">
            CSV
        </label>
    </div>

    @error('input_content_type')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="date_format">Date format</label>
    <input type="text" class="form-control @if ($errors->has('date_format')) is-invalid @endif" name="date_format" id="date_format" value="{{ $importConfiguration->date_format }}" />
    <small id="dateFormatHelp" class="form-text text-muted">Leave empty for automatic format recognition. Use control characters from <a href="https://www.php.net/manual/en/datetime.createfromformat.php">PHP</a> to override. Some examples: <code>U</code> to accept a unix timestamp, <code>Y-m-d H:i:s</code> to accept 2020-06-17 18:36:41</small>
    @error('date_format')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>


<div class="form-group">
    <label for="timezone">Timezone</label>
    <select class="form-control @if ($errors->has('timezone')) is-invalid @endif" name="timezone" id="timezone" required>
        <option value="-1">Please select...</option>
        @foreach($selectableTimezones as $selectableTimezone)
            <option value="{{ $selectableTimezone }}" @if($importConfiguration->timezone->getName() === $selectableTimezone) selected="selected" @endif>{{ $selectableTimezone }}</option>
        @endforeach
    </select>
    <small id="timezoneHelp" class="form-text text-muted">This value controls which timezone is used when the input data does not specify one. Note that this is only relevant if the input contains times. If the input only specifies dates, you can set anything here.</small>
    @error('timezone')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="url">URL (Only applicable when input method = download)</label>
    <input type="url" class="form-control @if ($errors->has('url')) is-invalid @endif" name="url" id="url" value="{{ $importConfiguration->url }}" />
    @error('url')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="has_header_row">CSV options (Only applicable when input content type = csv)</label>

    <div class="form-check @if ($errors->has('has_header_row')) is-invalid @endif">
        <input type="hidden" name="has_header_row" value="0" />
        <input class="form-check-input" type="checkbox" name="has_header_row" id="has_header_row" value="1" @if($importConfiguration->has_header_row) checked @endif>
        <label class="form-check-label" for="has_header_row">
            Input has header row
        </label>
    </div>

    @error('has_header_row')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="delimiter">Delimiter (Only applicable when input content type = csv)</label>
    <input type="text" class="form-control @if ($errors->has('delimiter')) is-invalid @endif" name="delimiter" id="delimiter" value="{{ $importConfiguration->delimiter }}" />
    @error('delimiter')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="enclosure_character">Enclosure character (Only applicable when input content type = csv)</label>
    <input type="text" class="form-control @if ($errors->has('enclosure_character')) is-invalid @endif" name="enclosure_character" id="enclosure_character" value="{{ $importConfiguration->enclosure_character }}" />
    @error('enclosure_character')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="escape_character">Escape character (Only applicable when input content type = csv)</label>
    <input type="text" class="form-control @if ($errors->has('escape_character')) is-invalid @endif" name="escape_character" id="escape_character" value="{{ $importConfiguration->escape_character }}" />
    @error('escape_character')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="entity_type">Import entity type</label>

    <div class="form-check @if ($errors->has('entity_type')) is-invalid @endif">
        <input class="form-check-input" type="radio" name="entity_type" id="entity_type-asset" value="{{ \App\Enums\EntityType::Asset->value }}" @if($importConfiguration->entity_type === \App\Enums\EntityType::Asset) checked @endif>
        <label class="form-check-label" for="entity_type-asset">
            Asset
        </label>
    </div>

    <div class="form-check @if ($errors->has('entity_type')) is-invalid @endif">
        <input class="form-check-input" type="radio" name="entity_type" id="entity_type-price" value="{{ \App\Enums\EntityType::Price->value }}" @if($importConfiguration->entity_type === \App\Enums\EntityType::Price) checked @endif>
        <label class="form-check-label" for="entity_type-price">
            Price
        </label>
    </div>

    <div class="form-check @if ($errors->has('entity_type')) is-invalid @endif">
        <input class="form-check-input" type="radio" name="entity_type" id="entity_type-transaction" value="{{ \App\Enums\EntityType::Transaction->value }}" @if($importConfiguration->entity_type === \App\Enums\EntityType::Transaction) checked @endif>
        <label class="form-check-label" for="entity_type-transaction">
            Transaction
        </label>
    </div>

    @error('entity_type')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="parse_expression">Parse expression</label>
    <textarea class="form-control text-monospace @if ($errors->has('parse_expression')) is-invalid @endif" rows="5" name="parse_expression" id="parse_expression" required aria-describedby="parseExpressionHelpBody">{{ $importConfiguration->parse_expression }}</textarea>
    @error('parse_expression')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror

    <div id="parseExpressionHelpAccordion" class="mt-1">
        <div class="card">
            <div class="card-header" id="parseExpressionHelpHeader">
                <h5 class="mb-0">
                    <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#parseExpressionHelpBody" aria-expanded="false" aria-controls="parseExpressionHelpBody">
                        <i class="fa fa-question-circle mr-1 text-primary fa-fw"></i>Parse expression help
                    </button>
                </h5>
            </div>
            <div id="parseExpressionHelpBody" class="collapse" aria-labelledby="parseExpressionHelpHeader" data-parent="#parseExpressionHelpAccordion">
                <div class="card-body">
                    <p>
                        A parse expression is a <a href="https://stedolan.github.io/jq/" target="_blank">jq</a> expression that takes the JSON response contents and
                        transform it into an array of objects with specific properties.
                    </p>

                    <p>
                        The easiest way to construct one of these is to follow these steps:
                    </p>

                    <ol>
                        <li>Make an expression that gets each element from the response</li>
                        <li>Make an expression that maps each element to an object with the specified properties. For supported properties, see below.</li>
                        <li>Separate the two with a pipe symbol</li>
                        <li>Wrap the expression in square brackets to combine all results into an array</li>
                    </ol>

                    <h3>Asset</h3>
                    <div class="table-responsive">
                        <table class="table col-md-12 col-lg-10">
                            <tr>
                                <th>Attribute</th>
                                <th>Type</th>
                                <th>Required?</th>
                                <th>Notes</th>
                            </tr>
                            <tr>
                                <td>name</td>
                                <td>string</td>
                                <td>required</td>
                                <td>The name of the asset</td>
                            </tr>
                            <tr>
                                <td>symbol</td>
                                <td>number</td>
                                <td>optional</td>
                                <td>Currency symbol, if applicable</td>
                            </tr>
                            <tr>
                                <td>mode</td>
                                <td>string</td>
                                <td>optional</td>
                                <td>The mode of the asset, either normal or simple. Defaults to normal.</td>
                            </tr>
                            <tr>
                                <td>aliases</td>
                                <td>array(string)</td>
                                <td>optional</td>
                                <td>
                                    Any asset aliases which will be used to match
                                    imported prices and transactions to the asset.
                                    If not supplied, the name and uuid will be taken
                                    as defaults.
                                </td>
                            </tr>
                            <tr>
                                <td>import_id</td>
                                <td>string</td>
                                <td>optional</td>
                                <td>
                                    Unique identifier used for deduplication. If not
                                    specified, it is generated based on the parsed
                                    data
                                </td>
                            </tr>
                        </table>
                    </div>

                    <h3>Price</h3>
                    <div class="table-responsive">
                        <table class="table col-md-12 col-lg-10">
                            <tr>
                                <th>Attribute</th>
                                <th>Type</th>
                                <th>Required?</th>
                                <th>Notes</th>
                            </tr>
                            <tr>
                                <td>asset</td>
                                <td>string</td>
                                <td>required</td>
                                <td>The name of the Asset</td>
                            </tr>
                            <tr>
                                <td>moment</td>
                                <td>string</td>
                                <td>required</td>
                                <td>
                                    if no timezone is supplied, UTC is used. The
                                    date format used for parsing can be configured
                                    on the import configuration
                                </td>
                            </tr>
                            <tr>
                                <td>value</td>
                                <td>number</td>
                                <td>required</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>currency</td>
                                <td>string</td>
                                <td>required</td>
                                <td>The name of the Asset in which the value is represented</td>
                            </tr>
                            <tr>
                                <td>import_id</td>
                                <td>string</td>
                                <td>optional</td>
                                <td>
                                    Unique identifier used for deduplication. If not
                                    specified, it is generated based on the parsed
                                    data
                                </td>
                            </tr>
                        </table>
                    </div>

                    <h3>Transaction</h3>
                    <div class="table-responsive">
                        <table class="table col-md-12 col-lg-10">
                            <tr>
                                <th>Attribute</th>
                                <th>Type</th>
                                <th>Required?</th>
                                <th>Notes</th>
                            </tr>
                            <tr>
                                <td>asset</td>
                                <td>string</td>
                                <td>required</td>
                                <td>The name of the Asset</td>
                            </tr>
                            <tr>
                                <td>moment</td>
                                <td>string</td>
                                <td>required</td>
                                <td>
                                    if no timezone is supplied, UTC is used. The
                                    date format used for parsing can be configured
                                    on the import configuration
                                </td>
                            </tr>
                            <tr>
                                <td>amount</td>
                                <td>number</td>
                                <td>required</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>note</td>
                                <td>string</td>
                                <td>optional</td>
                                <td>A text field for your own convenience</td>
                            </tr>
                            <tr>
                                <td>import_id</td>
                                <td>string</td>
                                <td>optional</td>
                                <td>
                                    Unique identifier used for deduplication. If not
                                    specified, it is generated based on the parsed
                                    data
                                </td>
                            </tr>
                        </table>
                    </div>

                    <h3>Examples</h3>
                    <p>
                        Example JSON input:
                    </p>

                    <pre><code>{
    "error": [],
    "result": {
        "prices": [
            [
                1527897600,
                "something",
                true,
                "6380.8"
            ],
            [
                1527984000,
                "something else",
                false,
                "6487.9"
            ]
        ]
    }
}</code></pre>
                    <p>
                        Example parse expression, assuming the moment is the
                        first subelement and the value is the fourth subelement:
                    </p>

                    <p>
                        <code>
                            [.result.prices[] | {asset: 'Orichalcum', moment: .[0], value: .[3], currency: 'EUR'}]
                        </code>
                    </p>

                    <p>
                        Example CSV input:
                    </p>

                    <pre><code>time,open,high,low,close,vwap,volume,count
1591833600,"8705.8","8749.9","8033.4","8218.5","8398.0","8362.59444663",35708
1591920000,"8218.5","8463.9","8185.4","8413.5","8358.1","5257.27332502",23781
1592006400,"8413.6","8419.1","8325.0","8399.2","8376.2","981.83953932",9125</code></pre>

                    <p>
                        Example parse expression:
                    </p>

                    <p>
                        <code>
                            [.[] | {asset: 'Orichalcum', moment: .time, value: .close, currency: 'EUR'}]
                        </code>
                    </p>

                    <p>
                        Or in case you need to work with indexes, for example if
                        there is no header row:
                    </p>

                    <p>
                        <code>
                            [.[] | {asset: 'Orichalcum', moment: .[0], value: .[4], currency: 'EUR'}]
                        </code>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
