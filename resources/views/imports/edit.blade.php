<?php

use App\Models\ImportConfiguration;

/** @var ImportConfiguration $importConfiguration */
/** @var string[] $selectableTimezones */

?>
@extends('layouts.app')

@section('content')
    <h1>Edit '{{ $importConfiguration->getOriginal('name') }}'</h1>

    <form class="needs-validation" action="{!! route('imports.update', $importConfiguration) !!}" method="post" novalidate>
        @method('put')
        @csrf

        @include('imports._form', ['importConfiguration' => $importConfiguration, 'selectableTimezones' => $selectableTimezones])

        <button type="submit" class="btn btn-primary">Update import configuration</button>
    </form>
@endsection
