<?php

use App\Models\ImportConfiguration;

/** @var ImportConfiguration $importConfiguration */

?>
<div class="modal" id="deleteImportModal{{ $importConfiguration->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteImportModalLabel{{ $importConfiguration->id }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteImportModalLabel{{ $importConfiguration->id }}">Delete import configuration '{{ $importConfiguration->name }}'</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this import configuration? You can still undo it later.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                <form action="{{ route('imports.delete', $importConfiguration) }}" method="post">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>
