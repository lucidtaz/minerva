<?php

use App\Models\ImportConfiguration;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/** @var LengthAwarePaginator&ImportConfiguration[] $importConfigurations */

?>
@extends('layouts.app')

@section('content')
    <h1>Import configurations</h1>

    <a href="{!! route('imports.create') !!}">
        <i class="fa fa-plus-square"></i>
        Create new
    </a>

    <a href="{!! route('imports.deleted') !!}">
        <i class="fa fa-trash-restore"></i>
        Show deleted
    </a>

    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>Name</th>
                <th>Method</th>
                <th>Operations</th>
            </tr>

            @forelse($importConfigurations as $importConfiguration)
                <tr>
                    <td>
                        <a href="{!! route('imports.show', $importConfiguration) !!}">{{ $importConfiguration->name }}</a>
                    </td>
                    <td>
                        {{ $importConfiguration->readable_method }}
                    </td>
                    <td>
                        <a href="{!! route('imports.edit', $importConfiguration) !!}" title="Edit..."><i class="fa fa-edit"></i></a>

                        <a href="#" data-toggle="modal" data-target="#deleteImportModal{{ $importConfiguration->id }}" title="Delete..."><i class="fa fa-trash"></i></a>
                        @include('imports._delete', ['importConfiguration' => $importConfiguration])
                    </td>
                </tr>
            @empty
                <tr><td colspan="999">There are no import configurations.</td></tr>
            @endforelse
        </table>

        {!! $importConfigurations->links() !!}
    </div>
@endsection
