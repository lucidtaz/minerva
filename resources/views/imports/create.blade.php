<?php

use App\Models\ImportConfiguration;

/** @var ImportConfiguration $importConfiguration */
/** @var string[] $selectableTimezones */

?>
@extends('layouts.app')

@section('content')
    <h1>New import configuration</h1>

    <form class="needs-validation" action="{!! route('imports.store') !!}" method="post" novalidate>
        @csrf

        @include('imports._form', ['importConfiguration' => $importConfiguration, 'selectableTimezones' => $selectableTimezones])

        <button type="submit" class="btn btn-primary">Create import configuration</button>
    </form>
@endsection
