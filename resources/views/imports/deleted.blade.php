<?php

use App\Models\ImportConfiguration;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/** @var LengthAwarePaginator&ImportConfiguration[] $importConfigurations */

?>
@extends('layouts.app')

@section('content')
    <h1>Deleted import configurations</h1>

    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>Name</th>
                <th>Method</th>
                <th>Date deleted</th>
                <th>Operations</th>
            </tr>

            @forelse($importConfigurations as $importConfiguration)
                <tr>
                    <td>
                        {{ $importConfiguration->name }}
                    </td>
                    <td>
                        {{ $importConfiguration->readable_method }}
                    </td>
                    <td>
                        {{ $importConfiguration->deleted_at->ago() }}
                    </td>
                    <td>
                        <form action="{{ route('imports.restore', $importConfiguration) }}" method="post">
                            @csrf
                            <button type="submit" class="btn btn-primary">Restore</button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr><td colspan="999">There are no deleted import configurations.</td></tr>
            @endforelse
        </table>

        {!! $importConfigurations->links() !!}
    </div>
@endsection
