<?php

use App\Models\ImportConfiguration;

/** @var ImportConfiguration $importConfiguration */

?>
@extends('layouts.app')

@section('content')
    <h1>Import configuration {{ $importConfiguration->name }}</h1>

    <a href="{!! route('imports.edit', $importConfiguration) !!}">
        <i class="fa fa-edit"></i>
        Edit
    </a>

    <a href="#" data-toggle="modal" data-target="#deleteImportModal{{ $importConfiguration->id }}"><i class="fa fa-trash"></i> Delete</a>
    @include('imports._delete', ['importConfiguration' => $importConfiguration])

    @if(!empty($importConfiguration->note))
        <h2>Note</h2>
        <p>
            {!! nl2br(e($importConfiguration->note)) !!}
        </p>
    @endif

    <h2>Attributes</h2>
    <ul>
        <li>Method: {{ $importConfiguration->readable_method }}</li>
        <li>Date format: {{ $importConfiguration->date_format ?? '(default)' }}</li>
        <li>Timezone: {{ $importConfiguration->timezone->getName() }}</li>
    </ul>

    <h2>Parse expression</h2>
    <pre class="text-wrap">{{ $importConfiguration->parse_expression }}</pre>

    <h2>Run this import configuration</h2>
    <form action="{{ route('imports.run', $importConfiguration) }}" method="post" enctype="multipart/form-data">
        @csrf

        <div class="form-group">
            <div class="custom-file col-lg-6">
                <input class="custom-file-input @if ($errors->has('import_file')) is-invalid @endif" type="file" name="import_file" id="import_file" />
                <label class="custom-file-label" for="import_file">Uploaded file (if applicable)</label>
            </div>

            @error('import_file')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <input type="submit" class="btn btn-primary" value="Run!" />
    </form>
@endsection
