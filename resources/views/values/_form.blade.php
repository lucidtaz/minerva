<?php

use App\Models\Asset;
use App\Models\Price;
use App\Models\Transaction;
use Illuminate\Support\Collection;

/** @var Collection<int, Asset> $baseAssets */
/** @var Price $price */
/** @var Transaction $transaction */

?>
<div class="form-group">
    <label for="value">Value</label>
    <input type="number" class="form-control @if ($errors->has('value')) is-invalid @endif" name="value" id="value" value="{{ $transaction->amount }}" required />
    @error('value')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="base_asset_id">Currency of value</label>
    @if(count($baseAssets) === 0)
        <div class="alert alert-danger">There are no currencies yet. To add a currency, <a href="{{ route('assets.create') }}">create a new asset</a> and enter a currency symbol.</div>
    @else
        <select class="form-control @if ($errors->has('base_asset_id')) is-invalid @endif" name="base_asset_id" id="base_asset_id" required>
            @foreach($baseAssets as $baseAsset)
                <option value="{{ $baseAsset->id }}" @if($price->base_asset_id === $baseAsset->id) selected="selected" @endif @if($asset->id === $baseAsset->id) disabled="disabled" @endif>{{ $baseAsset->currency_symbol }} - {{ $baseAsset->name }}</option>
            @endforeach
        </select>
        @error('base_asset_id')
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    @endif
</div>

<div class="form-group">
    <label for="note">Note (optional)</label>
    <textarea class="form-control @if ($errors->has('note')) is-invalid @endif" rows="5" name="note" id="note">{{ $transaction->note }}</textarea>
    @error('note')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="moment">Moment</label>
    <input type="text" class="form-control @if ($errors->has('moment')) is-invalid @endif" name="moment" id="moment" value="{{ $transaction->moment !== null ? $transaction->moment->setTimeZone($timezone)->format('Y-m-d H:i:s') : '' }}" required />
    @error('moment')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>
