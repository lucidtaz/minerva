<?php

use App\Models\Asset;
use App\Models\Price;
use App\Models\Transaction;
use Illuminate\Support\Collection;

/** @var Asset $asset */
/** @var Collection<int, Asset> $baseAssets */
/** @var Price $price */
/** @var Transaction $transaction */

?>
@extends('layouts.app')

@section('content')
    <h1>Add value for '{{ $asset->name }}'</h1>

    <form class="needs-validation" action="{!! route('values.store', $asset) !!}" method="post" novalidate>
        @csrf

        @include('values._form', ['baseAssets' => $baseAssets, 'price' => $price, 'transaction' => $transaction])

        <button type="submit" class="btn btn-primary">Add value</button>
    </form>
@endsection
