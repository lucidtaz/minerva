<?php

use App\Dtos\DataSet;
use App\Services\Graph\GraphControls;
use Illuminate\Support\Enumerable;

/** @var string $id */
/** @var Enumerable&DataSet[] $dataSets */
/** @var bool $stacked */
/** @var GraphControls $controls */

// Determined using http://vrl.cs.brown.edu/color
$colors = ['#68affc', '#09c553', '#ef662f', '#f3c011', '#997cfb', '#a7d479', '#ba865c', '#69c8c1', '#e66493'];
$availableColorSelector = static fn (DataSet $dataSet, int $index) => $colors[$index];
$fallbackColorSelector = static fn (DataSet $dataSet, int $index) => '#' . substr(md5($dataSet->title), 0, 6);
$colorSelector = static fn (DataSet $dataSet, int $index) => $index < count($colors) ? $availableColorSelector($dataSet, $index) : $fallbackColorSelector($dataSet, $index)

?>
<form action="{{ route('display.graph') }}" method="post">
    @csrf

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">Resolution</span>
        </div>
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary @if($controls->resolution === \App\Services\Graph\Resolution::Day) active @endif">
                <input type="radio" name="resolution" value="{{ \App\Services\Graph\Resolution::Day->value }}" @if($controls->resolution === \App\Services\Graph\Resolution::Day)checked="checked"@endif> Day
            </label>
            <label class="btn btn-secondary @if($controls->resolution === \App\Services\Graph\Resolution::Week) active @endif">
                <input type="radio" name="resolution" value="{{ \App\Services\Graph\Resolution::Week->value }}" @if($controls->resolution === \App\Services\Graph\Resolution::Week)checked="checked"@endif> Week
            </label>
            <label class="btn btn-secondary @if($controls->resolution === \App\Services\Graph\Resolution::Month) active @endif">
                <input type="radio" name="resolution" value="{{ \App\Services\Graph\Resolution::Month->value }}" @if($controls->resolution === \App\Services\Graph\Resolution::Month)checked="checked"@endif> Month
            </label>
        </div>
        <input class="btn btn-primary" type="submit" value="Submit">
    </div>
</form>
<div id="{{ $id }}"></div>
<script>
    var dataSets{{ $id }} = {!! json_encode(
        $dataSets->map(fn (\App\Dtos\DataSet $dataSet, int $index) => [
            'name' => $dataSet->title,
            'data' => $dataSet->data->map(fn (\App\Dtos\NumberAtMoment $numberAtMoment) => [
                $numberAtMoment->moment->setTimeZone($timezone)->getTimestamp() * 1000, // Use timestamps to minimize bytes sent. Multiply because JS timestamps use milliseconds.
                $numberAtMoment->number,
            ]),
        ])
    ) !!};
    var colors{{ $id }} = {!! json_encode(
        $dataSets->map(fn (\App\Dtos\DataSet $dataSet, int $index) => $colorSelector($dataSet, $index))
    ) !!};
    var stacked{{ $id }} = {!! json_encode($stacked) !!};

    // We need to wait until after DOMContentLoaded, since the app.js script
    //  is loaded with the "defer" attribute
    document.addEventListener('DOMContentLoaded', () => {
        var options = {
            chart: {
                type: stacked{{ $id }} ? 'area' : 'line',
                stacked: stacked{{ $id }},
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'smooth',
            },
            fill: {
                opacity: stacked{{ $id }} ? 0.0 : 1.0, // Detail of the library: for area charts, fill determines the area, while for line charts it determines the line color.
                type: 'solid',
            },
            legend: {
                showForSingleSeries: true,
                position: 'top'
            },
            colors: colors{{ $id }},
            series: dataSets{{ $id }},
            xaxis: {
                type: 'datetime',
            }
        }

        var chart = new ApexCharts(document.querySelector("#{{ $id }}"), options);

        chart.render();
    });
</script>
