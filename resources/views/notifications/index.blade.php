<?php

use App\Presentation\NotificationPresenter;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Pagination\LengthAwarePaginator;

/** @var LengthAwarePaginator&DatabaseNotification[] $notifications */

/** @var NotificationPresenter $presenter */
$presenter = app(NotificationPresenter::class);

?>
@extends('layouts.app')

@section('content')
    <h1>Notifications</h1>

    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>Status</th>
                <th>Message</th>
                <th>Moment</th>
                <th>Operations</th>
            </tr>

            @forelse($notifications as $notification)
                <tr>
                    <td>
                        @if($notification->read())
                            <i class="fa fa-envelope-open-text text-primary"></i>
                        @else
                            <i class="fa fa-envelope text-danger"></i>
                        @endif
                    </td>
                    <td>
                        {{ $presenter->formatMessage($notification) }}
                    </td>
                    <td>
                        <span title="{{ $notification->created_at->toDateTimeString() }}">{{ $notification->created_at->ago() }}</span>
                    </td>
                    <td>
                        <a href="{!! route('notifications.show', $notification) !!}" title="Show..."><i class="fa fa-eye"></i></a>
                    </td>
                </tr>
            @empty
                <tr><td colspan="999">There are no notifications.</td></tr>
            @endforelse
        </table>

        {!! $notifications->links() !!}
    </div>
@endsection
