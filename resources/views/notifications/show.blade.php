<?php

use App\Presentation\NotificationPresenter;
use Illuminate\Notifications\DatabaseNotification;

/** @var DatabaseNotification $notification */

/** @var NotificationPresenter $presenter */
$presenter = app(NotificationPresenter::class);

?>
@extends('layouts.app')

@section('content')
    <h1>Notification</h1>

    <h2>Stats</h2>
    <ul>
        <li>Type: {{ $notification->type }}</li>
        <li>Message: {{ $presenter->formatMessage($notification) }}</li>
        <li>Full data: {{ var_export($notification->data, true) }}</li>
    </ul>
@endsection
