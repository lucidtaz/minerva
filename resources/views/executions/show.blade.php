<?php

use App\Models\ImportExecution;

/** @var ImportExecution $execution */

?>
@extends('layouts.app')

@section('content')
    <h1>{{ $execution->importConfiguration->name }}</h1>

    <a href="#" data-toggle="modal" data-target="#deleteExecutionModal{{ $execution->id }}"><i class="fa fa-trash"></i> Undo</a>
    @include('executions._delete', ['execution' => $execution])

    <ul>
        <li>Executed {{ $execution->created_at->toDateTimeString() }}</li>
        <li>Entities created: {{ $execution->entities_created }}</li>
        <li>Warnings: {{ $execution->warnings }}</li>
        <li>Errors: {{ $execution->errors }}</li>
    </ul>

    <pre>{{ $execution->log }}</pre>
@endsection
