<?php

use App\Models\ImportExecution;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/** @var LengthAwarePaginator&ImportExecution[] $executions */

?>
@extends('layouts.app')

@section('content')
    <h1>Imports</h1>

    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>Moment</th>
                <th>Configuration</th>
                <th>Entities created</th>
                <th>Warnings</th>
                <th>Errors</th>
                <th>Operations</th>
            </tr>

            @forelse($executions as $execution)
                <tr>
                    <td>
                        <span title="{{ $execution->created_at->toDateTimeString() }}">{{ $execution->created_at->ago() }}</span>
                    </td>
                    <td>
                        <a href="{!! route('imports.show', $execution->importConfiguration) !!}">{{ $execution->importConfiguration->name }}</a>
                    </td>
                    <td>
                        {{ $execution->entities_created }}
                    </td>
                    <td>
                        {{ $execution->warnings }}
                    </td>
                    <td>
                        {{ $execution->errors }}
                    </td>
                    <td>
                        <a href="{!! route('executions.show', $execution) !!}" title="Show output"><i class="fa fa-align-left"></i></a>

                        <a href="#" data-toggle="modal" data-target="#deleteExecutionModal{{ $execution->id }}" title="Undo..."><i class="fa fa-trash"></i></a>
                        @include('executions._delete', ['execution' => $execution])
                    </td>
                </tr>
            @empty
                <tr><td colspan="999">There are no imports yet. Create an import configuration and run it.</td></tr>
            @endforelse
        </table>

        {!! $executions->links() !!}
    </div>
@endsection
