<?php

use App\Models\ImportExecution;

/** @var ImportExecution $execution */

?>
<div class="modal" id="deleteExecutionModal{{ $execution->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteExecutionModalLabel{{ $execution->id }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteExecutionModal{{ $execution->id }}">Undo import</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to undo this import? This cannot be reversed without re-importing the original data.</p>
                <p>{{ $execution->assets_count }} Asset(s), {{$execution->prices_count}} Prices and {{ $execution->transactions_count }} Transaction(s) will be removed.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                <form action="{{ route('executions.delete', $execution) }}" method="post">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-danger">Undo</button>
                </form>
            </div>
        </div>
    </div>
</div>
