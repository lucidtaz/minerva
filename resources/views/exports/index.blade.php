@extends('layouts.app')

@section('content')
    <h1>Exports</h1>

    <a href="{!! route('exports.create') !!}">
        <i class="fa fa-plus-square"></i>
        Create new
    </a>
@endsection
