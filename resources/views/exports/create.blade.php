@extends('layouts.app')

@section('content')
    <h1>New export</h1>

    <form class="needs-validation" action="{!! route('exports.run') !!}" method="post" novalidate>
        @csrf

        <p>
            The export contains the data of this organization and will be downloaded after clicking the button below.
        </p>

        <button type="submit" class="btn btn-primary">Create export</button>
    </form>
@endsection
