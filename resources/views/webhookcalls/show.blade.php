<?php

use App\Models\Webhook;
use App\Models\WebhookCall;

/** @var Webhook $webhook */
/** @var WebhookCall $call */

?>
@extends('layouts.app')

@section('content')
    <h1>Webhook call for '{{ $webhook->name }}'</h1>

    <h2>Stats</h2>
    <ul>
        <li>Moment: {{ $call->created_at->setTimezone($timezone)->toIso8601String() }}</li>
        <li>Status code: {{ $call->status_code ?? 'No response' }}</li>
        <li>Error: {{ $call->is_error ? 'Yes' : 'No' }}</li>
        <li>Error summary: {{ $call->short_error ?? 'None' }}</li>
        <li>Duration: {{ $call->duration_ms }}ms</li>
    </ul>

    <h2>Request</h2>
    <pre class="pre-scrollable">{{ $call->request }}</pre>

    <h2>Debug</h2>
    <pre class="pre-scrollable">{{ $call->debug }}</pre>

    <h2>Response</h2>
    <pre class="pre-scrollable">{{ $call->response ?? 'None' }}</pre>

    <h2>Error detail</h2>
    <pre class="pre-scrollable">{{ $call->error ?? 'None' }}</pre>
@endsection
