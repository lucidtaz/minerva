<?php

use App\Models\Webhook;
use App\Models\WebhookCall;

/** @var Webhook $webhook */
/** @var WebhookCall[] $calls */

?>
@extends('layouts.app')

@section('content')
    <h1>Webhook calls for '{{ $webhook->name }}'</h1>

    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>Status code</th>
                <th>Error summary</th>
                <th>Duration</th>
                <th>Moment</th>
                <th>Operations</th>
            </tr>

            @forelse($calls as $call)
                <tr>
                    <td>
                        {{ $call->status_code ?? 'None' }}
                    </td>
                    <td class="@if($call->is_error) text-danger @else text-success @endif">
                        {{ $call->short_error ?? 'None' }}
                    </td>
                    <td>
                        {{ $call->duration_ms }}ms
                    </td>
                    <td>
                        <span title="{{ $call->created_at->toDateTimeString() }}">{{ $call->created_at->ago() }}</span>
                    </td>
                    <td>
                        <a href="{!! route('webhookcalls.show', [$webhook, $call]) !!}" title="Show..."><i class="fa fa-eye"></i></a>
                    </td>
                </tr>
            @empty
                <tr><td colspan="999">There are no calls.</td></tr>
            @endforelse
        </table>

        {!! $calls->links() !!}
    </div>
@endsection
