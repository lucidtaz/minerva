<?php

use App\Models\User;

$user = auth()->user();
if ($user instanceof User) {
    $unreadNotificationCount = $user->unreadNotifications()->count();
} else {
    $unreadNotificationCount = 0;
}

?>
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" href="{{ mix('images/owl.svg') }}" type="svg">
    <link rel="icon" href="{{ mix('images/owl.png') }}" type="png">

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ mix('images/owl.svg') }}" width="30" height="30" class="d-inline-block align-top" alt="">
                    {{ config('app.name', 'Minerva') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @yield('mobilenav')
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if ($openRegistration)
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                @if($unreadNotificationCount > 0)
                                    <a class="nav-link text-danger" href="{{ route('notifications.index') }}">
                                        <i class="fa fa-envelope mr-1 fa-fw d-inline-block d-md-none"></i>
                                        Notifications ({{ $unreadNotificationCount }})
                                    </a>
                                @else
                                    <a class="nav-link" href="{{ route('notifications.index') }}">
                                        <i class="fa fa-envelope mr-1 fa-fw d-inline-block d-md-none"></i>
                                        Notifications
                                    </a>
                                @endif
                            </li>

                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <i class="fa fa-user mr-1 fa-fw d-inline-block d-md-none"></i>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    @can('admin-actions')
                                        <a class="dropdown-item" href="{{ route('admin.home') }}">
                                            {{ __('Admin Dashboard') }}
                                        </a>
                                    @endcan

                                    <a class="dropdown-item" href="{{ route('preferences.edit') }}">
                                        {{ __('Preferences') }}
                                    </a>

                                    <a class="dropdown-item" href="{{ route('organizations.index') }}">
                                        {{ __('Organizations') }}
                                    </a>

                                    @foreach(Auth::user()->organizations as $organization)
                                        <a class="dropdown-item pl-5" href="{{ route('organizations.switch', $organization) }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('organization-switch-form-{{ $organization->id }}').submit();">
                                            {{ $organization->name }} @if(Session::get('organization_id') === $organization->id) (active) @endif
                                        </a>

                                        <form id="organization-switch-form-{{ $organization->id }}" action="{{ route('organizations.switch', $organization) }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    @endforeach

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>

            </div>
        </nav>

        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    @yield('page')
                </div>
            </div>
        </main>
    </div>
</body>
</html>
