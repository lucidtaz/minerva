@extends('layouts.site')

@section('mobilenav')
    @guest
    @else
        <a class="nav-link d-block d-md-none" href="{{ route('assets.index') }}">
            <i class="fa fa-cubes mr-1 fa-fw"></i>
            Assets
        </a>

        <a class="nav-link d-block d-md-none" href="{{ route('allocations.index') }}">
            <i class="fa fa-table mr-1 fa-fw"></i>
            Allocations
        </a>

        <a class="nav-link d-block d-md-none" href="{{ route('imports.index') }}">
            <i class="fa fa-file-import mr-1 fa-fw"></i>
            Import configurations
        </a>

        <a class="nav-link d-block d-md-none" href="{{ route('executions.index') }}">
            <i class="fa fa-folder-open mr-1 fa-fw"></i>
            Imports
        </a>

        <a class="nav-link d-block d-md-none" href="{{ route('webhooks.index') }}">
            <i class="fa fa-fish mr-1 fa-fw"></i>
            Webhooks
        </a>
    @endguest
@endsection

@section('page')
    @guest
    @else
        <nav class="col-md-3 d-none d-md-block">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('assets.index') }}">
                        <i class="fa fa-cubes mr-1 text-primary fa-fw"></i>
                        Assets
                    </a>
                </li>

                @foreach($allAssets as $asset)
                    <li class="nav-item ml-3">
                        <a class="nav-link" href="{{ route('assets.show', $asset) }}">
                            <i class="fa fa-cubes mr-1 text-primary fa-fw"></i>
                            {{ $asset->name }}
                        </a>
                    </li>
                @endforeach


                <li class="nav-item mt-md-3">
                    <a class="nav-link" href="{{ route('allocations.index') }}">
                        <i class="fa fa-table mr-1 text-primary fa-fw"></i>
                        Allocations
                    </a>
                </li>

                @foreach($allAllocations as $allocation)
                    <li class="nav-item ml-3">
                        <a class="nav-link" href="{{ route('allocations.show', [$allocation, \App\Enums\AllocationViewMode::Detailed->value]) }}">
                            <i class="fa fa-table mr-1 text-primary fa-fw"></i>
                            {{ $allocation->name }}
                        </a>
                    </li>
                @endforeach


                <li class="nav-item mt-md-3">
                    <a class="nav-link" href="{{ route('imports.index') }}">
                        <i class="fa fa-file-import mr-1 text-primary fa-fw"></i>
                        Import configurations
                    </a>
                </li>

                @foreach($allImportConfigurations as $importConfiguration)
                    <li class="nav-item ml-3">
                        <a class="nav-link" href="{{ route('imports.show', $importConfiguration) }}">
                            <i class="fa fa-file-import mr-1 text-primary fa-fw"></i>
                            {{ $importConfiguration->name }}
                        </a>
                    </li>
                @endforeach

                <li class="nav-item mt-md-3">
                    <a class="nav-link" href="{{ route('executions.index') }}">
                        <i class="fa fa-folder-open mr-1 text-primary fa-fw"></i>
                        Imports
                    </a>
                </li>

                <li class="nav-item mt-md-3">
                    <a class="nav-link" href="{{ route('webhooks.index') }}">
                        <i class="fa fa-fish mr-1 text-primary fa-fw"></i>
                        Webhooks
                    </a>
                </li>
            </ul>
        </nav>
    @endguest

    <div class="col-md-9">
        @yield('content')
    </div>
@endsection
