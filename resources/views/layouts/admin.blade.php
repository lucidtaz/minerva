@extends('layouts.site')

@section('page')
    <nav class="col-md-2 d-none d-md-block">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('home') }}">
                    <i class="fa fa-arrow-left mr-1 text-primary fa-fw"></i>
                    Member area
                </a>
            </li>

            <li class="nav-item">
                <strong>
                    Users
                </strong>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.users.index') }}">
                    <i class="fa fa-people-arrows mr-1 text-primary fa-fw"></i>
                    View all
                </a>
            </li>

            <li class="nav-item">
                <strong>
                    Scheduled checks
                </strong>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.scheduled-checks.index') }}">
                    <i class="fa fa-clock mr-1 text-primary fa-fw"></i>
                    Past executions
                </a>
            </li>
        </ul>
    </nav>

    <div class="col-md-10">
        @yield('content')
    </div>
@endsection
