<?php

use App\Models\Invitation;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/** @var LengthAwarePaginator&Invitation[] $invitations */

?>
@extends('layouts.app')

@section('content')
    @if(Session::has('created_invitation_token_url'))
        <div class="alert alert-success" role="alert">
            <p>
                The invitation URL is: <span id="newInvitationTokenUrl" class="text-monospace">{{ Session::get('created_invitation_token_url') }}</span>

                <button type="button" class="clipboard_copy_link btn btn-sm btn-link"  data-clipboard-target="#newInvitationTokenUrl" data-toggle="tooltip" data-placement="top" title="Copy to clipboard">
                    <i class="fa fa-copy" aria-hidden="true"></i>
                </button>
            </p>

            <p>
                Please give this token to the right person.
            </p>

            <p>
                <strong>This code is not emailed automatically and it will not be shown again.</strong>
            </p>
        </div>
    @endif

    <h1>Open invitations</h1>

    <a href="{!! route('invitations.create') !!}">
        <i class="fa fa-plus-square"></i>
        Create new
    </a>

    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>Note</th>
                <th>Role</th>
                <th>Expiry</th>
                <th>Operations</th>
            </tr>

            @forelse($invitations as $invitation)
                <tr>
                    <td>
                        {{ $invitation->note }}
                    </td>
                    <td>
                        {{ $invitation->role->value }}
                    </td>
                    <td>
                        {{ $invitation->expires_at->ago(parts: 2) }}
                    </td>
                    <td>
                        <a href="#" data-toggle="modal" data-target="#deleteInvitationModal{{ $invitation->id }}" title="Delete..."><i class="fa fa-trash"></i></a>
                        @include('invitations._delete', ['invitation' => $invitation])
                    </td>
                </tr>
            @empty
                <tr><td colspan="999">There are no open invitations.</td></tr>
            @endforelse
        </table>

        {!! $invitations->links() !!}
    </div>
@endsection
