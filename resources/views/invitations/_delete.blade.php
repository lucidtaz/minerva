<?php

use App\Models\Invitation;

/** @var Invitation $invitation */

?>
<div class="modal" id="deleteInvitationModal{{ $invitation->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteInvitationModalLabel{{ $invitation->id }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteInvitationModalLabel{{ $invitation->id }}">Retract invitation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to retract this invitation? You will need to re-invite this user.</p>
                <p>Note: {{ $invitation->note }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                <form action="{{ route('invitations.delete', $invitation) }}" method="post">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-danger">Retract</button>
                </form>
            </div>
        </div>
    </div>
</div>
