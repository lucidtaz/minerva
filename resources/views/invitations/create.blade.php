<?php

use App\Enums\OrganizationUserRole;
use App\Models\Organization;

/** @var Organization $organization */
/** @var OrganizationUserRole[] $roles */

?>
@extends('layouts.app')

@section('content')
    <h1>Invite user</h1>

    <form class="needs-validation" action="{!! route('invitations.store') !!}" method="post" novalidate>
        @csrf

        @include('invitations._form', ['invitation' => $invitation, 'roles' => $roles])

        <button type="submit" class="btn btn-primary">Create invitation</button>
    </form>
@endsection
