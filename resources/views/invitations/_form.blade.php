<?php

use App\Enums\OrganizationUserRole;
use App\Models\Invitation;

/** @var Invitation $invitation */
/** @var OrganizationUserRole[] $roles */

?>
<div class="form-group">
    <label for="name">Note</label>
    <input type="text" class="form-control @if ($errors->has('note')) is-invalid @endif" name="note" id="note" value="{{ $invitation->note }}" required />
    @error('note')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="role">Role</label>
    <select class="form-control @if ($errors->has('role')) is-invalid @endif" name="role" id="role" required>
        <option value="-1">Please select...</option>
        @foreach($roles as $role)
            <option value="{{ $role->value }}" @if($invitation->role === $role) selected="selected" @endif>{{ ucfirst($role->value) }}</option>
        @endforeach
    </select>
</div>
