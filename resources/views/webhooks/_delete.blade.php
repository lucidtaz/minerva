<?php

use App\Models\Webhook;

/** @var Webhook $webhook */

?>
<div class="modal" id="deleteWebhookModal{{ $webhook->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteWebhookModalLabel{{ $webhook->id }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteWebhookModalLabel{{ $webhook->id }}">Delete webhook '{{ $webhook->name }}'</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this webhook? You cannot undo this.</p>
                <p>Webhook URI: {{ $webhook->uri }}</p>
                <p>{{ trans_choice('webhook.allocations_count', $webhook->allocations_count) }}.</p>
                <p>If there any allocations use this webhook, their check configuration will be cleared.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                <form action="{{ route('webhooks.delete', $webhook) }}" method="post">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>
