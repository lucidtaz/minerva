<?php

use App\Models\Webhook;

/** @var Webhook $asset */

?>
@extends('layouts.app')

@section('content')
    <h1>Edit webhook</h1>

    <form class="needs-validation" action="{!! route('webhooks.update', $webhook) !!}" method="post" novalidate>
        @method('put')
        @csrf

        @include('webhooks._form', ['webhook' => $webhook])

        <button type="submit" class="btn btn-primary">Update webhook</button>
    </form>
@endsection
