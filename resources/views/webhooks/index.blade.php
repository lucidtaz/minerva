<?php

use App\Models\Webhook;

/** @var Webhook[] $webhooks */

?>
@extends('layouts.app')

@section('content')
    <h1>Webhooks</h1>

    <a href="{!! route('webhooks.create') !!}">
        <i class="fa fa-plus-square"></i>
        Create new
    </a>

    @if(Session::has('shared_secret'))
        <div class="alert alert-success">
            Webhook created. The shared secret is {{ Session::get('shared_secret') }}. This value will only be displayed once.
        </div>
    @endif

    @if(Session::has('test_success'))
        <div class="alert alert-success">
            {{ Session::get('test_success') }}
        </div>
    @endif

    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>Name</th>
                <th>URI</th>
                <th>Calls</th>
                <th>Operations</th>
            </tr>

            @forelse($webhooks as $webhook)
                <tr>
                    <td>
                        <a href="{{ route('webhookcalls.index', $webhook) }}">{{ $webhook->name }}</a>
                    </td>
                    <td>
                        {{ $webhook->uri }}
                    </td>
                    <td>
                        {{ $webhook->calls_count }}
                    </td>
                    <td>
                        <a href="{!! route('webhooks.edit', $webhook) !!}" title="Edit..."><i class="fa fa-edit"></i></a>

                        <a href="#" data-toggle="modal" data-target="#deleteWebhookModal{{ $webhook->id }}" title="Delete..."><i class="fa fa-trash"></i></a>
                        @include('webhooks._delete', ['webhook' => $webhook])

                        <form method="post" action="{{ route('webhooks.test', $webhook) }}">
                            @csrf
                            <button type="submit" class="btn btn-sm btn-primary">Test</button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr><td colspan="999">There are no webhooks.</td></tr>
            @endforelse
        </table>

        {!! $webhooks->links() !!}
    </div>
@endsection
