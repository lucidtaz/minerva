<?php

use App\Models\Webhook;

/** @var Webhook $webhook */

?>
<div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" id="name" value="{{ $webhook->name }}" required />
    @error('name')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="uri">URI</label>
    <input type="url" class="form-control @if ($errors->has('uri')) is-invalid @endif" name="uri" id="uri" value="{{ $webhook->uri }}" required />
    @error('uri')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>
