<?php

use App\Models\Webhook;

/** @var Webhook $webhook */

?>
@extends('layouts.app')

@section('content')
    <h1>New webhook</h1>

    <form class="needs-validation" action="{!! route('webhooks.store') !!}" method="post" novalidate>
        @csrf

        @include('webhooks._form', ['webhook' => $webhook])

        <button type="submit" class="btn btn-primary">Create webhook</button>
    </form>
@endsection
