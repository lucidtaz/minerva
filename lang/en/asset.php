<?php

return [
    'transaction_count' => 'You have :count transaction with this asset|You have :count transactions with this asset',
];