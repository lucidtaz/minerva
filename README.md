# Minerva

Manage your investment portfolio without requiring in-depth finance knowledge

Project home page: https://gitlab.com/lucidtaz/minerva

## Features

- Portfolio rebalancing based on current value and target allocation
- Currency conversion
- Imports (JSON/CSV) of uploaded files or from external APIs, scriptable to accept any data structure
- Multi-user multi-organization functionality with role-based access control
- API for maximum interoperability: automate price imports and periodically trigger rebalancing checks
- Webhook notifications for easy integration with other APIs
- Responsive design for use on phones, tablets and personal computers
- Open source
- Written in PHP 8.2 and Laravel 9 using modern development practices
- Easy self-hosting; Docker containers included

## Planned features

- Automation: import schedules and email alerts when an allocation requires balancing so custom integration is not required for common use cases
- Compliance: Audit logs and 2FA
- Infrastructure: Hosted cloud service

For a full list check the [project milestones](https://gitlab.com/lucidtaz/minerva/-/milestones).

## Out of scope

This list of potential features is unlikely to be incorporated in this project, for the reasons mentioned:

- Financial account management: to keep the usage and implementation simple, only whole assets are managed
- Double ledger accounting: this is not accounting software, but an inventory of how much you have of a certain thing
- (Automated) trading: this type of functionality is better placed in a dedicated tool, it could be integrated using the API or webhook functionality though

## Usage

Take the contents of the example `docker-compose.selfhosted.yml` and put them in a `docker-compose.yml` file in a separate directory. Then simply run `docker-compose up -d` from that directory and navigate to `http://localhost:8080`.

### Register your account to login

The example is set up in such a way that it allows open registration by anyone visiting the site. If this is the case, simply click "Register" and create an account for yourself.

When open registration is not enabled, you will need another way of creating the first admin user when setting up the project. To do this you can generate an invitation by running:

```
docker-compose exec app php artisan invitation:create
```

Then navigate to the URL which is presented in the console and complete the account registration procedure.

After registration, you may promote your account to (site wide) administrator by running:
```
docker-compose exec app php artisan user:make-admin <email address of your user>
```

### Enter portfolio data

After registering it's time to add portfolio data: Assets, Prices and Transactions.

Assets can be marked as a currency by adding a currency symbol, but this is not required, a stock is an Asset without a currency symbol. Regardless of having a defined currency symbol, Assets behave the same with respect to Prices and Transactions. You will need to at least create one currency Asset: the base currency in which you want to see the value of your portfolio.

For each Asset you will need to enter the current Price, and you can also add historical Prices. You will need to select a currency for the Price.

Now it's time to enter the total amount of each Asset to represent how much of it you have in the portfolio. You do this by creating Transactions. You can enter a single Transaction with the current amount, or you can specify it more precisely by creating individual Transactions on their historical moments.

If you have Assets that trade in different currencies, you will need to add Price records to those currency Assets in order to let the application do automatic currency conversion. For example if you have USD Prices but want to view your portfolio in EUR, you need to add a EUR-currency Price to the USD Asset.

### Create an Allocation

To balance your portfolio, you will first need to set a target Allocation. In it you set the relative weight of the Assets in your portfolio, which translates to percentages in the display. Each Asset has an imbalance threshold above which it's considered out of balance. Any Allocations out of balance will trigger visual alerts.

When the Allocation is out of balance, the recommendation is to do a full rebalancing of your portfolio. The Allocation overview shows how much to buy or sell of each Asset (assuming the Prices are up to date) to bring it back to balance.

Besides Assets, an Allocation can also consist of sub-Allocations. This is useful if you want to group different Asset classes without cluttering the view.

### Automate

Instead of adding each Price and Transaction manually you can also import them automatically or semi-automatically. You will need to create an Import Configuration for this. How it looks depends on the type of data you want to import. Some examples of what's possible:

- Call a JSON API to import Prices from an exchange (and trigger it automatically)
- Upload a CSV containing Transactions from your investment broker (use the web form or provide the file via the API)
- Call a blockchain API to import Cryptocurrency Transactions from and to your wallets
- Import data which was exported previously from this application (e.g. restore backup or move between organizations)

In the Import Configuration form you will be able to specify attributes to describe your data. The most complex attribute is the parse expression, which transforms your data from source to destination. The form has a detailed explanation with examples.

## Contributing

### Reporting issues

Please report any issues or requests for help on the GitLab issue page at https://gitlab.com/lucidtaz/minerva/-/issues.

### Making code changes

The sections below detail how to run the project and in broad lines what the code should look like. Merge requests should be sent on GitLab. Don't forget to update `CHANGELOG.md`.

### First time setup of local development environment

Run the following commands to set up the project and run the tests:
```
docker-compose build --parallel
./dev composer install
./dev npm install

cp .env.example .env.docker

./dev npm run dev
docker-compose up -d
./dev php artisan db:seed

./dev php artisan ide-helper:generate
./dev php artisan ide-helper:models --nowrite
./dev php artisan ide-helper:meta

open http://localhost:8080

# Login with admin@example.com / admin
```

### Updating dependencies

The above steps should be enough to get the mainline version of the project working. When dependency declaration files change, you can run the following commands to install the newest dependencies without having to start from scratch:
```
# JavaScript:
./dev npm install

# PHP:
./dev composer install
```

If you're interested in upgrading dependency versions in the lockfiles themselves, instead of just installing the latest lockfile changes from upstream, run the following:
```
# JavaScript
./dev npm outdated
./dev npm update
./dev npm audit
./dev npm audit fix

# PHP:
./dev composer outdated --direct
./dev composer update
```

then be sure to commit the changed lockfiles after verifying that the project works as expected.

### Engineering principles
Note: these principles may change over time as the project matures.

- Simplicity over Clever code: use Laravel idioms for a code base without surprises
- Iteration over Perfection: shape the project first, add polish after
- Integration over Unit tests: cover the code with integration tests and use unit tests for edge cases

### Quality Assurance
To run the tests, execute:

```
./dev php vendor/bin/phpunit
./dev php vendor/bin/behat
```

The first command executes the unit tests, ensuring that each unit of code does
what it's supposed to do. The second command runs behavioural tests, ensuring
that the product does what it's supposed to do.

The behaviour test suite includes a specification of features in human-readable
terms. Please read `tests/Acceptance/features*.feature`.
