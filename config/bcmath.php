<?php

return [

    // Default scale with which to perform bcmath operations
    'scale' => env('BCMATH_SCALE', 8),

];
