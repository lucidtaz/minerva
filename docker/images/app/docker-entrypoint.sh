#!/bin/sh

set -e

docker/scripts/wait-for-database.sh "$DB_HOST" "$DB_USERNAME" "$DB_PASSWORD" "$DB_DATABASE"

if [ "$APP_ENV" != "local" ]; then
    php artisan config:cache
    php artisan route:cache
    php artisan view:cache
fi

php artisan migrate --force

exec "$@"
