#!/bin/sh

# Abort immediately if one command fails
set -e

maxcounter=60

counter=1
while ! nc -z -v $APPLICATION_SERVER; do
    sleep 1
    counter=$(expr $counter + 1)
    if [ $counter -gt $maxcounter ]; then
        >&2 echo "Timeout of $maxcounter seconds while waiting for application server to become ready"
        exit 1
    fi;
done
