#!/bin/sh

# Usage: wait-for-database.sh <host> <user> <password> <database>

# Abort immediately if one command fails
set -e

maxcounter=60

counter=1
while ! PGPASSWORD="$3" psql -h"$1" -U"$2" -d"$4" -c "SELECT 1" >/dev/null 2>&1; do
    sleep 1
    counter=$(expr $counter + 1)
    if [ $counter -gt $maxcounter ]; then
        >&2 echo "Timeout of $maxcounter seconds while waiting for database to become ready"
        exit 1
    fi;
done
