#!/bin/sh

# Usage: wait-for-container-healthy.sh <container>
# Run this on the docker host, not inside a container!

# Abort immediately if one command fails
set -e

maxcounter=60

counter=1
while [ "`docker inspect -f {{.State.Health.Status}} $1`" != "healthy" ]; do
    sleep 1;
    counter=$(expr $counter + 1)
    if [ $counter -gt $maxcounter ]; then
        >&2 echo "Timeout of $maxcounter seconds while waiting for container to be ready to become ready"
        exit 1
    fi;
done;
